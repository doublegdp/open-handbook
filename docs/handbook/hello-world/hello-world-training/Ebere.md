#### Hello World,

I live in Ikeja, Lagos State in Nigeria and boy do i love the envrionment, its people, work and social lifestyle!. 

Ikeja, is the **capital of Lagos State** and is situated in the Lagos Mainland (Lagos is divided in two - Lagos Mainland and Lagos Island). The estimated population of Ikeja LGA is **411,228 inhabitants** with the area comprising of members of diverse ethnic affiliations. The Yoruba and English languages are commonly spoken in the area while Christianity and Islam are extensively practiced in the area.

Ikeja houses the **official seat of the Governor of the state** alongside various government agencies, parastals and bodies. A large number of businesses mostly retail and service-based businesses operate here. Ikeja can be classified as a high-class residential area on the mainland. It is easily accessible and widely popular. There are hotels, schools, churches, mosques, sites of attraction and residential places for both the rich and the middle-income earners alike. Living in Ikeja would help anyone stay abreast of what is happening in the city and get close to the business spots in Lagos.

One of the very popular spots in Ikeja is the large phone and computer accessories hub known as **Computer Village**, **Africa's largest information and communication technology market**. It serves as the trademark of the city itself as computer professionals buy and sell in this particular district.

Districts in Ikeja include **Oregun, Agidingbi, Magodo, Ogba, Maryland, Government Residence Area (GRA), Ojodu, Opebi, Akiode, Alausa, etc**. Many companies and businesses openings have their head offices in Ikeja because of its centrality. The city is indeed a place of commerce on a large scale.

Ikeja hosts several landmarks which include 

* Murtala Muhmmed Local and International Airports.

* Ikeja City Mall.

* The New Africa Shrine.

* Kalakuta Museum.

And a number of exqusite hotels, bars, entertainment areas and of course religious centers.

If by any chance you are in Lagos, chances are that you would most likey pass through Ikeja (Lagos only Airport is located in Ikeja), ensure you spend time to have a feel of the city, thank me later!.


