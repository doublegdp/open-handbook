I would like to introduce you to my city Nairobi, commonly referred to as "The green city in the sun".Some Kenyans like to refer to it as "shamba la mawe" which means "a rocky field".

The name Nairobi comes from a maasai phrase "Enkare Nairobi" that translates to "cool water" a reference to the Nairobi river that flows through it. Nairobi hosts over 100 major international companies and organizations hence its an established hub for culture and business. It is Africa's 4th largest exchange in terms of trading volume, capable of making about 10million trades a day.

It is found within the Greater Nairobi Metropolitan region which consists of 5 out of 47 counties, which generates about 60% of Kenya's GDP. The counties are Nairobi, Kiambu, Murang'a, Kajiado and Machakos county.

Nairobi also boasts of Nairobi National Park,the only National park that exists within a capital city and is located only 4 miles South of the city center.
