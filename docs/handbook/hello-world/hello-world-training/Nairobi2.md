Hello World,

I live in Nairobi, the capital city of Kenya.

**The only urban city with a national park** that the big 5 call home ie lion, rhino, elephant, buffalo and leopard.

The city has 4 rivers namely Nairobi, Mbagathi, Ngong and Mathare rivers.

Furthermore, the city has 3 major forests mainly Karura, Oloolua and Ngong forests. 

Nairobi is a beautiful city with clear blue skies on hot sunny afternoons with temperatures as high as 26°C and 12°C on cold days with grey gloomy skies.

The skyline of the city is an artist's wildest dream, denoted by the giant sky scrapers including the KICC, Times towers and the UAP Towers which create a beautiful outline of the city as the sun rises and sets everyday. 

The city life is fast paced with hustle and bustle of pedestrians, motorists, cyclists all in a rush to beat the never-ending traffic jam.

***The 4.3 million inhabitants*** of the city are spred across slums like Kibera and Korogocho and lush gated neighbourhoods like Karen and Runda.

We the people of Nairobi are warm, always smiling and ready to assist another person. 

Nairobi houses the following international/regional offices:

>1. UNON
>2. UNEP
>3. IOM
>4. World Vision
>5. IMF

The most popular meal is Ugali (maize meal) and Nyama choma (grilled meat).

The most favourite beverage is tea that is in abundance in the highlands. 

I LOVE NAIROBI!

KARIBU NAIROBI!

