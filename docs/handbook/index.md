---
title: Handbook Overview
---

# Handbook Overview

The DoubleGDP handbook is the centerpiece of our approach to building an effective all-remote team. Its purpose is to provide a single reference for all company processes that is universally available, always up to date. Together with our "[handbook-first](#handbook-first-process)" approach to process documentation, it provides the foundation of collaborative, asynchronous, and transparent process development.

The key tenets to making this approach work are that:

* All DoubleGDP processes are documented and actively referenced by the team. We are "[public by default](https://about.gitlab.com/handbook/values/#public-by-default)".
* At the same time, protect information that should be considered [not public](https://about.gitlab.com/handbook/communication/#not-public), including specifics about customers, finances, end-users, legal, and personnel.
* Every teammate actively participates and contributes directly to its ongoing evolution and is expected to make a change to the handbook with the goal of achieving one merge request per person per sprint.
* Processes and suggestions for improvement begin with a clear articulation of the proposal, and leverage GitLab's collaboration tools for the discussion and workflow that will enact the change.

The handbook is never "done" and will always be getting better. As is our product and internal workflow. The processes outlined below will help you learn how to contribute.

<details><summary>Tip from Olivier:</summary> “One of the most difficult things about async communication is learning to expect not to hear back right away… You have to learn to phrase your communication in such a way that you don’t inadvertently block yourself. Making a proposal rather than asking a question helps.”</details>

## Handbook-first process

### What does handbook-first mean?

Simply put, "handbook-first" means that in any circumstance where you explain a process or procedure or solution, you share the answer via a deep-link to a location in the handbook rather than a message on Slack, email, or Google Doc.

In practice, this adds a bit of overhead to the first time you answer any question but yields extensive dividends over the long-term by providing a reference that you can reuse, making it easier for all future team members to follow the solution, and keeping all of our documentation up to date.

Some practices that will help us all be handbook-first:

* Have a page where you are the principle maintainer
* Create long pages with many subsections rather than many small pages
* Write handbook edits as [minimal viable changes](#minimal-viable-change-mvc)
* [Make proposals](#make-a-proposal) for solutions rather than questions whenever possible

Read more about this in [GitLab's handbook-first primer](https://about.gitlab.com/company/culture/all-remote/handbook-first-documentation/).

### Minimal Viable Change (MVC)

We encourage MVCs to be as small as possible. Always look to make the quickest change possible to improve. If you validate that the change adds more value than what is there now, then do it. No need to wait for something more robust.

### Make a proposal

If you need to decide something as a team, make a concrete proposal instead of calling a meeting to get everyone's input. Having a proposal will be a much more effective use of everyone's time. Every meeting should be a review of a proposal. We should be [brainwriting on our own instead of brainstorming out loud](https://www.fastcompany.com/3033567/brainstorming-doesnt-work-try-this-technique-instead). State the underlying problem so that people have enough context to propose reasonable alternatives. The people that receive the proposal should not feel left out and the person making it should not feel bad if a completely different proposal is implemented. Don't let your desire to be involved early or to see your solution implemented stand in the way of getting to the best outcome. If you don't have a proposal, don't let that stop you from highlighting a problem, but please state that you couldn't think of a good solution and list any solutions you considered.

### Make small merge requests

When you are submitting a merge request for a code change, or a process change in the handbook, keep it as small as possible. If you are adding a new page to the handbook, create the new page with a small amount of initial content, get it merged
quickly, and then add additional sections iteratively with subsequent merge requests.

### Prefer fewer long pages to more shorter pages

Generally speaking, it's better to have longer pages with more content than many smaller pages because it makes it easier to find related content on the same page and to maintain and update existing content. This means when adding new content, give preference to creating a sub-head within an existing page than to start a new page.

See [more depth and a example on this rationale](https://gitlab.com/doublegdp/handbook/-/merge_requests/337#note_1046027102).

##  Basic processes and steps  
In order to contribute to the handbook, you will need to understand a little bit about Markdown. This is a simple text-based language that makes it easy to add structure (headings and bullets and hyperlinks) that can be interpreted by the browser and displayed in HTML. Please see this [Markdown Guide](https://www.markdownguide.org/basic-syntax/) for a basic tutorial.

There are two essential processes that all teammates are expected to follow in order to contribute to the handbook:

1. Make a small fix that doesn't need communication
1. Propose an improvement that should be reviewed or communicated before enacting

This section outlines the key steps for each.

### How to make a fix directly
You may make changes directly to the handbook and don't need approval. Use this process when you see a typo, something that you know to be out of date, or if you are the direct owner of a process. To do this:

1. Open the handbook source code in its editing environment: [https://gitlab.com/-/ide/project/doublegdp/handbook/tree/master/-/docs/](https://gitlab.com/-/ide/project/doublegdp/handbook/tree/master/-/docs/)
1. Make the change directly to the source code.
1. Click `Commit` and set to commit to the `master` branch, as shown in the image below. This will automatically "re-publish" your changes to the production version of the handbook
1. After 1-3 minutes, confirm that your edit shows correctly at [https://handbook.doublegdp.com/](https://handbook.doublegdp.com/)


<iframe width="560" height="315" src="https://www.youtube.com/embed/MBmUKP9FOK8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<iframe width="560" height="315" src="https://www.youtube.com/embed/pkqNRXApIFI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


That's it!

![Commit to Master Branch](../img/commit_to_master.jpg)

### How to propose a change
There are many other times that we have ideas on how to approve a process but may not be sure if it's a good one, or how it should get approved or communicated to others. This process provides a way to move forward -- you'll propose the change as you think it should be, and create a `merge request` that will be sent to a manager for review, approval, and communication.

Here are the steps for this process:

1. Open the handbook source code in its editing environment: [https://gitlab.com/-/ide/project/doublegdp/handbook/tree/master/-/docs/](https://gitlab.com/-/ide/project/doublegdp/handbook/tree/master/-/docs/)
1. Make the change directly to the source code.
1. Click `Commit` and select the option to `Create a new branch`, and the checkbox for `Start a new merge request`. (See screenshot below.) *This initiates a new `merge request` that will be reviewed by your manager.*
1. Enter a `title` for the change that you are proposing to make. Note that if you want to discuss or make further edits before your manager publishes the change, prefix the title with "Draft: " or "WIP: " (This is short for "Work In Progress")
1. You may write a `Description` if the proposal requires some context or if you want to initiate a dialog. This step is optional.
1. In the `Assignee` box, select your manager
1. Click `Submit merge request`

![Commit to New Branch](../img/commit_to_branch.jpg)

### Approval process

Your manager will review the change and decide whether to publish it, discuss it further, or decide not to enact the proposal.

Here are the considerations that a manager should make when deciding to approve:
- Who is the appropriate person to approve the change? (e.g. changes in Engineering are Nicolas' decision, but there are other areas that may be less clear.)
- Who needs to be consulted prior to approving the change? (e.g. The manager should either cc them on the MR, bring them in via Slack, or have a discussion.)
- Who needs to be informed about the change and how should they be notified? (e.g. sometimes it may be sufficient to add @all on the MR, but for other changes it may be better to communicate via Slack, email, share and discuss in a team meeting, or even to conduct a training)


<iframe width="560" height="315" src="https://www.youtube.com/embed/5U4j_J5Yii8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<iframe width="560" height="315" src="https://www.youtube.com/embed/4j-KC70k9K4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<iframe width="560" height="315" src="https://www.youtube.com/embed/r8TiV3pCF3E" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### Practice
If you're new to using the handbook, we recommend you do the "Hello World" exercise on the [Skills page](skills.md) before proceeding.

You might consider adding to your hello world page an introduction to how you like to work with others, including strengths and flaws. See [GitLab's CEO page](https://about.gitlab.com/handbook/ceo/#intro) or our [Head of Product's Hello World](hello-world/head-of-product-readme.md) page for an example.

## "Handbook-first" process development

With these fundamentals understood, you're ready to contribute to our ongoing process improvement. Here are the guidelines for how to approach it:

* When you see something in the handbook that's out of date or erroneous, fix it.
* When you want to improve the way we work, propose it with a Merge Request. (See [GitLab's take on this](https://about.gitlab.com/handbook/communication/#everything-starts-with-a-merge-request).)
* When you're not sure how to do something, look in the handbook.

To make a change:
1. Propose the new process in the way you would like it to work. Start in the **Proposed process improvements**

For improvements that are straightforward, start by writing the proposal specifically as you would like the new process to work directly in the IDE. Trivial changes like fixing typos that require no review from others can be committed directly to `master`. Any change that should be reviewed by someone else or communicated to others should be committed to a branch as part of a merge request. In this case you should think about content you put into the commit as the proposed process, and the MR as the discussion and workflow that is helpful to rolling it out.

For example, let's imagine you want to add a step to our bug filing process. In the IDE, you will write simply the step that you propose to add. You may have discussed this with your manager or your team prior, but it is not necessary -- it's fine to initiate a proposal by making a direct change on a new branch.

The merge request that you create becomes the workflow that helps your team understand the context of the proposal. If this is well understood, no additional context may be required. Your manager can review the change and `Approve`. However, if more context is helpful to understanding your proposal, you may add a title or description that explains the problem as you're experiencing it currently. You may ask questions or concerns, or share alternative ideas that you considered. This will help your manager decide whether to approve, and your teammates to understand and/or improve on the proposal.

Note that if the proposal requires a bit of back-and-forth and is not ready to publish, just prefix the title of the MR with "Draft:" or "WIP:" (short for "work in progress") -- this will prevent your manager from inadvertently approving it before you're ready. Remove this prefix when you're ready for it to be published.

Either way, your manager will need to review and approve the change. Once they do, they will then make sure that the appropriate people are aware of and agree to the change and merge it into master, thereby making it official DoubleGDP process.

**Complex changes**
In most cases, a proposal with a merge request will be sufficient to make the change that you want. However, you may sometimes want to describe a problem for which you don't yet have a proposed solution. In this case, you may start the process by creating an `issue` instead of a `merge request`. An issue can be used to describe a problem or concern, even if you don't yet know how to solve it. Think if it as raising a question that eventually will be answered via the MR.


## Guidelines for writing process

Write straightforwardly the way you want the process to work, and in the present tense. You can include a rationale, but don't make reference to the change. e.g. it's great to say "we use this process because..." but not to say "we're making this process to improve xyz." This latter context of the problem you aim to address should be described in the MR rather than the handbook itself.

Also, start small. Incremental improvements over time are far more effective, less risky, and easier to roll out than big changes all at once.

### Filenames in handbook
File names in the handbook must be all lowercase and use hyphens (-) instead of underscores (_) to separate words. They may not have spaces in them. e.g. `hello-world.md` instead of `Hello_World.md` or `Hello World.md`.

This is done because filenames become the urls that are shared, and this is a common url convention. It avoid files sorting in unexpected order (since git and GitLab sort all capital letters before all lowercase ones), and consistency across directories and links within the handbook. More explanation is available here: [https://medium.com/@victor.leong.17/5-url-best-practices-936329ba36a7](https://medium.com/@victor.leong.17/5-url-best-practices-936329ba36a7)

### Use descriptive names for hyperlinks

When creating hyperlinks in the handbook, try to use a concise and precise description for the text that's being linked. Specifically:

1. When linking to a Google Doc, use the full name of the document as its descriptor
1. When linking to a website or article, use its name or title

In both cases, this helps with searchability and for the handbook user to know what they're clicking on without having to read the full sentence for context. Specifically, [avoid links that use "here" or "this"](https://ux.stackexchange.com/questions/12100/why-shouldnt-we-use-words-such-as-here-and-this-in-textlinks) as their description.


## Confidential issues

Confidential issues should be used when we reference PII (e.g. any individual person's name) or any named partner for whom we do not have explicit permission in our [partner confidentiality preferences list](https://docs.google.com/document/d/1PJ2aVAiZLh0yxTfS395AK2wtoEvlXGBuh9sd56LEQug/edit?usp=sharing).

Please refer to GitLab's documentation on confidential issues [here](https://docs.gitlab.com/ee/user/project/issues/confidential_issues.html)



## Handbood Ownership per Page

When you suggest a process to a page owned by someone else, it should be as an MR and not a commit. That person must approve all changes to their page.

Here's the ownership of directories:

| Directory | Owner|
| --- | --- |
| City Processes | Head of Revenue |
| Company | CEO |
| Customer Success | Head of Revenue |
| Engineering | Head of Engineering |
| Finance | People Ops Manager |
| Handbook | People Ops Manager or CEO |
| Marketing | Head of Product or CEO |
| People Group | People Ops Manager or CEO |
| Product | Head of Product |
| Sales | Head of Revenue |
| User Guide | Head of Product |

The owner may delegate sub-pages as they see fit. When there is more than one owner, either may approve a merge request but both are accountable to following that agreed process, so they must coordinate if there is any doubt.


## Usage Statistics

Team members may view statistics on our handbook usage through [Google Analytics](https://analytics.google.com/analytics/web/?authuser=1#/report-home/a150647211w265002080p238945283)
