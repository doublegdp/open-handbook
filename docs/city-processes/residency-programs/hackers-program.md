---
title: Hackers In Residence Program
---

## Purpose
The purpose of the Hackers in Residence (HIR) program is to provide residential agglomeration at Nkwashi by creating economic opportunity from living within the city.

The HIR will host a cohort of ambitious, smart, and entrepreneurial engineers at the Nkwashi site. It will provide them with housing, training, supplies, and a living stipend for them to study and learn software development and job skills. It will then help them find remote work and support them as they learn to contribute to a global workforce. It aims to attract talent to the city, provide them access to a well-paying job, and settle them as early residents of the city. 

## Roles & Responsibilities

#### DoubleGDP's HIR's Commitment
- Provide housing at Nkwashi that has:
    - A high-speed and stable internet connection that meets these minimum requirements
    - Stable power supply 24/7 to optimize learning
- Provide a $100 monthly food stipend and a $200 living stipend for student residents for those accepting to join the program in 2021
- Provide a $200 monthly food and living stipend for student residents accepting to join the program in 2022.
- Provide a $100 food stipend when the Student takes a break/leave from Microverse.
- Conducive work environment
- If necessary, help direct potential candidates into Microverse's application process
- Provide a good path for them to stay and live in the city.
- Provide access to a laptop for the duration of the program. Students should fill in the Asset Handover Document.

#### Microverse's Commitment
- Reach out to existing Microverse applicants to help get first 2 student residents (we have some Zambian applicants already in our pipeline)
- Run applicants through our technical and soft skill selection process
- Prioritize spots for our 1-2 student residents in our March cohort
- Provide a ~10-month learning experience where students will learn to become remote, professional web developers and land a job
- Job search curriculum and life-time job career coach support

### Dufuna and Bongo Hive's Commitment
- Recruit a maximum of 20 students for the duration of the 6 month program
- Provide the 6 month curriculum where students will learn remotely
- Recruit a Program Manager who will live at Nkwashi with the students

#### Student Resident's Commitment
- All Microverse student commitments, including attendance, professionalism, etc.
- Microverse Program Hours: 9 am - 6:15 pm, Zambia time
- Return the Laptop and other assets provided when they exit the HIR Program
   - **NOTE** : If any concerns are raised during the program around standard laptop options, the recipient of the device shall resort to using their personal equipment. The only exception made is when the device has factory defects that make the equipment problematic on use. 
- Agree to live together at Nkwashi during the course of the program
- Participate in building the city's economy and culture
- Help attract new residents by becoming an Nkwashi Ambassador, using the [Brand Ambassador Playbook](https://docs.google.com/presentation/d/1cWxYeey6lDmgtrDKwS0WZx3KdKiMSTLoMAgZzrzMm7E/edit?usp=sharing)
- Consider (but not commit to) continuing to live at Nkwashi once employed

#### Nkwashi Commitments
Provide the Hackers in Residence with:

- Power & water supply
- Help with internet connection, if needed
- Roads or transportation
- Welcome the hackers into Nkwashi community

## HIR Applicant Journey
1. Submit initial application on [Microverse](http://www.microverse.org)
2. Pass Coding Challenges with Microverse
3. Submit satisfactory Check-in on Microverse
4. Fill out HIR application 
5. Complete and pass virtual interview with DoubleGDP CSM team
6. Pass Coding Trials with Microverse
7. Get accepted or rejected from the HIR Program
8. Acceptance call from Microverse
9. Register as student
10. Get assigned to next cohort
11. Move into Nkwashi

Note: External and internal communication plan (including email templates) and ads can be found [here](https://www.notion.so/microverse/Hackers-In-Residence-Microverse-357709de4e30477bb1d32005e6dd1b2a).

## HIR Onboarding Process
- Sign HIR the Terms & Conditions
- Collect Next of Kin, Home Address and Contact Information 
- Register as a student with Microverse
- Receive and set-up laptop
- Share Brand Ambassador Playbook 
- Attend Brand Ambassador Workshop
- Introduction meeting between Chena, the artists and the hackers
- Move into Nkwashi
- Complete [HIR Bank Details](https://docs.google.com/spreadsheets/d/1Irmmdk8VIRnBQJVazuH8Oy1nfy22SUgqW8l3Wz6nxnw/edit#gid=195740912) for Wise transfers.

## HIR Offboarding Process
- If the Hacker has withdrawn/dropped out of Microverse, the Hacker should receive a written notice detailing their exit from the HIR Program and next steps.
- Collect Assets such as Laptop and Mifi/Router devices.


## HIR Program Roles
#### HIR Program Manager
Duties include: 

- Recruiting the hackers, including taking the final decision on recruitment (note: Doublegdp's CEO can veto any decision)
- Onboarding the hackers
- Promoting the HIR Program from DoubleGDP and Nkwashi social media channels
- Supporting the hackers in any educational or personal challenge they may encounter
- Coordinating stipend payments
- Overall project management of the HIR program at Nkwashi

#### HIR On-site Coordinators
Duties include:

- Purchasing equipment for the hackers, including laptops and furniture
- Liaison between HIR program manager and hackers to address on-site issues


## Marketing: Promoting the HIR Program

The HIR Program Coordinator is in charge of promoting the HIR Program. The main components of the program promotion include:

- The HIR program has a website available at [https://www.hackersinresidence.co/](https://www.hackersinresidence.co)
- A bi-weekly [webinar](https://docs.google.com/presentation/d/1sr0IO9nvCZrM7psXGbe868qvGftWvo-ot_420bv20Ag/edit?usp=sharing) is hosted on Zoom. Participants can register on the [website](https://www.hackersinresidence.co/).  
- The program is to be promoted on both [DoubleGDP's](https://twitter.com/2xgdp) and [Nkwashi's](https://www.instagram.com/nkwashi.city/) social media handles. 
- [Bongohive](https://bongohive.co.zm) has also agreed to share HIR's flyers with their communities, including [Facebook Developer Circle Lusaka](https://www.facebook.com/groups/DevCLusaka/). 
- [Microverse](http://www.microverse.org) also promotes the HIR Program through their mailing list of over 1,000 Zambian current and past applicants. 





