## Overview
Most of our Partner Communities have a variety of Amenities to offer to their Residents, Customers or Visitors. The "Book Amenity" page gives our Community Partners the option to advertise them through the App or even book slots to use those amenities that require booking in advance.

### Creating a New Amenity
To create a New Amenity for your community, follow these steps:

1. Login to your Community App as an Administrador.
2. Go to the Main Menu (Hamburger) and click on "Community".
3. Once Community menu is displayed, click on "Book Amenity".
4. Go to the top right of your screen and click on "+ Add New".
5. A Form will be displayed requiring information related to the Amenity. Fill out the information and click "Save".
6. The Amenity will be created and it will be visible to users on the Amenities page.
7. IMPORTANT NOTE: If the Amenity requires Booking it is necessary to include a Calendly link when creating the Amenity. Follow the instructions below to create a Calendly link for the Amenity.

Amenity can be Edited and Deleted through the options available for each Amenity Card on the Kabab Manu.

### Creating a Calendly Link for an Amenity
When an Amenity requires booking, it is necessary to create a Calendly Event to manage the bookings. You must work with the Community to coordinate and gather all the information related to the Amenity, such as, availability time slots, dates, email address to receive confirmation and sync calendar, and any other information that is considered of interest when booking an Amenity.

Follow these steps to create a Calendly link:

1. Login to the Calendly account using the credentials available in 1Password Shared Vault.
2. Once you have logged in, select "+ New Event Type".
3. Then click on "Create" for One-on-One option. This will allow only one person to book the Amenity at the same time. If multiple people can book at the same time select Group option.
4. Fill out the information required as General and click on "Next".
5. Fill out the information related to Time and Availability and click on "Next".
6. Continue filling out the information related to Invitee Questions, Notification and Cancellation Policy, and Confirmation Page. On Confirmation Page it is highly recommended to select "Redirect to an external site" to redirect to the Community App page.
7. Once the "Event" has been created, is important to link the Event with a personalized calendar from someone in the Community that will be in charge of managing the Amenity. To do this, go to "Account" and click on "Calendar Connections".
8. Click on "+ Add Calendar Account" and select the Calendar type you want to Connect with the Event/Amenity.
9. Next step requires that the person logs in to their account to allow syncing the calendar account.
