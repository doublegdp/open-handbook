## How to Import Contacts in Bulk

In the case that an admin needs to create several profiles on the Application, there is a Bulk Import option. The data received may not be clean enough for immediate use, so the admin should ensure that all data in the spreadsheet is in the right format.

#### The Format
1. The columns in the sheet should be (in the exact stated order);
Name, Email primary, Phone number primary, Phone number secondary 1, Phone number secondary 2, User type, Labels, State, Expiration date, Notes on client.
For more elaborative context, visit the [Import Page](https://app.doublegdp.com/users/import)

2. After cleaning up the data, convert it to a .csv file
3. Go back to the [Import Page](https://app.doublegdp.com/users/import) and select "Choose File"
4. Select "Import"

Note: This may take some time.

A message on the screen will appear confirming that the contacts have been uploaded to the App.

The system will also be able to notify an admin if there has been any duplicated entry.
