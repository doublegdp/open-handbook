## **Overview**
This process relates to Ciudad Morazán and is intent to help Administrators and Customs Delegate in the usage of the Customs Forms Registry.
## Creating and Submitting a Customs Form
How to create, complete and submit a Customs Form? Watch this [video](https://www.loom.com/share/65c20ed5aefb4be18cfc73a5032df0d3) for the English version and this [video](https://www.loom.com/share/17a1d00c47e4474cb88c144aa1d7ee4d) for the Spanish version, and learn how to do it.

