## How to Create a User on the App

In order to have a successful City Application, you need to create Users who will utilize the App on a daily basis.

1. Log in as a Admin.
2. Click the Menu button on the left side.
3. Click on the Search button. Search for the following below; <ul>
      <li>Full Name</li>
      <li>Email Address</li>
       <li>Phone Number</li>


This enables you to not add the same person twice. Once you have confirmed that the User does not exist, click on "Create a New Request."

4. Fill in all the required fields and click "submit". 
5. Click on the menu button on the top right and "Send OTP" to the new user.

Click <a href="https://www.loom.com/share/e8c34ed4437b4d4aac3b48c0ee1a84ed">here</a> to watch a video on how to create a user.





      
