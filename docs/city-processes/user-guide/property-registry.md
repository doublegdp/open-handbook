## How to Register a Plot on the App

<ol>
<li>Log in as an Admin and go to the Menu Bar and select Properties.</li>
<li>Select New Property</li>
<li>Fill in the necessary 'Details' required</li>
<li>Next select 'Ownership' and click on 'New Owner'. Make sure the Owner is already registered on the App so that their name automatically appears once typed in.</li>
<li>Click Save and just like that you have registered a new property on the App. The new property will appear on the homepage as the latest property added to the App.</li>
</ol>


This Process can also be made by going on the Users profile and selecting the menu bar on the right side. Select Plots and follow the steps above.

An Admin can view the users profile and verify the plot information as accurate.


