## Publishing Via Wordpress

DoubleGDP powered Applications in Partner cities have a News feature where Residents are kept abreast of current happenings pertaining to the City. An Admin is able to publish Articles onto the App by using Wordpress. The newest Posts are automatically placed at the top of all other featured content. To add a post to the Nkwashi App;

1. Go [here](https://wordpress.com/stats/day/doublegdp.wpcomstaging.com)
2. Go to the admin dashboard on the left, click **Posts** and then **Add New**
3. Be sure to add a title for your post and then type out the Article content in the provided field. Insert different elements to the post via **Blocks** ("+" sign at the top right corner).

A standard feature to note is that all News pieces in the App are accompanied by a Feature Image before getting to the content itself. To add in a Feature Image to your Post, go to *Feature Image on the right panel, and follow all the prompts.

Content is typically sort through by adding tags to each post. This assists App users to easily find all content based on their selection of tags of interest. To create a new tag or use any of the existing ones, go to **Tags** on the right panel.

Click **Save Draft** to save changes as a draft, or click **Publish** to immediately take the post live.

For DGDP WP account for testing use the **"DoubleGDP City"** site to publish onto **demo environment** (demo.doublegdp.com). The guidelines to publish are the same as the customer's sites. To access the WordPress account use the credentials shared in 1Password and once you're there, select **DoubleGDP City** Site by switching site on the right menu.
