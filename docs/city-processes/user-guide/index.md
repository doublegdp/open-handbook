---
title: User Guide Overview
---


## Purpose
Welcome to the DoubleGDP App user guide. This guide provides step-by-step instructions to city administrators and city residents on how to get the best from the app.

### Functionality and Use Cases

| Functionality | Use Cases |
| ------ | ------ |
| Campaign Management System (Sales) | -  Schedule and send campaigns through SMS and Email to prospects |
| Community Engagement: Discussion | - Allow your residents to share information and discuss pressing matters in the community |
| Community Engagement: Feedback | - Gather quantitative and qualitative sentiment from residents on services |
| Content Management System (News) | - Keep residents and prospective residents up to date with news |
| CRM, Customer Journey and Pipeline | - Organize leads from marketing and track their journey from sales to residency - Group leads by labels, user types, and customer journey stages - Directly connect CRM to campaign management system  |
| Emergency Reporting | - When there is immediate danger, residents can use their location to get immediate emergency response - When there is no immediate danger, residents fill out an incident report so the proper authorities can follow-up and investigate  |
| Forms and workflows | - Allow administrators to design forms and workflows, assign tasks,  and report on time to fulfill requests of city processes such as:  - Incident Reporting - Permitting - Licensing - Business registry - City repairs - Municipal requests |
| Gate Access  | - Central identifications system for all community members and guests - Allow your security teams to control and log who enters and exists the site - Allow your security teams to add information and observation on visit activity |
| Maps  | - Show points of interest including parks, utilities, commercial centers and schools - Show plots by status (sold, available) and any construction activity to solve coordination challenges |
| Payments, Invoices and Collections | - Create and send payment request and plans for service charges and payments - Share account statements and receipts with clients - Track cash flows, accounts receivables, non-payments, and understand trend insights |
| Property Registry |  - Track property ownership, plot details, coordinates, and valuation history - Keep a central database on which plots have been sold and are available |
| Service Requests and Incident Reporting | - Submit requests, track status, upload documents, and receive notifications for residents |
| Task Management System | - Create, assign, and track tasks for administrators, contractors, site workers, and guards with due dates - Send automated notifications based on different types of task events" |
