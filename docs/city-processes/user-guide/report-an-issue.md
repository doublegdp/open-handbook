## Report an issue

1. Click on “community”
2. Click on “report an issue”
3. Fill out the form
    1. Your name
    2. Choose the type of issue
    3. Enter a subject line
    4. Enter a description of the issue
    5. Attach an image (if needed)
    6. Submit

 ![](https://ibb.co/4g9gsD1)  
