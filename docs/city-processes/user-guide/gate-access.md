# Gate Access
Gate Access is a high priority for DGDP and its partner communities as we want the residents to be safe all the time. For the different communities we work with similar, but different in some parts, processes to ensure security at the entrance (gate). Follow these guidelines to manage gate access process through the communities:

## Guard Digital Entry Flow for Registered Users

1. Once a Resident/Worker approaches, greet her/him warmly and ask if they have their QR Code available.
2. **If Resident/Worker has the QR Code**, ask her/him to present it. **Select Scan** on Dashboard or on Search Bar. The phone camera will load.
3. Point camera frame to QR Code and scan.
4. User profile will load on scanning.
5. Go to the bottom on User's profile page and select **Log This Entry**.
6. **If Resident/Worker does not have the QR Code available**, search user’s name through the search bar on Dashboard. If Resident/Worker found, **Click on User's profile/Name**.
7. Go to the bottom on User's profile page and select **Log This Entry**.
8. If account is not valid the user will have to be logged in manually, if permitted.

## Guard Digital Entry Flow for Guests
1. Once a Guest approaches, greet her/him warmly and ask if they have a QR Code available or an Invitation.
2. **If the Guest has a QR Code**, ask her/him to present it. **Select Scan** on Dashboard or on Search Bar. The phone camera will load.
3. Point camera frame to QR Code and scan.
4. Guest profile will load on scanning.
5. Check that all details match Guest's information. *Optional: Update information if need it and select **Update**. After updating the information it will require that you go back to Step 1 by clicking on the green Check Mark on the top of the screen.
6. Go to the bottom on Guest's profile page and select **Grant Access**.
7. **If Visitor/Guest does not have a QR Code**. Go to the **Log Book** and click on **Guests** tab.
8. Search Guest’s name through the search bar or scroll down until you find the Guest. If Guest is found and the Invitation is "Valid" **Click on Grant Access** button on the Guest box.
9. If Guest does not show up in the **Guests** tab, or the Invitation is "Invalid", inform the Guest that she/he doesn't have an Active Invitation and ask to contact their Host.

## Guard Manual Entry Flow

1. If Visitor does not have an account, a valid QR Code or an Invitation, then select **Manual Entry** on Dashboard.
2. Enter Name, Phone Number, ID Number, and Vehicle Plate Number if needed.
3. Provide reason for visit.
4. Grant or Deny access and then allow entry into site. *For some Communities Deny button may not be available.


## Admin and Guards View for Logs and Guests (Entries/Exits/Observations/Invitations)

1. **Select Log Book** on Dashboard or left hand Menu.
2. To see all individual logs, Select **Log View** tab. You can see Entries, Exits and Observations.
3. Use the search bar or scroll down for specific log entries.
4. To see Guests Invitations, Select **Guests** tab. You can see all Invitations, Valid and Invalids.
5. Use the search bar and type Guest's name or scroll down for specific Invitations.
6. To see Users and Guests that have been granted access to the Community, and type of Entries, Select **Visit View** tab. You can also see the Statistics for the day: Total Entries, Total Exits and Total in City.
7. Use the search bar or scroll down for specific log entries.

For a Spanish version to train Security Guards, use this [presentation](https://docs.google.com/presentation/d/1CIzpXuteHTGvtuegHU_Ecc8fZN5y-9Fu7KvEy7us0ow/edit#slide=id.geb8cda4c0f_0_0) as a reference.

## Generating Logs Report (CSV)
Admins and Security Supervisors are able to generate reports to obtain data related to the Log Book. Follow these steps to generate a report:

1. **Select Log Book** on Dashboard or left hand Menu.
2. Select **Log View** tab.
3. Select the "Start Date" and "End Date" and then click on **Export data**. The report will be generated.
4. After clicking on **Export Data** click on **Download Report** and the report will be downloaded onto your device.
5. Save the report and open it as CSV.

## Log Book Labels Details

1. “Manual” indicates Manual Entry.
2. “Scanned Entry” indicates QR Code scan for a Registered User or Guest.
3. “User Granted Access” indicates that access was granted to a Registered User.
4. "Granted Access" indicates that access was granted to a Guest.
5. "Observation" indicated that an Observation was logged for a User/Guest.
6. “Exit Logged” indicates the User or Guest left the Community.

## Creating Guest Invitations

There are different user types across all Communities that have the ability to create Invitations for Guests. Depending on their needs, Communities enable these type of users, which are for example, Admins, Residents, Site Managers, to create Invitations. The purpose for creating an Invitation, is for the User (Host) to pre-register in a Guest List (Log Book) a Visitor (Guest), so the Security Team at the Gate can grant access to them. To create a Guest Invitation follow these steps:

1. Login with your account to the Community App.
2. On the left side of your screen, go to the main Menu (three bars/hamburger).
3. Select “Log Book” and then select “Guest List”.
4. Click ok the blue + icon.
5. A form to Invite Guest will show up.
6. Is recommended that you first fill out day of visit (consider recurrent visits selecting days), visit start time and visit end time.
7. Search for the user (Guest) on the search bar: type the name of your Guest or Company Name, if it is a Company:
 **a) If Guest/Company is found**, select "Add".
 **b) If Guest/Company is not found**, you will have to select PERSON if you want to create a regular Guest or select COMPANY if you want to create Company. In both scenarios, a second form will be displayed to be filled out with Guest/Company information. Fill out the information with Full Name (mandatory) and phone number and then select "Add".
8. After adding the first Guest, you can include more Guests to the same invitation by going to the search bar again and repeat step number 7.
9. Once you've finished adding all the Guests in your invitation, click on “Invite Guest”.
10. The invitation will be created and will be shown in the Guest List.
11. If a valid phone number was added to the Guest, they will receive a SMS with a link to access a QR Code to be presented at the gate.

For a Spanish video with instructions on how to create an invitation click [here](https://www.loom.com/share/e1c09f6227584394a62edc2b4352d059). For Spanish instructions in pdf on how to create an Invitation click [here](https://drive.google.com/file/d/1dBnZrAcnHpfw-or97RgWlE_i2elyyaG1/view?usp=sharing).
