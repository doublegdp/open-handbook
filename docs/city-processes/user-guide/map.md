## Map

The Map helps the user to navigate the city and pinpoint exactly where the city is located in a broader aspect. View the [Map Feature](https://app.doublegdp.com/map).

1. The Map can be viewed from a range of imagery modes such as;
<ul>
<li>OSM (Open Street Map)</li>
<li>Satellite</li>
<li>Mapbox</li>
</ul>

2. The User is able to visualize different areas on the map by selecting layers such as:
<ul>
<li>Land Parcels - When the User clicks on this checkbox the map brings up the Legend of 'plots sold' and 'unknown.' The User is able to identify which plots are sold and those that are not yet sold.</li>
<li>Points of Interest - When the User clicks on this checkbox it brings up the areas marked by the blue/white pin. Click on the point of interest and it will bring up the information about that particular area.</li>
<li>Sub-urban Areas - This shows the neighborhood information in the city. When the user clicks on this checkbox it highlights the divided suburb areas of the city.</li>
<li>Coverage Area - This is the entire geographical city boundary on the surface of the earth (depicted by large polygon shape).</li>
</ul>
