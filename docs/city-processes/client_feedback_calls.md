# Client Feedback Calls

## Purpose
Scheduling time to learn and listen from our customers/clients is paramount to DoubleGDP for market research in order for us to make informed product decisions and adjust services and processes accordingly. In addition, our feedback calls help us assess customer satisfaction amongst our clients and to measure how they view our product and support. These calls are usually requested by the product, design or customer success teams based on the different projects being worked on.

## Process
To schedule these calls, we use contacts from the client database that we have. There are two ways to go about this:

1. Send out a product survey and include a question asking participants whether they would like to discuss their answers further. This would work as direct consent from the clients as opposed to asking for it from Nkwashi.
2. Go through Nkwashi; ask their Relationship Manager which clients would be eligible to participate in the feedback calls.

Once the participants have been identified, it is the CS team's responsibility to get in touch with them and schedule time for the calls. These can either be zoom meetings or WhatsApp calls. The DGDP stakeholders are required to create a shared document in which notes will be taken and for asynchronous internal communication.

After all the sessions have been had, the data should be compiled and written in a report format. An example can be found [here](https://docs.google.com/document/d/1t6YlrdvVdENjTQCoH2YyTuqU8nkVegBnkX3nVAInNiE/edit#heading=h.b1iaklwqlejv) This report should include an overview of the project, purpose for client participation, feedback themes, conclusions and any other relevant data.
