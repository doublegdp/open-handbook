---
title: Managing User Data
---

### Overview

Digital identity is one of core capabilities of the app. All other city processes are dependent on accurate and relevant digital identities in order for the city to work optimally.

When used correctly user data allows the city to manage payments and other revenues; effectively communicate marketing offers and efficiently manage issues raised by the community members.

### User Types

City managers can update the following key user types:

1. **Resident:** This user type is given for individuals who live in the city as home-owners; renters; participants of residency programs and city/residents' staff who live within the city. They receive specific information such as service disruption updates. Their number is tracked to ensure the city is able to provide responsive services

1. **Client:** This user type is for individuals who have signed up for a plan to own property in the city. This may also apply to clients that have finished the purchase process and are constructing their property.

1. **Prospective Client:** This user type applies to individuals that are interested in owning property in the city and are yet to start the purchase process. They may receive targted promotional campaigns.


### Customer Journey Stages

City managers use the customer journey stages to track the progress clients are making towards residency that is a key factor in city growth.

1. **Plot Fully Purchased:** This applies to clients that have completed the purchase process as per their selected plan. These clients can receive targeted communication on how to start the construction process.

1. **Floor Plan Purchased:** These clients have selected and paid for their desired floor plan.

1. **Building Permit Approved:** These clients have met all the requirements to begin construction including plot and floor plan purchased. They would have a project team in place and have received the city's approval to start construction.

1. **Construction in Progress:** These clients have started the construction process and the property is not ready for occupation.

1. **Construction Completed:** These clients have completed the construction process and the property is ready for occupation.

### Adding Users

The process to add new users for the city app is under the responsibility of the Implementation Manager. The Implementation Manager, or the person he/she defined to manage the process is responsible to add these type of users to the app:



    1. Residents.
    2. Security Guards.
    3. Security Supervisors.
    4. Site Workers.
    5. Site Worker Managers.
    6. Any other user he/she considers to be relevant.

The guidelines to manage the process are as follows:



1. The IM collects the information related to each type of user.
2. Resident’s information is gathered through the rental application.
3. Security Guards and Security Supervisor’s information is gathered through the Security Company or after interviewing them when they first arrive.
4. Site Worker and Site Worker Manager’s information is gathered through the Site Worker Manager for the first case and through the Contractors Manager for the second case.
5. After gathering the information related to the users that are going to be created, the IM will create them in the app using the [guidelines provided here](/city-processes/user-guide/digital-id-generation-and-admin-resident-registry/#admin-resident-registry-digital-id-generation).

The Implementation Manager can ask the CSM for help when adding new users to the city app, whenever it is not possible for him/her to do it. For this, the IM will share a file (list) with the information related to every user that needs to be added, the list must include: Name and last name, phone number, email, external reference ID, type of user, expiration date (if applicable).

### Printing QR Codes

The Printed QR Codes have a relevant impact in the gate access flow as they help to minimize the time that the security guards spend granting access to guests, or visitors in the city. This process also prevents the usage of manual entries, which minimizes mistakes when entering information from a guest/visitor.

It is up to each city to decide who to grant a Printed QR Code and the type of material to use in their printing. It is highly recommended to grant them to all regular visitors. We also recommend using cards printed on paper and laminated on plastic. It is an easy, cheap and long lasting option.

Generating the QR Codes is a manual process that requires time when having a high number, but it can be quick when having just a few. The process to download (generate) a QR Codes is defined [here](/city-processes/user-guide/digital-id-generation-and-admin-resident-registry/#generating-downloading-qr-codes).

After having the downloaded QR Codes they can be printed in paper and then having them laminated in plastic for protection. This can be done in a local printer or the supplies can be acquired by the city to manage the process themselves. Here is a list of what you will need to be considered for the process:



* [Laser Jet Printer](https://www.amazon.com/pcr/Top-Rated-Laser-Computer-Printers-Reviews/172648)
* [Thermal Laminator Machine](https://www.amazon.com/card-laminator/s?k=card+laminator)
* [Business Card Laminating Pouches](https://www.amazon.com/Best-Laminating%C2%AE-Business-Laminating-Pouches/dp/B0127TGKWI/ref=pd_lpo_3?pd_rd_i=B0127TGKWI&psc=1)
* [Paper Cutter](https://www.amazon.com/Swingline-Trimmer-Guillotine-Capacity-ClassicCut/dp/B016LDV41S)

The Implementation Manager is responsible to manage the process on how the city will print the QR Codes. The CSM can assist and help any time the IM requires. It is recommended to have a test of how the process works after having printed the first batch of QR Codes. It is also under the responsibility of the IM to define the process on how to deliver the QR Codes to its owners (users).
