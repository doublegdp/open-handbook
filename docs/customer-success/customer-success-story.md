---
title: CS Stories
---

## Customer Success Stories Overview

Customer success stories are content that illustrate our customers' experience using the DoubleGDP app, the challenges they had before using the app, how the DoubleGDP app features helped and the level of success they have achieved.

DoubleGDP can use the content as a tool to demonstrate value realization for an existing customer and for prospects to understand the value of the product from an organization or community that has similar needs to theirs.


### Creating a Customer Success Story

Use the [customer success story template](https://gitlab.com/doublegdp/handbook/-/blob/master/.gitlab/issue_templates/Customer%20Success%20Story%20Template.md) to create a gitlab ticket and outline the details of the customer' story.



## Customer  Quotes and Success Stories

A collection of customer quotes and success stories can be found on this [link](https://docs.google.com/presentation/d/1KOaqmr0z1KKF_oY8ZdfPG2NAcVuoQUah8OIW1yZRtwE/edit#slide=id.g13cbf5e98e3_0_28). 
