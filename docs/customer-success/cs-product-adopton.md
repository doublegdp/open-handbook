---
title: CS Product Adoption
---

## Product Adoption

Product adoption is a process that ensures a user has become successful in achieving their goals while using a product.

The customer has to reach a point where they have to incorporate the city app into their daily routine and that cannot happen until they realize the value of it.

Though user logins and activity are positive signs they are not sufficient measures for adoption.


### Best Practices

By incorporating these best practices, customer success managers can track adoption progress and drive conversations with the DoubleGDP and city stakeholders.


1. Understand the challenge: By understanding the challenge faced by the users., the CSM can offer solutions through use of the city app, training needs or product enhancements. These questions can be used as guidelines:
     - Which user groups and processes are affected by the challenge?
     - How does the process affect the business? At a high level define this in terms of cost, revenue, profit, growth, security etc

1. Ensure executive buy-in: Business leaders are constantly looking for ways in which to add value to their organisations. To gain support across the city management ensure that any value reased is communicated to city managers. This can be done in various ways such as [Executive Business Reviews](/customer-success/executive-business-reviews), monthly reports, emails etc.

1. User Training: By training users CSM will have a level of surety that the users have knowledge and skills to use the city app to meet their goals.
     - Training Accessibility: Can the user find steps on how to use the app in the [user guide](/city-processes/user-guide/)?
     - Training Frequency: Has the user received training on the app functionality?
     - Product Updates: Is the user aware of new features added to their area of interest in the city appHas refresher training been done?

1. Use data to track usage. The KPI dashboard provides usage information. Check with the Head of Product before sharing with the customer.

### Templates

1. Executive Business Review: [product adoption summary slide](https://docs.google.com/presentation/d/1UnjlZYZ1tjnbXaIInZs5uNSbSnvjS6XtQMYtlpUPZvQ/edit#slide=id.gecdd06322b_0_0)
