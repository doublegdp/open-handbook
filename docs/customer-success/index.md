---
title: Customer Success Homepage
---

## Customer Success at DoubleGDP

The Customer Success team works with our customers to help them get value from our platform and services. This includes:


1. Driving adoption of the product through [user training](/city-processes/training-the-customer) and [user guides](/city-processes/user-guide/)
1. Driving [revenue growth](/customer-success/cs-sales-process/) through sales to new communities and upsells
1. Designing [city processes](/city-processes/)

See [customer success goals](/company/goals/#customer-success) under company OKRs.
