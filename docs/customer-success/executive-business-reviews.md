---
title: Executive Business Review (EBR)
---

## Executive Business Review

The purpose of our Executive Business Reviews (EBRs) help us stay aware of and aligned around top priorities with our customers. We aim to have one with each customer quarterly.

1. DoubleGDP will share some information in advance
    1. CEO will share a company progress update
    1. CEO or PM will share draft product roadmap for next ~6 months, as starting point to discussion
    1. Head of CS or account CSM will share account progress and key initiatives for next ~6 months
1. CSM will collect some information in advance of the call:
    1. Summarize current account expectation for population and housing stock in next 12 months -- this is so we can be jointly looking at same data
    1. Ask execs from account about their top 2-3 priorities in next 3-6 months (irrespective of DoubleGDP)?
    1. Do you have requests of us coming into the meeting?
    1. The [EBR template](https://docs.google.com/presentation/d/1UnjlZYZ1tjnbXaIInZs5uNSbSnvjS6XtQMYtlpUPZvQ/edit#slide=id.g9aa6f6f5d2_0_201) is available to the DoubleGDP team. It will be modified quarter-by-quarter to align with the city's goals. We also have an example of a [renewal deck](https://docs.google.com/presentation/d/1gIxFC-02oF1QNGScj5p8R233yjdJ-d1B_HqXuPYITWU/edit#slide=id.g1351641c9cd_0_16) that has good material to eventually merge into the EBR template.
1. During meeting, we will cover:
    1. Review of population and housing stock expectations for next 12 months
    1. Review of what we heard of priorities and requests?
    1. Do you have feedback for us? What can we be doing better?
1. After the meeting:
    1. The actions agreed with the customer will be tracked in the [action plan](https://docs.google.com/spreadsheets/d/1wfmRyVYnwPQ-T4amcJ4TKYfnA2NZz5EKHSXCbefdpUU/edit#gid=0) that is also made available to the city's representative. 

We will aim to include the key stakeholders from the customer, as well as DoubleGDP leadership team and account CSM.
