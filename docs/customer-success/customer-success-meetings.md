---
title: CS Collaboration
---

## Collaboration with DoubleGDP Teams

Customer Success has a few standing meetings:

 - All hands Customer Success sync (sales focus) - every Monday 4:15 PM CAT

 - All hands Customer Success sync (product adoption focus) - every Wednesday 4PM CAT

 - Customer Success and CEO sync - every Thursday 6:30 PM CAT

 - Customer Success, Product, Engineering sync - every Thursday 5PM CAT


### Customer Success and CEO Sync

This is a 25-minute weekly session for the Customer Success Team and the CEO. The meeting will be an opportunity for the CS team to ask questions directly of the CEO and for the CEO to discuss strategic initiatives with the team.  The agenda format will be in the form of a Q&A similar to the format used for the [All Team + Investor Call](https://handbook.doublegdp.com/Company/meetings/#all-team-investor-call).

The agenda is tracked in the "[CS team sync document](https://docs.google.com/document/d/1K6BegZkcpofhgJWV8KZ8IkMw0Y6tPlwkfOaUqpc3PyU/edit#)".

### Customer Success and Product Sync

This is a 50-minute weekly session for the Customer Success Team, Head of Product and Head of Engineering interactions. The meeting will be an opportunity for the CS team to discuss product related topics. Customer Success Team members can use the time to advocate for new product features or updates to the product that DoubleGDP cities can leverage to meet their business objectives. Participants from Product and Engineering will provide guidance to the proposals and highlight relevant product strategy and tactics to Customer Success.

The agenda is tracked in the "[Product CS sync document](https://docs.google.com/document/d/1CwQAZXi1Nq_FUaIOkoay1u7wQ2TymftWI5xiNqxDP5U/edit)".


### Customer Success Managers Meetings with Customers

Customer Success Managers (CSM) have frequent engagement with customers. The DoubleGDP meeting guidelines apply.

## Collaboration between CSM and Head of CS

Each CSM will have a weekly 1:1 session with the Head of CS. The 1:1 conversations are not limited to once a week and the CSM should request for more time if and when required. The goal is to ensure that the CSM have the tools and an environment that allows them to achieve their goals.

The 1:1 session structure will cover:

1. Revenue Progress: The CSM and Head of CS will review the Pipedrive [Insights](https://doublegdp.pipedrive.com/progress/insights/dashboard/44f110bec22bc61ee1dce7f803f6ef49) and [Activities](https://doublegdp.pipedrive.com/activities/list) for each CSM against their respective leads and deals.

1. Product Adoption Progress: The CSM and Head of CS will review the account product adoption dashboards and discuss activities required to increase adoption.

1. FYI and Concerns: These will be brief updates that may not have been covered in the other topics. A sentence or two, and may entail a quick clarification or question.
