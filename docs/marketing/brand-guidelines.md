# Brand Guidelines
This handbook page includes information about our brand, and guidelines on using our name and logo externally.

## Name
The name of our company is 'DoubleGDP' with the D & GDP capitalized and no spaces between the 'Double' and 'GDP'. We use 'DGDP' as a shortcut - but be careful to use this only when in a context where it is clear that you are referring to DoubleGDP.

Incorrect usage:
- "Double GDP" X
- "Doublegdp" X
- "doublegdp" X

Correct Usage:
- DoubleGDP ✓
- DGDP ✓

## Do we brand our app?
We include a “Powered by DoubleGDP” tag in the app, but want all branding to focus on our customers’ communities. There are enough new concepts for residents of new cities to grapple with, and explaining “who is DoubleGDP” is not something we want to introduce into the equation at this point. Better for people to associate our application on the innovation of the community itself.

We should think of our brand as important to entrepreneurs building new cities. This community is small enough that we do not think it’s necessary to have a consumer facing brand.


## Company letterhead
Our company Letterhead can be accessed [here](https://docs.google.com/document/d/14ctQ2A1SdhDdQ3xjibHKOlJfgsXJz7FIwJhfWUm_XxQ/edit). This is read-only; make a copy to edit

## Logos
We use the following logos at DoubleGDP. When using the logos, please keep space between the logo and other images or text. Generally a good amount of separation space is the size of the DoubleGDP 'house' away from the logo

- Logo 1, The DoubleGDP house:  
<img src="/img/marketing/logo_square.png" alt="DGDP Logo"
	title="DoubleGDP Square Logo" width="50" />
- Logo 2, The DoubleGDP house with text, orange:  
<img src="/img/marketing/DoubleGDP_rectangle_logo.png" width="150" />
- Logo 3, The DoubleGDP house with text, black  
<img src="/img/marketing/doubleblack.png" width="150" />

### Font
Our logo font is [Sofia Pro Soft Medium](https://www.linotype.com/1346538/sofia-pro-soft-medium-product.html?site=webfonts&format=ot-cff&branding=pro). We have a license, and the login to LinoType is stored in the shared vault of 1P. This font can be used for headlines only not body copy (text).

Text font is [Roboto](https://fonts.google.com/specimen/Roboto) for print and web. 

### Colors

**Orange**

- hex: #F07030
- cmyk: 0,71,98,0

**Black**

- hex: #000000
- cmyk: 0,0,0,100

**Light Grey (60%)**

- hex: #808285
- cmyk: 0,0,0,60

**Dark Grey (80%)**

- hex: #58595b
- cmyk: 0,0,0,80

## Marketing Assets

Company assets, including full resolution versions of the logo, are available in our [Marketing Assets](https://drive.google.com/drive/u/1/folders/0AEq4mz4LcLacUk9PVA) shared drive
