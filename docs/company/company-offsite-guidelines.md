---
title: Guidelines to 2022 Company Offsite in Nairobi, Kenya
---

## Event Location
[Radisson Blu Hotel & Residence, Nairobi Arboretum](https://www.radissonhotels.com/en-us/hotels/radisson-blu-nairobi-arboretum)

Arboretum Park Lane, Nairobi, Kenya

Phone:+254 709 031 000

Contact Person: [Lornah Nyakio](maito:lornah.nyakio@radissonblu.com)


## Is DoubleGDP’s Company Offsite a vacation?
Team members should participate in professional activities throughout the week. The primary purpose is to build bonds with team members and learn about DoubleGDP’s culture. Therefore, this week's leisure activities are not the focus, and team members should not view this week as a holiday. There will still be plenty of fun, and we’re planning many enjoyable activities.

## Will I have free time?
You can count on having free time during Arrivals Day and on some evenings or mornings. You’ll have a packed schedule the rest of the time, including excursions, workshops, mixers, team time, and other activities. Please note that events are not mandatory, so if you’re not up for a tour or a workshop, you’re not obligated to attend. However, the goal of this event is to build bonds with each other and cultivate a strong community, so if you decide not to participate in an event, we hope that you spend that time recharging or getting to know others.

## Can I attend for a shorter amount of time?
While we encourage everyone to attend the entire event as the program is crafted to ebb and flow with various learnings and differentiated interactions, we understand that this is a long time away from family and other priorities, obligations, and religious observances, so we’re able to accept partial attendance as long as team members can attend for at least 3 full days excluding Arrivals Day. We’re limiting partial attendance to at least 3 full days because the cost of attendance is such that a shorter amount of time may not be the best use of the company’s money.

## What should I wear?

Suggested Attire: Business casual

- Try some nice jeans (no holes) or dress pants.
- Clean, closed-toed shoes, please.
- Long pants, no tee shirts. (Especially for any events that are on-site with customers and for team visit events)
- Travel tip: Check the [weather](https://www.timeanddate.com/weather/kenya/nairobi/ext) before you pack your suitcase. 


## Will I have to share a room?
No, every team member will have a single room and will not share a room unless they bring a guest.

## May I bring a guest?
Yes, you may bring a guest. Guests must adhere to all of the requirements listed here (e.g. vaccination). You must pay their airfare. They will be invited to after-hours social events but not during-the-day team building events. You must register them in advance with EA, so that we have an accurate count.

## Will DoubleGDP provide health coverage?
DoubleGDP will cover the cost of an international health insurance for the duration of the trip. We recommend using [Atlas International](https://www.worldtrips.com/travel/insurance/Atlas-International-Insurance///////#/atq) This cost will be reimbursed by the company.

## Is a Covid-19 vaccination mandatory?
Yes, a full vaccination card is required to travel to Kenya. We are following local regulations closely to ensure safety.

Latest updates on International Travel Requirements as of April 1st, 2022:

- Travelers who are fully vaccinated are **exempted from the PCR test requirementfor entry**
- Vaccination certificates must be uploaded to the to the [Global Haven website](https://globalhaven.org/) before arrival.
- Eligible unvaccinated travelers over age of 5 years must have a negative COVID-19 PCR test 72 hours before departure. They will also be required to take a rapid antigen test at their own cost ($30) on arrival. Any person who tests positive will have a PCR test administered at their own cost ($50) and self-isolate. Please visit the Kenyan [Ministry of Health’s website](https://www.health.go.ke/)
- Incoming travelers are required to have a QR code for a completed Travelers’ Health Surveillance Form that they can access from this [website ](https://ears.health.go.ke/airline_registration/) . When they register on the website, the code is sent to their email.

## Is a Yellow Fever vaccination required?
Yes, Kenya requires vaccination for Yellow Fever at least 10 days before travel.

## How do I apply for a Visa to Kenya?
Be advised that effective January 1, 2021, all passengers are required to apply and obtain an e-visa before boarding an inbound aircraft to Kenya. The e-visa can be obtained [here](http://evisa.go.ke/evisa.html)

## What expenses will DoubleGDP cover?
There should be few accrued expenses for the event. Some situations may require additional fees, Finance will provide a list of approved team members and their expenses, and any unapproved costs will be rejected.

## Places to go when free

- [Giraffe Center](https://www.giraffecentre.org/)
- [Nairobi Safari Walk](http://www.kws.go.ke/content/nairobi-safari-walk)
- Museums: 
    - [Kenya Museum Society](https://www.kenyamuseumsociety.org/) 
    - [Nairobi National Museum](https://www.museums.or.ke/)

- Malls & Shopping
    - [Westgate Shopping Mall](https://www.westgate.co.ke/)
    - Woodlands Mall
    - Two Rivers



## Team members should only have the following expenses:

* Travel day meal stipend (Fixed cost USD 50)
* Visa-related expenses (if applicable) [VISA](https://evisa.go.ke/single-entry-visa.html)
* Covid-19 PCR test (if needed) for the team member
* All vaccinations required to enter Kenya (Ex: Yellow Fever)
* Passport (if applicable). Fixed cost. If you need to get a passport or renew it specifically for traveling to this event, you can expense up to $75 USD to cover the cost of this process. If the costs are higher, DoubleGDP will only reimburse up to $75 USD.

## TODO for team members


- [ ] When it comes to travel, **team members will be booking their own flights** using Brex or Ramp company cards and will follow our [travel guidelines](https://handbook.doublegdp.com/people-group/travel/), you can also pay out of pocket and submit as expense  for reimbursement; or request a cash advance if necessary. DoubleGDP will pay for flights between your start and end destination. If you want to modify from that, you can book directly with the airline and then modify and pay any change fees on your own.

- [ ] Once the flight ticket is purchased please fill out this [spreadsheet](https://docs.google.com/spreadsheets/d/1hojLqFn5HU3e0F0Qjcl_PODsZydELhTzxai0rucChpg/edit#gid=1430447698) with your flight details for hotel check in and out and transportation arragements.

- [ ] Hotel's room booking will be handle by EA once you complete the [spredsheet](https://docs.google.com/spreadsheets/d/1hojLqFn5HU3e0F0Qjcl_PODsZydELhTzxai0rucChpg/edit#gid=1430447698). You can find your hotel confirmation in our [shared drive](https://drive.google.com/drive/folders/1XLtY_9nOwSsxeG24PVO2E8B7q3UdjkOp?usp=sharing) 



Notes: If you decide to buy a souvenir, upgrade your seat, have late-night snacks, or do a different excursion, these expenses will not be reimbursed.

If you have general ideas and suggestions, please add them to [Issue #577](https://gitlab.com/doublegdp/handbook/-/issues/577)


### [AGENDA](https://docs.google.com/spreadsheets/d/1hojLqFn5HU3e0F0Qjcl_PODsZydELhTzxai0rucChpg/edit#gid=674165557)

2022-05-23 Arrivals - Travel Day: travel day with optional dinner around 7 pm local time

2022-05-24 Team Activities [Day 1 Pictures](https://stawiadvisory.pixieset.com/guestlogin/doublegdpday1/?return=%2Fdoublegdpday1%2F)

2022-05-25 Team Activities & Team Dinner [Day 2 Pictures](https://stawiadvisory.pixieset.com/guestlogin/doublegdpday2/?return=%2Fdoublegdpday2%2F)

2022-05-26 Team Activities - [Day 3 Pictures](https://stawiadvisory.pixieset.com/guestlogin/doublegdpday3awards/?return=%2Fdoublegdpday3awards%2F)

2022-05-27 Departures - Travel day - Optional group breakfast
