---
title: Company Overview
---

## Mission Statement
Our mission is to double the global GDP by providing every person in the world with access to a well-functioning community in which to live, work, and play.

Our company name originates from economic research that [world GDP can be doubled](https://openborders.info/double-world-gdp/) by allowing everyone the opportunity to live in the community of their choice. This argument focused on opening national borders; we believe open borders is not politically viable and aim to achieve the same goal by allowing well-functioning communities to flourish in every corner of the world.

## Elevator Pitch

DoubleDGP is building an end-to-end platform for new cities to connect with residents, accelerate growth, and deliver responsive public services. We have a [one-minute pitch](https://www.youtube.com/watch?v=ahTyhj7K9Hc) available on our YouTube channel and [a company overview deck here](https://docs.google.com/presentation/d/1c-Ci9QVSc-H0vwtelS0mL4Ka5v2kbzr-1BfOSfLp4d4/edit). The key points to convey when first introducing our story are:

1. Cities are economic engines and the biggest drivers of access to high quality of life.
1. There are too few great places to live, and restrictive national policies make them inaccessible to most people.
1. The world could experience tremendous growth of opportunity if entrepreneurs could build a well-functioning city where there's demand.  
1. In some ways cities are easier than ever to create -- need power, water, food, and internet connection. But still many barriers.
1. We help new city developers attract new residents, run the city responsively, and stay connected with their residents.
1. We do it with an end-to-end platform so that they don't need to build the IT infrastructure themselves.

Our Elevator Pitch:
<iframe width="560" height="315" src="https://www.youtube.com/embed/ahTyhj7K9Hc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
&nbsp;

Our **Overview Deck** is linked [on our website](https://www.doublegdp.com/approach/01-overview/)

### Investor materials
Answers to common investor questions are available in our [accelerator pitch responses](https://docs.google.com/document/d/1Q92NxVX7Ymdc3kbq1CzZuzXtogbZyuAjUMoJ__K7GmU/edit) (team members only).

### Product demo

- We have a [product demo](https://www.youtube.com/watch?v=ucCIBSxNUjc ) available.
- Our [Product demo script](https://docs.google.com/document/d/15oT0Xn61Pq69uDFWt2B4XyQwI2RwnvlvlWXn23wufHM/edit#) is available for anyone that wants to record or update our product demo (team members only)

## Values

By default, we will adopt the values from [GitLab](https://gitlab.com), who has them well documented in their [Handbook](https://about.gitlab.com/handbook/). GitLab has built a great all-remote company and their handbook provides a template for many of the processes and practices we aspire to. (In addition, GitLab's CEO is the founding investor of DoubleGDP.) Therefore, we leverage their work by default and then adapt or modify it when we find that other approaches work better in our context.

One starting place we've leveraged their work is in [Values](https://about.gitlab.com/handbook/values/). They highlight these six:

*   Collaboration
*   Results
*   Efficiency
*   Diversity, inclusion, and belonging
*   Iteration
*   Transparency

Presented in this order, the values spell CREDIT, which can be a helpful mnemonic to remember them. That said, the next section lists them in their proper priority order to us.

### Hierarchy

One of the most interesting points about GitLab values is that they are clear about the hierarchy of them. This is the same for us. We are a company, and one that is intent on making a huge contribution to the world. This means that first and foremost we focus on `Results`. We trust that `Iteration` and `Transparency` will help us achieve results. And we believe that `Collaboration`, `Diversity and Inclusion`, and `Efficiency` will distinguish us from other companies in how we approach our work.

### Collaboration

GitLab outlines several 'sub-values' related to Collaboration, and they collectively articulate a set of practices that we also aim for and that will be essential to us developing a strong collaborative culture. There are two that we view as fundamental that will help lead to the practices articulated in the others. And these are "kindness" and "assuming positive intent." If we focus on these in spirit, they will guide us toward many of the others that are more behavior-oriented, such as saying thanks, not pulling rank, giving effective feedback, and not demonstrating ego.

In teammate onboarding sessions, we also have enjoyed the concept of "short toes" as a way to encourage us to help each other and not take offense at another teammate giving suggestions about any work that we ourselves are leading.

Great collaboration starts with showing kindness to ourselves. We can develop empathy - one of the building blocks of a collaborative culture, by accepting that our previous experiences have shaped our perspective and we can open up channels to new ways of thinking. We can practice this by sharing personal perspectives on the problem rather than on the merits of our “solution”. Like the parable of the blind men examining an elephant, each of us has a unique and relevant perspective on a problem.

We use a lot of text-based communication, and if you know the person behind the text, it will be easier to work well together. So we encourage people to get to know each other on a personal level through our [team visits](/people_ops/#team-meetings), through virtual coffees, or even meeting in person when logistics allow.



### Results

Results come from making commitments and then delivering on them.

We want to emphasize "customer results" over "plans" to do, what our scope or assumptions are, or even what a customer requests. We strive also to ensure that we validate our ideas against tangible progress, users interacting with the application, clients purchasing land or starting to build a home, ultimately people making the decision to move to a new city.

We also strive to measure results achieved rather than hours worked, acting with a sense of ownership, and accepting uncertainty. We like to "give agency" and "take ownership."

We aim to build alignment between personal accountabilities and our team, company, and customer goals. When setting personal goals, we want to communicate the impact we hope to have on our customer in a quantitive or observable way. When summarizing what we've accomplished, we want to emphasize how that achievement has impacted our customers or the lives of those who live in their cities.



### Efficiency

The first sub-value we want to emphasize around efficiency is the importance of writing things down. And especially writing them into some sharable and trackable format, like the handbook, an issue, or a public channel on Slack. Especially in an all-remote organization spanning many time zones, efficiency comes from each person being able to access and contribute to knowledge on their own time, and written communication is the cornerstone of efficient asynchronous collaboration.

We want to emphasize "boring solutions" as an aspect of efficiency. Complexity compounds and over time and will slow growth. We aim to align with well-worn technologies and approaches, and to find simple improvements to workflow and communications. Our goal is to bring together many reasonably good approaches into one coherent whole rather than to innovate conceptually new ways to do things.

Note that we don't fully ascribe to GitLab's notion to "spend company money as your own," especially because we all may have different personal values around spending money. We think it's more important to utilize an "investment mindset" with company money. Frugality is, of course, important. But we recognize that especially in our early days, we are investing and thus should think more about the results we can achieve per dollar spent (even if that's around personal productivity) rather than in frugality for its own sake.

### Diversity, Inclusion, and Belonging

We hold a shared belief that we can not succeed without putting diversity in all its forms – including race, background, and life experience – at the very center of how we build DoubleGDP the company. Aligned with our value of iteration, we see our organization as constant work in progress.

One of the GitLab sub-values that we most want to highlight for ourselves is to seek "culture add" instead of "culture fit" when considering bringing new people to the team. We want "values fit" with all new teammates, but want to broaden our perspectives, in addition to our collective skills, with each new teammate. Not only will this make DoubleGDP a more interesting place to work, but will better enable us to achieve results in the inherently global and multi-cultural new city market.

Every employee should feel valued at work, by their peers and their employer. It’s not about just opening the invitation to everyone, it’s about making sure that every individual knows and feels they are welcome at the organization. Inclusion requires active, intentional, and ongoing efforts to promote full participation and sense of belonging of every employee, customer, and strategic partner.

Another sub-value that we find particularly interesting is Gitlab's **bias toward asynchronous communication** within diversity and inclusion. This seems somewhat unusual in the context of D&I, but it in fact is an important practice to allow people who are in different timezones, or who prefer to work their own hours, to contribute effectively. It helps build a global culture, people working in different timezones, or people having control over their own working time within the day. And that there's not inadvertently an advantage for being based in one country or region.

We also agree with the importance that GitLab places on avoiding unfriendly comments about characteristics of people in order to help [foster a safe community](https://about.gitlab.com/handbook/values/#building-a-safe-community) for all.

We aim open-mindedness and pragmatism over ideology.

### Iteration

In particular, we have adopted more specificity around our practice of `iteration`. To us, this means taking the **simplest possible step** toward a large vision, represented as `Epics` in GitLab, that are a subset of the vision around which we can align.

The practice of iterations helps us bias toward action over discussion, and toward learning from observation of user behavior instead predictions of what the behavior will be. It also allows us to experiment quickly and allow a broad base of contributors. Trying one small step to see if it gains traction should be straightforward and inexpensive, and therefore we can try several good ideas rather than debating which of them is "best."

We have two specific practices that we've adopted toward or development process:

1. We only start sprint stories that are estimated at 2 days of dev effort or less
1. We do a single merge request per GitLab issue

More about how we apply iteration in practice can be seen in our [product development process](/engineering/productdev/)

Iteration is also important in how we think of our "handbook-first" processes. We want to be constantly updating our handbook with learnings, and in small increments. Consider this example: if we improve the collective knowledge in our handbook 1% week over week, in a while we will have a tremendous wealth of information that is well-organized, up to date, and searchable. Conversely, if we keep our knowledge in Google Docs rather than the handbook, over time the collective knowledge of the company will become searchable, but not organized or up to date and thus mostly useless. This is why it's so important that we're constantly updating the handbook, and with small steps consistent with the iteration value.

### Transparency

*Trustworthiness* is actually the higher goal here. Transparency helps to build trust. We are public by default, which fosters accountability, allows people to repeat what they've heard, enforces consistency of message among different stakeholders (and helps rein in the instinct to tailor our communications so that our audience hears what they want to hear -- a practice that would be very inefficient in an all-remote company where it may take 24 hours before you talk with that person again.)
- We also must be careful not to share sensitive material, as confidentiality is also important toward building trust. We follow Gitlab's guidelines for [not public](https://about.gitlab.com/handbook/communication/#not-public) and [limited access](https://about.gitlab.com/handbook/communication/#limited-access) information.

We like their articulation of "[disagree, commit, and disagree (again)](https://about.gitlab.com/handbook/values/#disagree-commit-and-disagree)" and think it's a nice extension of the more common idea of "disagree and commit." The key insight here is that one can be working to achieve results, even while aiming to reverse a previous decision that was made. However, to reverse a previous decision it's also expected to bring new information or perspective and to assume that the original decision was made with positive intent.

## Practices Associated with Specific Values

It's important that we utilize these values for inspiration, and also that we reinforce them with our practices. Here's a quick summary of behaviors that we associate with specific values.

- Collaboration
    - Every meeting has a shared agenda & notes doc. Type your name and main message prior to speaking.
    - Proactively engage in finding solutions
- Results
    - Focus on the external manifestation of the work rather than internal analysis -- e.g. a feature pushed to production, an email to a third party
    - Contribute with proposals and their rationale. e.g. "Here's what I think we should do and here's why"
- Efficiency
    - Leverage "handbook-first" and solicit input from others via merge requests
    - Use synchronous meeting time effectively
    - Send handbook links when sharing how to do a process
- Diversity (perspectives and timezones)
    - Solicit different point of views respectfully
    - Make information available asynchronously so that team members in different time zones may participate
- Iteration
    - Aim for a "minimum viable change" -- the smallest possible step toward addressing a problem, rather than to solve it entirely.
- Transparency
    - Share information with team and external parties
    - Engage in public channels on Slack
