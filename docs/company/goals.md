---
title: Goals and OKRs
---

# Company Goal

Our north star measure of progress is to increase revenue. (As of 2022-03-30.) See our [Twitter announcement](https://twitter.com/2xgdp/status/1509322562093780993?s=20&t=t2lLVlQ-_KjFLLZ2O6DCrg) and explanation for [why we switched to revenue](https://docs.google.com/document/d/1rjYfKQ2duw6Vr7_ROGHXNVBpVlsZzfRFoBHTPV9Dvdc/edit) from population as our north star. As of 2022-04-17 this is still a work in progress. See [Epic 272](https://gitlab.com/groups/doublegdp/-/epics/272) for the latest changes.

We will set growth goals shortly, and some references that will help us frame our ambitions are :  

- [Triple triple double double double "T2D3" approach](https://www.battery.com/powered/helping-entrepreneurs-triple-triple-double-double-double-to-a-billion-dollar-company/)   
- [Benchmarks](https://docs.google.com/spreadsheets/d/1IO5PP7vQSLWUuLZqQ_WY4tN_WgvmyxyntH3DjJbG9oM/edit#gid=1048847991) of fast-growing cities   
- [Week over Week rule](https://about.gitlab.com/blog/2020/05/05/wow-rule/) for setting targets at VC-backed startups   

<details>
<summary>History of our "North Star" goal at DoubleGDP</summary>
We established revenue as our goal on 2022-03-30 and archived previous sections of the handbook in [Issue 616](https://gitlab.com/doublegdp/handbook/-/issues/616). Prior to revenue, we used population as our north star, and prior to that we used "weekly active users" (WAUs). Here's context on why in 2020-10 we <a href="https://gitlab.com/doublegdp/handbook/-/issues/40">we switched to population from WAUs</a>.
</details>

## 2022 Company Goals and Plan   

Goals for 2022:    

1. Population goal -- 1000 by 2022 and 10,000 by 2023  
1. Grow new city partners to 12  
1. Establish a revenue model  

Plan to get there:

1. Grow headcount
    1. Engineering to ~17 (2 teams of 6 + 2 devops + 2 UX), based mostly in Africa and LATAM
    1. CS to 9 (1 for each 2 cities + 1 for Nkwashi), based mostly in Africa and LATAM
    1. Add Product (2) and People Ops (1), mix of US and Africa
1. Hold an all-team conference (covid-permitting)
1. Build strong product with special emphasis on land sales, workflow, and security use cases

## 2022 Company OKRs

### Partnerships

Below are the Partnerships OKR for 2022.  

**2022 Partnerships OKR**   

**Objective 1: Close 8 new cities as DGDP Partners in 2022**  

KR1: Sign up 2 of the current prospect city partners by 2022-03-31  

KR2: Ensure that the 2022 pipeline has no less than 24 qualified leads to provide a chance for success with Objective 1. Have 6 realistic prospects for Q1 2022 and 12 prospects for Q2 2022.    


**Objective 2: Increase partnerships pipeline by 3 every quarter**  

KR1: Re-establish contact with qualifying cities in our database and engage them with a goal towards contracting  

KR2: Identify new potential partners and add to the DGDP database with a clear progression plan  


**Objective 3: Create a robust Sales & Marketing process for use with all leads and prospects**  

KR1: Develop and document a Sales manual that DGDP will use in identifying and engaging leads  

KR2: Develop marketing collateral to be used with leads and prospects at different stages of the sales process   

KR3: Have a product Demo that clearly highlights Campaign management, Payments Invoices & Collections and Gate Access & Emergency Reporting.   

KR4: Put out two press releases on signing with Tilisi & Enyimba in January 2022.   


**Objective 4: Provide significant direction on development of a Revenue model for the company**  

KR1: Propose revenue models that will be evaluated to determine what aligns best with our overall mission and is sustainable  


_What does Partnerships need from others on the leadership team to be successful?_  

- Continue the support demonstrated so far in engaging prospective partners   
- Provide support on Product demos and product information necessary for collateral and pitches  
- Provide honest and prompt feedback on my input and effort towards these goals and the organizational goals.   

_What are the 1-3 biggest risks that are out of your control, but that collectively we might be able to address?_  


### Customer Success   

**Objective: Achieve a population of 1,000 residents across all DGDP cities in 2022**   

1. KR 1: Launch HIR-coding academy track at Nkwashi with a target population of 21 residents in 2022-January   

1. KR 2: Develop growth strategies for non-sponsored DGDP residents at Nkwashi by 2022-02-28   

1. KR 3: Achieve population of 20 residents at Tilisi by 2022-03-31   

1. KR 4: Achieve population of 32 residents at Ciudad Morazan by 2022-03-31   

1. KR 5: Setup onboarding plan with Enyimba by 2022-01-31   


**Objective: Set baseline for Customer Satisfaction Measurement**   

1. KR 1: Publish a minimum of one customer success story per quarter.    

1. KR 2: Define and launch customer satisfaction measures that will assess customers’ interaction with DGDP by 2022-03-31.     

1. KR 3: 90% completion of onboarding steps completed within the onboarding period (6 weeks)     


**Objective: Increase Product Adoption**    
(Work in progress to align Product OKR)    

1. KR 1: Define best practices and product adoption process and metrics by 2022-01-31   

1. KR 2: Define metrics and track news functionality adoption by 2022-02-28   

1. KR 3: Define metrics and track gate access functionality adoption by 2022-03-31    

1. KR 4: Define metrics and task functionality adoption by 2022-04-30   

**Objective: Increase Customer Success Team Productivity**   

1. KR 1: 100% CSM coverage for all DGDP cities by 2022-02-28    



### Product   

**Objective 1**: Launch of DRC and DLC  

1. KR 1: 10 tasks created, updated or/and closed weekly  
2. KR 2: 2 DRC or DLC forms are submitted weekly   
3. KR 3: Get a high CSAT score   

**Objective 2:** Develop CRM functions   

1. KR 1: Have one or more people at EECD use the CRM once a week.
1. KR 2: EECD Users must give a high satisfaction score to the DGDP CRM.

**Objective 3:** Improve Gate Access Flow    

1. KR 1: Ability to register 1 visitor at the gate under 30 seconds
1. KR 2: Ability to grant access to un-announced visitors in 1 minute.
1. KR 3: Ability to grant access to a resident or known community member in ____

**Objective 4:** On-boarding Greenpark in 4 weeks.   

1. KR 1: Greenpark using campaigns 1 sprint after kick-off meetings.   
1. KR 2: Greenpark publishes its own news articles 1 sprint after kick-off meetings.   
1. KR 3: Customers are comfortable using the application without DoubleGDP’s assistance after 4 weeks.  



*improve internal operations*    

**Objective 5:** Accelerate onboarding for new customers   

1. KR 1: Creating a customer in DoubleGDP under 10 minutes.   



**Objective 6:** Make our Product Management team even better    
1. KR 1: Hire HoP   

**Objective 7:**  Improve our Sales demos.   

1. KR 1: Get an internal feedback score of 8/10 on UX for news and campaigns redesign from the sales team    
1. KR 2: Get an internal feedback score of 6/10 on overall UX



### Engineering   

**Objective 1:** Improve the quality of our product  

1. KR 1: Reduce the number of new bug reports from 15 to 10 every 2 weeks



**Objective 2:** Improve the quality of the development process  

1. KR 1: Jest logs warning count to 200 warnings per run    
1. KR 2: Reducing test execution time from 23 mins to 15mins   
1. KR 3: Enabling eslint on all javascript code.    
1. KR 4: Hire one SRE.



**Objective 3:** Accelerate the development team speed   

1. KR 1: Reduce average code review time by 30%   
1. KR 2: Reduce the average number of bugs per feature by 50%.  


**Objective 4:** Continuously improve and grow as a team   

1. KR 1: Increase usage of the Engineering handbook section by having each engineer make 1 contribution to the handbook.

**Objective 5:** Improving Security Compliance   

1. KR 1: Fewer than 2 high or/and critical known vulnerabilities.     
1. KR 2: No more than 5 Medium, low, others.   
1. KR 3: Document process to resolve security issues    
1. KR 4: Successfully train 2 Engineers on security compliance  

[Product and Engineering Epics for 2022-Q1](https://gitlab.com/groups/doublegdp/-/epics/213)
