## Customer Council

As of 2021-Q3, we will focus on an [Executive Business Review](../customer-success/executive-business-reviews.md) with each account instead of a customer council. We think this will support a deeper dive on strategy and product roadmap with each of our accounts. We are considering replacing the former Customer Council process with an Advisory Council so that we can broaden its participation and engage more in market trends with less emphasis on our roadmap.

**Most Recent Meeting**
Here are the slides from our most recent [Council Meeting](https://docs.google.com/presentation/d/13a52PjcOV5izNKZUI8P7qtoPzu-W62TWPNyd3fVAnLY/edit) and here is the preview [video update](https://www.youtube.com/watch?v=kbXjuY2WqkA)
