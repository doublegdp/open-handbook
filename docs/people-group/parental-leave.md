## Parental Leave 

DoubleGDP offers anyone (regardless of gender) who has been at DoubleGDP for six months up to 16 weeks of 100% paid time off during the first year of parenthood. This includes anyone who becomes a parent through childbirth or adoption. The paid time off is per birth or adoption event, and may be used only within the first 12 months of the event.

We encourage parents to take the time they need. DoubleGDP team-members will be encouraged to decide for themselves the appropriate amount of time to take and how to take it. 

### How to Initiate Your Parental Leave

Some teams require more time to put a plan of action in place so we recommend communicating your plan to your manager at least 3 months before your leave starts. In the meantime, familiarize yourself with the steps below:

To initiate your parental leave, submit your time off by selecting the Parental Leave category in PTO by Roots at least 30 days before your leave starts. We understand that parental leave dates may change. You can edit your PTO by Roots at a later time if you need to adjust the dates of your parental leave. It's important that you submit a tentative date at least 30 days in advance. Your manager will get notified after you submit your leave and will send you an e-mail within 48 hours confirming that they've been notified of your Parental Leave dates.

Please note, even though we have a "no ask, must tell" Paid Time Off Policy, it's important that your Team Head is aware of your leave **at least 30 days before your leave starts.**


### Planning Your Parental Leave Dates

Your 16 weeks of parental leave starts on the first day that you take off. This day can be in advance of the day that the baby arrives. You don't have to take your parental leave in one continuous period, we encourage you to plan and arrange your Parental Leave in a way that suits you and your family's needs. You may split your Parental Leave dates as you see fit, so long as it is within the 12 months of the birth or adoption event.

If you don't meet the initial requirements for paid leave, DoubleGDP payroll coverage begins once you meet the requirements. Until then, you will not receive pay.  If for example, you are someone who qualifies after 6 months at DoubleGDP and then goes on leave at the start of your 120th day at DoubleGDP, you would not receive payment from DoubleGDP for the first 60 days. You would receive payment from DoubleGDP for up to 60 additional days taken within a year from the birth event.

You can change the dates of your parental leave via PTO by Roots. Your Team Head will receive a notification every time you edit your Parental Leave dates. Make sure your leave is under the Parental Leave category, otherwise your Team Head won't get a notification.

Please note, if you are planning to change or extend your Parental Leave by using a different type of leave such as PTO, unpaid leave or any local statutory leave, please send an e-mail to your Team Head.

### Administration of Parental Leave Requests

For Your Team Head:

  * PTO by Roots will notify Your Team Head of any upcoming parental leave.
  * Notify the team member that the parental leave request was received by sending a confirmation e-mail. .
  * PTO by Roots will automatically update the employment status. 
  * Check if the team member has confirmed their return via email. If not, send the Return to Work e-mail to the team member and manager.
  * Do a weekly audit of parental leave return dates. If a team member hasn't sent an e-mail within 3 days of returning, send them the return to work e-mail, copying the manager in.
