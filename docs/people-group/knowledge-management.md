# Knowledge Management


## Google Drive



1.   By default documents are viewable by everyone at the company. This means anyone with a DGDP email address can see the document if they have the link, but it will not show up in their search results.
     * When a document should have [limited access](https://handbook.doublegdp.com/company/communications/#limited-access), adjust the document to `Restricted`. Do this by selecting `Share` --> `Change` under "DoubleGDP Anyone in this group can view" --> `Restricted` from the dropdown.
1.   Documents should use these prefixes:
    1.   [public] - open to everyone, even outside of dgdp
    1.   [dgdp] - available to all at dgdp (assumed if not labeled)
    1.   [dgdp-only] - available to dgdp team, but not externally (useful for customer notes documents that one might otherwise think are viewable by customer team)
    1.   [shared] - used to indicate that the document is viewable both by dgdp and customers
    1.   [restricted] - shared, but only to specific individuals. May also want to adjust the sharing settings
    1.   [private] - might be useful to put this for any files that you wish to keep to yourself.
1. To use documents that are stored in a shared drive, it is helpful to create a link to that shared drive from your personal drive. To do this:
    1. Head to [drive.google.com](https://drive.google.com)
    2. Click on “Shared with me”
    3. Tap the name of the folder you want to add to your Google folder
    4. Click on the name of the folder
    5. Click on “Add shortcut to drive”

### Share with all@ by default when you share a Google document
Generally speaking, when you share a document share it with [all@doublegdp.com](mailto:all@doublegdp.com). We want to allow anyone to contribute to all company work products, and this ensures that when someone needs to access it they can. (There may be some specific cases where information or access rights need to be more restrictive, but please do it sparingly.)

## Airtable CRM



*   We love sharing knowledge! Please post articles pertinent to our learning about the market to our publicly-available knowledge base here: [https://airtable.com/shrZGPMbZRH4DjTML](https://airtable.com/shrZGPMbZRH4DjTML)
    *   Please share your name in the “Who are you?” dialog, unless anonymity is important to you in sharing.
    *   Ask your friends to share things too, though they don’t need to put their name :)
    *   Sharing here will add it to our repository (viewable on our [website](https://www.doublegdp.com/progress/5_WhatWeAreLearning/)) and create a post to our internal #market-insights channel in Slack
*   We also keep track of companies, cities, communities and people who are relevant to our market learning. That’s kept in Airtable directly and not available to the public: [https://airtable.com/tblI8yl7CDFKZc1D2/viwpAFYrRP3u1ze94?blocks=hide](https://airtable.com/tblI8yl7CDFKZc1D2/viwpAFYrRP3u1ze94?blocks=hide)
    *   Airtable will only trigger a notification to Slack if a new Contact or Community has been added into the CRM, or if a contact/community has moved to a new stage in the pipeline.
    * For all other activity (for instance, data added on # residents, or information added to the description of the contact, etc), you'll need to go directly to the CRM to find the information there.
    * As a practice, if there are key insights about a city that you think everyone should know, first capture that information in the appropriate system of record (if it's a high-level description, directly into Airtable...if it's detailed notes from a discussion with an account/contact, in a Google doc for that account that is linked to the Airtable record)...and then select a few insights to copy and share more widely with the DGDP team in Slack.
    * The partnerships team will summarize a few key insights to share with everyone during our bi-weekly sprint update videos, every two weeks.
    *   Note that we’re using the free version of AirTable and limited number of licenses, so we don’t by default make this available to all teammates. But if you’re interested in browsing let us know and we can accommodate.
