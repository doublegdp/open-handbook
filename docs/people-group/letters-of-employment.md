## **Letters of Employment & Full-Time Contractor**

Please submit your Letter of Employment / Full-Time Contractor to [peopleops@doublegdp.com](mailto:peopleops@doublegdp.com) with the following information:

*   Example Letter from the entity to be presented to (Financial Establishment, School, etc.) or complete the template [Here](https://docs.google.com/document/d/1CKaUuErGZX0_kFtgfgyN6QdG4aA9PEk26d0kgZU2UaI/edit)

Your request will be reviewed and returned to you within 72hs of submission.
