## **Work Availability and Paid Time Off**

We emulate GitLab’s “don’t ask, must tell” policy, and encourage people to take the time off. It's up to each teammate to choose what days will work best for them, but we generally encourage you to take your national Holidays, in addition to some long weekends and vacations.

References that may be helpful:

* [GitLab's Paid Time Off Policy](https://about.gitlab.com/handbook/paid-time-off/)
* [GitLab's Guidance on Mental Health](https://about.gitlab.com/company/culture/all-remote/mental-health/)

### How to Take Time Off

You can take time off whenever you want. However, please be responsible in when you take it and how you communicate it. Here are a few things to consider:

1. Check with your manager to try to avoid being out around a critical milestone or at the same time that others on your team will be out
1. Try to provide advance notice -- a general guideline would be to share twice as many days in advance as you'd like to take off. e.g. two days' notice is for one day off; two weeks in advance would be better if you want to take a full week.
    * Note that this does not apply for emergencies, and is not a requirement. Sometimes fun things come up last minute that you want to do. But this guideline will help prevent leaving others in the lurch when you have some flexibility in scheduling.
1. Team members who are with us through an agency should also follow that agency's procedures in addition to ours.
1. If you will miss the sprint video update, please record one in advance so that others will be aware of what you contribution during the sprint
1. Being part of a global remote team means you need to be highly organized and a considerate team player. Each team has busy times so it is always a good idea to check with them to ensure there is adequate coverage in place.
1. Engineers should ensure the following are in place before going on leave:
    - All tickets assigned to you are completed, verified and closed
        - If there are pending tickets, discuss with an engineer who can take up while you are gone
    - There are no merged requests assigned to you that are not merged
    - If you are taking a longer leave, communicate with the team few days prior to your leave


#### Absenteeism

Our time off policy is flexible, but requires proactive communication. Any team member who takes 3 consecutive days without prior notification will be considered to have voluntarily resigned.

If someone is absent, the manager will make effort to contact the team member or someone listed as their emergency contact. Please share [emergency contact information via this form](https://docs.google.com/forms/d/e/1FAIpQLSfabH3dmyzT1jfI6wvc-gJ-1noyqK53QF7vTlNK7dWAj4Nnyw/viewform?usp=sf_link) with people-ops, so that we know who to call in case of an unexpected absence.

Managers can find information on who to call in our [Emergency Contact Information Sheet](https://docs.google.com/spreadsheets/d/1h1QcknDamZukc0OOoG35W9BEm3q9vTMUJi8nBSrbLB0/edit?usp=sharing)

### Communicating Your Time Off

All time off for a full day or more must be communicated via PTO by Roots in Slack. This includes national holidays, vacations, or travel days. Here's how to communicate time off:

1. Add the time off using [PTO by Roots](#pto-by-roots) to Slack. Please use the following syntax which uses the [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) date notation in order to avoid confusion: `OOO from YYYY-MM-DD to YYYY-MM-DD, please contact XXX for assistance`.
1. Mark the time as `Out of office` on your calendar.
    1. Select the `Automatically decline new and existing meetings` option so that your meeting status will show "declined"
1. If you won't check email at all, add an out-of-office response in your email settings with the start and end date of your time off so that people reaching out to you on email will be notified
1. It may be useful to share your planned time off as a **FYI** on your team's Slack channel(s), especially if you lead a team.

### PTO by Roots

[PTO by Roots](https://www.tryroots.io/pto) allows employees and managers to coordinate time off seamlessly with intuitive Slack commands. The integration from Slack automatically monitors PTO balances and takes action. PTO by Roots also reminds employees of their upcoming time off and helps them assign roles and tasks for co-workers, giving all parties involved greater alignment and peace of mind.

[PTO by Roots Training Video](https://drive.google.com/file/d/1t-rRHvWxo9IguvtHuacR0pAlK3nHWS4H/view?usp=sharing)

Please be aware that new team members' PTO by Roots accounts are activated and synced automatically once a day, so if you try to access PTO by Roots on your first day and receive an error message, please attempt to access PTO by Roots on Day 2. If you still receive an error message, let your assigned People Ops Specialist know in your onboarding issue.

If you receive an error message **"Uh-oh! It doesn't look like you have access to PTO by Roots"** please attempt the following steps.
1. Check to see if your email address (DoubleGDP) in your slack profile matches your email address.  
1. If these two are not the same exact email then the application will not recognize you as a team member.
1. If these two are the same, please reach out in the `#peopleops` channel.

#### Slack Commands
* `/pto-roots ooo` Create an OOO event.
* `/pto-roots me` View your OOO dashboard to edit, add or remove OOO events.
* `/pto-roots whosout` See upcoming OOO for everyone in the channel where this command is used.
* `/pto-roots @username`  Check if a particular person is OOO and if they are, see which of your co-workers are covering for them.
* `/pto-roots feedback` This is your direct line to support. At any time, use this command to report bugs or share your thoughts on how the product can be improved or what’s working well.
* `/pto-roots help` A top-level look at PTO by Roots items that you may need help with, and how to receive help.
* `/pto-roots settings` This is where you modify your profile and calendar settings. This is also where you opt-in and out-put for reminders, including monthly messages prompting you to consider what PTO you may take this month.

#### Additional Features

##### Google Calendar Sync

PTO by Roots allows you to sync time off requests with your Google Calendar.

##### Automatic Status + Do Not Disturb Updates

PTO by Roots can automatically set your OOO status in Slack and apply “Do Not Disturb” settings while you’re away. You must add these permissions individually.

##### Roles and Task Handoffs

PTO by Roots provides an experience that allows you to set roles and tasks for co-workers while you’re away. Accepted roles then become a part of a Slack away message that appears as co-workers try to tag you in discussions while you’re OOO.

##### Bulk Add holidays

You can bulk add holidays based on your region and then modify them manually if needed. Any Public Holidays added to PTO by Roots in your dashboard will not auto-set an OOO event for you, you will still need to create an OOO event if observing that holiday. To add a Public Holiday, follow the below instructions.

* Type `/pto-roots me` in Slack
* Choose `Holidays` from the `Upcoming OOO` dropdown menu
* Choose the desired year
* Click on `Bulk Add By Region`


### How to Add Bulk Time Off

We encourage you to take the national holidays in your home country.  We've made this easy by providing a calendar with those dates so that you can add them in bulk.

1. Visit our [Important Dates Spreadsheet](https://docs.google.com/spreadsheets/d/1POhDzesk5fEjiafkWxJmsFnjbY5F_c--0fmFdSiO2vE/edit#gid=882612580) and see the tab for your National Holidays
1. Create a local CSV file of that tab, using the menu option `File: Download: CSV`.
    * After you create a copy, feel free to add or remove dates as fit to your circumstances
    * Note that holidays that occur on weekdays have start and end times, so that importing them will block off your calendar
    * [More instructions](https://support.google.com/calendar/answer/37118?co=GENIE.Platform%3DDesktop&hl=en) are available from Google
1. Import into your Google Calendar using its `Settings: Import & export` feature
1. Confirm it added the dates properly

Note that you should notify your manager when you opt which dates to take and if you made an deviations from these defaults. It's not necessary to email all@ if you use these default dates.
