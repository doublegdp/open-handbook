---
title: 360 Feedback
---

## 360 Feedback

Starting in 2021-03, we are beginning a team-member review cycle to provide feedback on performance and growth. Some important context:

- We're starting with reviews of only two team members: Head of CS and Head of Engineering (this is in order to be iterative and practice the process)
- We're using Culture Amp to request and collect feedback
- Our aim is to have the process complete by 2021-03-31

If you want more context or back-story, the [Company Goals page](https://handbook.doublegdp.com/company/goals/#company-goal) discusses our performance management framework and philosophy, and our specific goal for this round is outlined in a [GitLab ticket](https://gitlab.com/doublegdp/handbook/-/issues/156).

After this first iteration we will get input on the process and roll it out for the rest of our team members. Follow [this OKR](https://gitlab.com/doublegdp/handbook/-/issues/168) for more.

Process to complete this cycle:

1. Head of CS and Head of Eng will nominate people to provide feedback on them. They will focus on those with whom they've worked directly. [**Due: 2021-03-27**]
    - If you don't receive an invite but want to provide input, please reach out to EA and ask to be included
1. Reviewers enter feedback in the system. We expect this process to take 15-20 (and not more than 30) minutes per person, so please don't overthink it. This feedback will be sent to CEO to incorporate into their reviews. [**Due: 2021-03-29**] Note: we're focused on 3 questions:
    - What's working well that they should continue?
    - What can they improve upon?
    - How well do they [exhibit our values](https://handbook.doublegdp.com/company/#behaviors-associated-with-specific-values)?
1. CEO will compile results and share with these team members. [**Due: 2021-03-31**]

Please contact EA if you have questions on how to give the feedback or utilize Culture Amp.

### Performance review calibration meeting

Prior to delivering feedback, the two managers -- Head of Eng and Head of CS -- will meet with CEO to calibrate approach to performance review. This is a 25 minute meeting. In advance, each manager will share a team list with each person's assessment of "Excellent", "Meets Expectations", "Needs Improvement".

We will not discuss those who meet expectations. We will talk through each person who was rated as Excellent, Needs Improvement. The manager should have ready a 1-minute summary of why, followed by 2 minutes of input from the other two participants. The discussion should be focused on whether the assessment of performance seems reasonable and whether the rationale is consistent across teams and with our company values.

## All DoubleGDP - Company Performance Review

Starting on April 2021 we are beginning a team-member review cycle to provide feedback on performance and growth. Some important context:

- We're using [Culture Amp](https://www.cultureamp.com/) to request and collect feedback.
- Peer feedback request should be completed by 2021-04-16.
- By 2021-04-27 Managers have written performance reviews for their teams and discuss with CEO
- Managers have delivered performance reviews to team by 2021-05-04
- EA Receives feedback on process and refine in handbook for following year.

## All DoubleGDP - Company Performance Review 2022

- We're using [Culture Amp](https://www.cultureamp.com/) to request and collect feedback.
- **2022-03-22**  Team members start a self-reflection.
- **2022-03-31** Team members will receive an email from [Culture Amp](https://www.cultureamp.com/) asking to nominate peers for feedback and share it with their manager.
- **2022-04-01** The manager selects peers reviewers from the list provided.
- **2022-04-04** Peers receive feedback requests and submit feedback.
- **2022-04-07** Team members complete a self-reflection.
- **2022-04-11** Managers gather feedback and self-reflection and write an evaluation.
- Managers review feedback with the CEO and before 2022-04-15, present constructive feedback and ratings to their employees.

We are focused on the following areas:

1. How did this person impact you or your team's performance?
1. What are 1-2 things you’ve observed this person grow or excel at that they should continue?
1. What’s one thing this person could improve upon or get additional coaching on?
1. [Values: Exhibits Collaboration](https://handbook.doublegdp.com/company/#practices-associated-with-specific-values)
1. [Values: Delivers Results](https://handbook.doublegdp.com/company/#practices-associated-with-specific-values)
1. [Values: Operates with Efficiency](https://handbook.doublegdp.com/company/#practices-associated-with-specific-values)
1. [Values: Allows for Diversity of perspectives](https://handbook.doublegdp.com/company/#practices-associated-with-specific-values)
1. [Values: Iterates on solutions](https://handbook.doublegdp.com/company/#practices-associated-with-specific-values)
1. [Values: Demonstrates Transparency](https://handbook.doublegdp.com/company/#practices-associated-with-specific-values)

Note: In this period we will evaluate those who have at least 2 months in the company.

Please contact [EA](mailto:cecilia@doublegdp.com) if you have questions on how to give the feedback or utilize Culture Amp.
