## **Benefits**

In this section, we describe what we offer to our Team Members; our default policy is to provide benefits for all team members:

DoubleGDP covers 80% for team members and 50% for dependent of the Health Plan Premium monthly costs.

* US-based team members can select their benefits through [Gusto](https://gusto.com/) AmTrust is the worker's compensation provider, and the [policy](https://drive.google.com/file/d/1SFzX1v4sQHIWqMCPgYgyA0fXHrYgkWK3/view?usp=sharing) is connected to DoubleGDPs Gusto account.
    1. Your internet bill
    1. Your cell phone bill
    1. Any other recurring expenses that are aligned with our policies.




* Non-US based team members receive a monthly stipend of **USD 300** to cover for:

    * A premium healthcare plan. (We aim for the second-best available.)
    * Internet fees
    * Cell phone bill
    * Bank fee deductions from your paycheck
* For all team members, we offer:
    * All-remote work
    * Coverage of internet and phone, if used primarily for work
    * Home office setup (see [workstation setup](/people-group/workstation-setup/))
    * Access to coworking space, if desired


### Stipend for non-US team members

The stipend is intended to make it easy for you and for the company to administer the benefits. Thus we've made a "default" of the payment amount listed above, which can be increased upon request and with showing appropriate receipts. To receive the default amount you don't need to do anything; it will be included by default in your monthly paycheck.

If this does not cover your monthly expenses, you may opt to share the details of your expenses, and we will adjust your stipend accordingly. To do this, please share the following information with [peopleops@doublegdp.com](mailto:peopleops@doublegdp.com):

1. Your healthcare bill. We'll calculate the stipend based on 80% for team members and 50% for dependent.
1. Your internet bill
1. Your cell phone bill
1. Any other recurring expenses that are aligned with our policies.

We'll recalculate and adjust your default monthly stipend accordingly.


For countries where health plans must be paid upfront for the year, we will be pay 80% for team members and 50% for dependent upfront.  The upfront amount paid will be deducted from the yearly total stipend amount.  The remainder will be divided by 12 to calculate the monthly stipend amount.  The new monthly stipend will be disbursed monthly.


For example:  If an insurance require to pay $1500 upfront for an individual, DoubleGDP will pay $1200 upfront (80% of $1500).  The monthly stipend will be $3600 (yearly stipend) minus $1200 ( what DoubleGDP paid) divided by 12.  In this case it would be $200.
