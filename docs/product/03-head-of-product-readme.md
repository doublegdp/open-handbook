---
title: Head of Product Readme
---

## Intro
My name is [Craig Simoneau](https://www.linkedin.com/in/craigsimoneau/) and I am the Head of Product at DoubleGDP. This page is intended to serve as a helpful guide to who I am and how I work and interact with others (*both where I think I excel and where I feel I need some improvement*). I invite you to use this page as a resource and an invitation to provide feedback and suggestions for me.

## Bio
I joined DoubleGDP in February 2022 after spending a couple of years in the ecommerce industry at [BuySafe](https://www.buysafe.com/about/). Prior to that, much of my experience was centered in the real estate and proptech industries. I briefly held a real estate sales license while I was in college before working at [Bisnow](https://www.bisnow.com/about), a commercial real estate events and publication company. I then joined the founding team of [FiveStreet](https://www.fivestreet.com/) building CRM and other SaaS productivity tools for residential real estate agents and brokers there and at [Realtor.com](https://www.realtor.com/about/) following its acquisition of the company. 

## Why DoubleGDP?
I jumped at the opportunity to be part of the DoubleGDP team because I believe very strongly that the places we live, work and spend our free time are most directly correlated to our enjoyment and outcomes in life. More than ever, technology plays an integral role in our interaction with these physical spaces and our ability to build connection in our communities. [DoubleGDP's mission](https://handbook.doublegdp.com/company/#mission-statement) highlights our commitment to helping everyone have access to communities where they can thrive and I am very grateful to have the opportunity to help drive that mission forward. 

## Communication 
Some of my known strengths and weaknesses that may be helpful to keep in mind when communicating with me:

- I prefer expressing my thoughts verbally, but I have a tendency to be quite verbose or cause the topic of discussion to wander.<br> 
*Feel free to say something like "Can we get back to the original topic?" or "Can you give me a summary of your thoughts instead?"* 

- I consume information best when I can slow down and repeat to process it at my own pace. This includes written text and recorded audio or video.<br>
*If you are like me and prefer to convey your thoughts verbally, feel free to send me a recorded [Loom video](https://www.loom.com/home) or similar audio/video clip.*

- When I am thinking, I sometimes look around the room or drop eye contact.<br>
*I know this can be distracting. I won't be offended if you remind me to shift my attention back to you*

- I often relate in conversation by using anecdotes from my personal life (and I often have no shortage of them). <br>
*While my intention is to better connect, this can often distract from someone else's point or make it seem like I only want to talk about myself. I promise that is not my intention. Feel free to steer the conversation back to your point.*

- When I am very engaged in a conversation, I will sometimes interrupt, blurt something out, or try to wrap up someone else's point for them.<br>
*I'm sorry, I know this is very rude. I think I do it out of excitement and to build on a point someone is sharing, but unfortunately it signals exactly the opposite. Please either continue talking, or politely remind me to let you finish first.*


## Personal Interests
- I have two dogs (they apologize in advance if their chaos occasionally disrupts a call) 
- I enjoy spending time outdoors while hiking, skiing or kayaking. 
- Every few years, I buy a guitar and attempt to learn how to play, but it usually starts collecting dust in a corner pretty fast. 
- I consider myself relatively handy. My wife and I spend a lot of our free time working on DIY house projects. 
