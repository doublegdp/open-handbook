---
title: Product Communications
---
## Product Documentation
Product documentation is located in a [gDrive folder](https://drive.google.com/drive/u/1/folders/1oHUmTsiw3GV_vrILXbuMKLDWBEY-0U-o).

## Product Roadmap
The [product roadmap](https://gitlab.com/groups/doublegdp/-/roadmap?state=opened&sort=start_date_asc&layout=MONTHS&timeframe_range_type=CURRENT_YEAR&label_name%5B%5D=GOAL%3A%3ARoadmap&progress=WEIGHT&show_progress=true&show_milestones=true&milestones_type=ALL) provides visibility into features and improvements that are we are considering as we build toward our broader product vision. Each of our engineering sprints is made up of a number of stories (small changes or improvements) and epics (larger features that are often an assembled group of smaller stories). These stories and epics are often the result of use cases and feature requests we hear directly from our subscribers.

## Feature Requests & Bugs
We firmly believe that the most helpful form of guidance we can receive is in the form of use cases, feature requests, and bug reports received directly from the communities we work with. This often comes from city admins, but can even come directly from community residents As an example, we received [this new feature request from  a resident, Blessed Jason Mwanza,](https://gitlab.com/doublegdp/app/-/issues/2506) that we implemented just two sprints later.

If you would like to suggest a feature, or report a bug please reach out your customer success manager or [submit a bug request directly to our engineering team via our service desk process](https://handbook.doublegdp.com/engineering/production/support/#submitting-a-service-desk-request).

Our team members also collect use cases received from customers and document them into epics through the process outlined below. Feature requests that stem from conversations with prospective customers are collected by our team using the [process outlined here](https://handbook.doublegdp.com/customer-success/cs-sales-process/#tracking-product-feature-interest-feedback) before eventually making it to the product roadmap.

## Transparency & Confidentiality
Our goal is to be as transparent as possible with our product development process as we believe doing things in the open enables us to move quickly and collect as much input as possible from everyone involved. However, we acknowledge that our partners may prefer to keep the some of the specifics of their business practices and employee information private in certain situations. To balance these needs, we use the following process when documenting items on our public Gitlab issues and epics.

- As much as possible, we document use cases in a way that should make sense to a client or resident if they are reading our roadmap.
- We write use cases with generic terminology and avoid mentioning clients and their employees by name.
- If discussion of confidential information is necessary to best convey the request, it should be referenced in a link to a document that is only visible to DoubleGDP team members (such as the notes document from a call where the feature was discussed).
- If additional confidentiality is required to document the work item, the issue or epic should be marked as confidential in Gitlab (Note: We strive to use this as sparingly as possible to enable maximum transparency of our roadmap).
- For internal purposes, we use [Part: Labels](https://handbook.doublegdp.com/product/04-prod-communications/#part-labels) so our team can affiliate the epic or issue with the correct partner.


## Creating Epics
- Epics can be created by any member of the DoubleGDP team.  
- Epics must have at least a Goal: and Dept: label.
- Goal:Roadmap epics must have a start and due date that aligns with an upcoming sprint.

## Labels

### Goal: Labels

**Goal: Triage**![image-6.png](./image-6.png)
The [Goal::Triage](https://gitlab.com/groups/doublegdp/-/epics?state=opened&page=1&sort=start_date_desc&label_name[]=GOAL::Triage) label is for all new feature requests or improvements under consideration and can be created by any team member. Please start them by using this [epic template.](https://docs.google.com/document/d/1LCc1rIIAMYLNtSHHcH2fWBKxIV2aKHeBN-VJSeVnFEw/edit?usp=sharing) Items on the template that will need to be filled out before any engineering work can be done are:


1. **Description**  — A general overview of feature or improvement in a sentence or two.
2. **Business Use Case** — How does this benefit the business/client. It’s very important to include in the use case a clear description of the problem the client is having, why doing what they are attempting is important to them and how improving this will help their business. It's ok to include what the client thinks the solution is but a clear and detailed Business Use Case is critical to help the engineering team understand how and why it’s being used so that they can come up with the best solution for all of our users and build the right thing.
3. **Users** — Who will use this.
4. **Required Acceptance Criteria** — What it should do. This is important for the engineers know what should be included and to create a "definition of done" so we as a team know when this version of the feature or improvement is completed and the epic can be closed.
5. **Nice to Haves** — things that would enhance the feature or improvement,  if time allows but are not critical for release.

Once these items are filled out, please “@" the Head of Engineering in the comments on the epic so can be moved to engineering review which will trigger a design phase and/or a decomposition phase where tickets are created. The epic may be moved to either Roadmap or Initiative depending it's on scope. If it is moved to Initiative, you may be asked to provide additional information for the sub epics.

Triage epics should not be assigned a start or due date. This will be done when it is moved to Roadmap by the Head of Product and/or the Head of Engineering.

**Goal: Roadmap** ![image-5.png](./image-5.png)
Epics that have the label [Goal::Roadmap](https://gitlab.com/groups/doublegdp/-/epics?state=opened&page=1&sort=start_date_desc&label_name[]=GOAL::Roadmap) are epics that describe a use case that can be accomplished in a sprint or two at most. The use case is well defined and we believe we have enough information to move forward with design and engineering work.

All epics with the Roadmap label should be assigned to an upcoming sprint by setting the start date and due date of the epic to match the sprint dates in the epic property menu. Epics cannot be scheduled in the current sprint and should only be added to upcoming sprints. If you have an urgent request that you feel should be addressed in the current sprint, please bring it to the attention of the Head of Product and Head of Engineering for consideration.

**Goal: Initiative** ![image-4.png](./image-4.png)
The label [Goal::Initiative](https://gitlab.com/groups/doublegdp/-/epics?state=opened&page=2&sort=start_date_desc&label_name[]=GOAL::Initiative) is designed for longer-running initiatives or projects that may span more than two sprints and contain several, smaller Goal::Roadmap epics. This allows us to view, group and track work together into larger initiaives without having unending projects on our visual sprint backlog. The only time an epic should have multiple GOAL labels is when it aligns with an Initiative in addition to a Roadmap or Triage epic type.

### Roadmap Status Labels
These scoped labels are intended to show the status of the Goal Lables on their path to being on the Roadmap. They can be used to assess the status of an epic on the [Epic Development board](https://gitlab.com/groups/doublegdp/-/epic_boards/2376) or reviewed on the [Epic Roadmap Status board](https://gitlab.com/groups/doublegdp/-/epic_boards/26084) for a more granular approach.

**Roadmap Status: Planning**
This label means that the epic is in the planning stage where ACs are gathered and questions are answered. Support documents would be created and posted to the epic in this stage like User flows, spreads sheets or research results.

**Roadmap Status: Design**
This label means that the epic is in the queue for mockups, wireframes or prototypes.

**Roadmap Status: Review**
This label means that the epic is that mockups, wireframes or prototypes are done and waiting for review from any of the following: internal or external stakeholders for alignment, engineers for feasibility final check. At this stage the epic will either get sign off and move to decomp or need revisions and revert to design or planning.

**Roadmap Status: Decomp**
This label means that the epic is ready for an engineer to break it into small, doable pieces that would allow more than one engineer to work on the epic simultaneously. This stage is done in collaboration with UX Designers to make any minor adjustments or additional screens and answer questions. Major adjustments would revert to Planning or Design depending on the severity. Since the engineers review this at several stages, reverting at this point should be very rare.

**Roadmap Status: Ready**
This label means that the Decomp is completed and the epic's tickets are ready for active development in a sprint.  

### Dept: Labels
We use Dept: labels to track which department or area of the organization is the primary requester of the use case being tracked in the epic. This enables us to track where our features and improvement requests have initiated. It is acceptable to assign multiple Dept labels to an epic.

![image-1.png](./image-1.png) ![image-2.png](./image-2.png)![image-3.png](./image-3.png)

### Exp: Labels
We use Exp: labels to track the role or user experience that will primarily benefit from the use case described in the epic. It is acceptable to assign multiple Exp: labels to an epic.

### Module: Labels
We use Module: labels to track the module of the DoubleGDP application that use case described in the epic will primarily impact. This enables us to track which modules are seeing the most improvements over time. It is acceptable to assign multiple Module: labels to an epic.

### Part: Labels
We use Part: labels to track which partner a particular epic or use case applies to. This allows us to speak generically on the public Gitlab issue so that our roadmap can be as visible and transparent as possible, while also allowing us to link a particular work item to a particular client. DoubleGDP team members can access a [legend of the various Part: Labels we use](https://docs.google.com/document/d/1VbI4CfmJMzJVtb-XEqYT9UN4ZlPi_Dbh2aL8qLAQ4oY/edit?usp=sharing)

### Priority: Labels
We use Priority: labels to identify the urgency of a particular epic or work item. Issues are triaged and prioritized in terms of severity. There are several priorities we use:

- **Priority: 0** - Generally applies to bugs that have impacted all of production and are preventing a large segment of users from accessing the application. These are "all hands on deck" situations.
- **Priority: 1** - Issues that are severe enough that they have to be fixed within the current sprint or [improvements that will have a high impact for prospective customers.](https://handbook.doublegdp.com/customer-success/cs-sales-process/#tracking-product-feature-interest-feedback). A story may have to removed from the sprint if the Priority:1 issue takes more than 4 hours to resolve.
- **Priority: 2** -  Issues that can be addressed in the following sprint or in a later sprint. P2 issues are prioritized by the product team and are treated the same as stories during Sprint Planning.

## Product Sprint Updates
We plan for and complete our product development work on a [two week sprint cycle.](https://handbook.doublegdp.com/engineering/team-works/sprints/#sprint-planning-process)

[Product sprint update videos](https://handbook.doublegdp.com/engineering/team-works/sprints/#sprint-demo) are recorded every two weeks along with the rest of the company and are posted publicly on our [Youtube channel](https://www.youtube.com/channel/UCALY7l5iisNVrEyvLgQa3ig). Product Management's [sprint update presentation](https://docs.google.com/presentation/d/1iVlDBLg_zCipZUwMRbj4k6MZUD5-42Q7n54sMxUH4KY/edit#slide=id.gbaec4e76d8_0_13) is available publicly.

The product sprint update includes the following:

- High priority areas of product focus for current sprint
- High priority areas of product focus for upcoming sprint
- Head of Product's explanation of the "why" behind these focus areas
- Metrics or stats to show adoption and use in product priority areas

Note: Product management sprint work focuses on planning for features and improvements that will be built in later sprints by the engineering team. Often we work 1-2 sprints ahead of engineering as priorities shift.

## Product Update Blog Posts
[Product update blog posts](https://www.doublegdp.com/blog/) are published approximately every 6-8 weeks on our website by the Head of Product. These posts are intended to give a bit more detail around "why" behind features that are being explored, are actively in development, or are being released. We hope this helps highlight to our current and prospective partner communities what we are working on and the motivations behind the feature.
