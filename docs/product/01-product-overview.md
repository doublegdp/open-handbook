---
title: Product Overview
---

## Introduction

DoubleGDP is an end-to-end platform for planned communities to connect with residents, accelerate growth, and deliver responsive public services. Residents use our one-stop portal to access services and interact with their neighbors to foster civic engagement, encourage social connection and build thriving communities. Community administrators utilize our central hub of purpose-built productivity tools to manage all the processes necessary to launch, grow and sustain the communities of the future.

## Core Value Propositions

While our platform offers a versatile featureset that is capable of serving the needs of planned communities at all stages of development, its core value stems from the unique ability to help customers:

 1. Market their city and sell its land
 2. Streamline administrative processes and eliminate red tape
 3. Build connection between residents, community members and administrators
 4. Create a unified digital hub for traditionally siloed services

## Product Vision

This diagram provides a high-level overview of how core DoubleGDP productivity tools, third-party software solutions, and custom-built apps can work together to meet the unique needs of planned communities big and small.

![Platform as a Hub](/img/company/platform-as-a-hub.png)

## Pricing & Features
We have three product tiers available to subscribers. Pricing and features for each tier are available [on our website](https://www.doublegdp.com/pricing/).

Our [internal pricing spreadsheet](https://docs.google.com/spreadsheets/d/152D-u_z9lHu7xn10-oqHaFtrVp8aojMjjPSuOjh6qsw/edit?usp=sharing) has more detail about the history of our pricing and how it relates to our company goals.
