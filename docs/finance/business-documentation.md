## Business Documentations

Important business documents, such as business registration, tax documents, business insurance, business letters, and various agreements, etc. are required to be documented in the DGDP Google Drive. \
DGDP team members are required to forward important documents to [Accounting@doublegdp.com](mailto:Accounting@doublegdp.com) for documentation. The accountant in the Accounting team is responsible for the documentation and is expected to complete the documentation within 2 business days from the receiving date. How to use the Main Folders under DoubleGDP LLC G-drive



*   **Taxes:** all tax-related documents should be saved here. Examples: Income Tax return, 1099 Filings, CA State Tax Filings, R&D tax credits, etc.
*   **People Operations:** all employment-related documents. Examples: Worker's Compensation, Labor Compliance, Employment Agreement, Contractor Agreements, etc.
*   **Insurance:** all business insurance documents. Examples: [Hub](https://drive.google.com/file/d/1WlSU4oXH0hV2Ow7FOKXg1A-CRnisQftX/view?usp=sharing), Guardian, etc.
*   **Business Letters:** important business letters. Examples: Enyimba Economic City Letter Nolan forwarded to Accounting
*   **Business Formation/Registration/Renewal:** business registration documents. Examples: EIN letter, CA EDD documents, Registration Renewals
*   **Business Agreement:** business agreements. Examples: Ciudad, Customer Testing agreement
*   **Accounting:** The files that Accounting actively manages. Examples: Accounting month-end close files, Bank Files, Bank statements, and all other confidential documents
*   **Vendor Agreements:** important vendors and vendor agreements. Example: agreement associated with Ufudu, Thebe, etc.

    Try to maintain documents based on these main folders. Sub-folders can be created as needed. If a new main folder needs to be created, please contact Controller as well as the CEO's Executive Assistant for creation.


**Important Dates:**

Accounting is responsible to coordinate and manage these important dates and make sure they are completed  on time.



1. CA LLC Secretary of State renewal - Due every two years by the formation anniversary date (DGDP is formed in CA July 17, 2019)
2. Annual 1099-NEC Filing - typically due by Jan 31 each calendar year with IRS
3. Annual LLC income tax return - typically due on April 15 each year
4. San Francisco Annual Business Tax Return - typically due on April 15 each year
5. DGDP quarterly capital contribution - 1st Monday of 2nd month of each quarter. Google Calendar reminder is set up for this between Controller and CEO's Executive Assistant. The controller will remind the CEO during monthly finance reporting too.
6. R&D Federal R&D Tax Credit. This tax credit can be applied as soon as close the year and have the annual financials ready. Since DGDP has month end close in routine, Accounting will work with the current CPA (Practical Tax)to apply 2021 R&D tax application in Q1 2022.
7. Important business insurance and subscription renewals dates. Since Accounting manages the prepaid insurance and prepaid subscription schedules, during the month end close process, accounting will monitor the items that are coming up for renewal, such as Hub, AIG Travel Guard, Greenhouse, Gitlab etc. Accounting will alert the CEO and the CEO's EA on the items that are due for renewal 1-2 months before the renewal day.
