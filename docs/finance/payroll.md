## Payroll


*   Payroll for US-based full-time employees is managed via Gusto.
*   DoubleGDP runs a semi-monthly payroll schedule.
*   US Employees will complete the onboarding process through Gusto and set up direct deposits for their payroll payments.


## Contracted Non-US Team Members


*   For contracted **non-US team members**, a completed W-8BEN Form is collected during the [Pilot](https://handbook.doublegdp.com/Finance/Payroll/#pilot) onboarding process.
*   Payments related to the contracted services are usually processed in [Pilot](https://handbook.doublegdp.com/Finance/Payroll/#pilot) monthly.
*   Expense reimbursement reports from the **non-US team members** are required to submit in Expensify.

Please refer to the Expense Reports and Pilot sections below for more details.

## Pilot

DGDP uses [Pilot](https://pilot.co) to process payments for **non-US team members.**

When a team member successfully completes the onboarding process, the direct manager of the new hire should notify Accounting immediately via sending an email to [accounting@doublegdp.com](mailto:accounting@doublegdp.com) with the information below:


*   Full name of the new hire
*   Official start date
*   Annual or monthly compensation in USD$

With the new hire's information, Accounting will help to pre-generate the monthly invoices for the next 12 months on behalf of the new hire. If there are any exceptions on this, please notify Accounting by sending an email to [accounting@doublegdp.com](mailto:accounting@doublegdp.com).

Since the team members in Pilot are mostly based outside of the US and the processing time of non-US payment varies, Accounting has developed a monthly payment calendar to streamline the payment schedule and to allow the funds to arrive on or before the last day of each month.

For new hires who joined on or before the 22nd of a given month, Accounting will try to include the new hire in the monthly payment schedule. If a new hire joined after the 22nd of a given month, Accounting may need to run an off-cycle payment for the new hire. Accounting will notify the new hire when the payment will arrive in the situation of an off-cycle payment.

If you have any questions or concerns about the status of your payment, please contact [accounting@doublegdp.com](mailto:accounting@doublegdp.com).


## CSM's Sales Bonus

DoubleGDP’s goal is to grow Revenue – both “annual recurring (ARR)” and land sales. The customer success team plays an important role in driving this growth, operating as our initial sales team as we find initial product market fit. The CSM bonus plan aims to focus the Customer Success team on bringing in new ARR. For details on the CSM's bonus plan please contact [peopleops@doublegdp.com](mailto:peopleops@doublegdp.com) 

* We keep track of our sales bonus progress [here](https://docs.google.com/spreadsheets/d/1dHQ57HJHHLOpAIo2sKxAxvmfAzSJqAjvEqDL5JMH1ok/edit#gid=0)
* Details on the CSM's Bonus plan can be found in this [document](https://docs.google.com/document/d/12uxaquCgTpBh6st-s0CR4HetpHARoPeRFf7cX-K5sG4/edit#) 
