---
title: Finance Team Homepage
---
## **Customer Invoice**

To create an invoice to customers, please provide below information to [accounting@doublegdp.com](mailto:accounting@doublegdp.com)

Upon receiving below information, accounting will create invoices within 2 business days in QuickBooks Online and distribute the invoice to the customer contact email provided for invoicing. If any DGDP team members need to be copied on the invoice distribution, please indicate it to Accounting.

If the invoice is urgent, please ask Accounting to expedite the invoice process.

1.	Executed Customer Agreement
2.	Customer information
    * Customer name
    * Business address
    * Contact name
    * Email to distribute invoice
    * Payment terms
    * Service terms (start and end date of service period)
    * Product/ services sold (SKU name)
    * Unit price of product / services sold
    * Quantity of product / services sold
    * Discount, if any
    * Billing currency
3.	Customer PO
    * If any customer would provide a PO, please send information listed in 2 and the PO to [accounting@doublegdp.com](mailto:accounting@doublegdp.com) so the PO number can be included in the invoice

4. Customer Payment
    * DGDP accept wire payments for invoices. The detailed wire instructions are built in the invoice template and we don’t need to provide it separately.  



## **Vendor Invoice Submissions**

*   All invoices are required to submit to [accounting@doublegdp.com](mailto:accounting@doublegdp.com).
*   Please DO NOT submit invoices and/or payment instructions via GitLab tickets
*   Vendors are required to submit their invoices within 7 days after the services are rendered or the goods are delivered. Timely submission of your invoices will enable Accounting to record the expenses in the right financial period.

*   **Important Tax Form Requirements**

1. For **US-based vendors**, vendors are required to provide a completed [W-9 Form](https://www.irs.gov/pub/irs-pdf/fw9.pdf) to [accounting@doublegdp.com](mailto:accounting@doublegdp.com) prior to or at the time when they submit their 1st invoice.
1. **For Non-US vendors**, vendors are required to provide a completed [W-8BEN-E Form](https://www.irs.gov/pub/irs-pdf/fw8bene.pdf) prior to or at the time they submit their 1st invoice to [accounting@doublegdp.com ](mailto:accounting@doublegdp.com). Here are the [instructions](https://www.irs.gov/instructions/iw8bene) for Form W-8BEN-E Form.
1. **Important: Accounting cannot process a payment without these Tax Forms**

## **Vendor Invoices Approvals**

* All invoices that require Accounting to make a payment are required to be processed in Bill.com
* Approvals of invoices are completed in Bill.com.
* Accounting will route the invoices according to the following approval hierarchy
* In general, invoices up to $5000 can either be approved by [Executive Assistant](https://handbook.doublegdp.com/people-group/roles-and-responsibilities/) or [CEO](https://handbook.doublegdp.com/company/teams/#ceo)
* Invoices over $5000 require [CEO](https://handbook.doublegdp.com/company/teams/#ceo) approval
* **Project and Program Related Invoice Approval**. For Project and Program-related invoices, ONLY the Head of CS team approves the bill in Bill.com. The EA and the CEO can approve the payment in SVB for wires.
* The Head of CS and Accounting can review and validate the spending according to the Project and Program budget periodically. However, all Project and Program-related bills will need the Head of CS to approve in Bill.com to enable proper internal control.

## **Vendor Invoice Receiving Acknowledgement and Documentations (Accounting’s Responsibilities)**

*   Process invoices received in [accounting@doublegdp.com.](mailto:accounting@doublegdp.com) in Bill.com and route the invoices for approval in Bill.com according to the Invoice Approval Hierarchy.
*   The AP accountant will reply to the [accounting@doublegdp.com.](mailto:accounting@doublegdp.com)and the vendor to acknowledge that the invoice has been processed in Bill.com. The turnaround time is usually 1-2 business days from the day the invoice is submitted to Accounting and the acknowledgment.
*   AP Accountant needs to make sure to use the contractor's contact email to send out bill.com e-invite (do not use the contractor's DoubleGDP email). This will help us to make sure the applicable contractors can receive their tax forms even if they are no longer working with DoubleGDP
*   For US vendors, upload a copy of W-9 in Bill.com under the Document section
*   For non-US vendors, upload a copy of   [W-8BEN Form](https://www.irs.gov/pub/irs-pdf/fw8ben.pdf) in Bill.com under the Document section

## Vendor Invoice Payments

### Bill.com Payments

*   Once an invoice is approved, payment is usually processed based on Net 30 payment terms unless otherwise specified in an agreement or invoice
*   Bill.com e-payment is recommended to pay all vendors
*   How Bill.com e-payment works?
    *   To establish Bill.com e-payment, Accounting will send an e-invite to an email that is provided by the vendor. The vendor accepts the invite and provides their bank information within Bill.com. The payment will be scheduled to deposit to the vendor's account according to the payment terms. E-payment will usually arrive in the recipient's account within 2-4 business days.

**Exception:**  For some non-US vendors who may have difficulties setting up a Bill.com e-payment, a wire payment will be processed.

### Wire Payments

All wires from SVB are initiated by Accounting and approved by DGDP authorized SVB signer(s) per the authorization that is set up with SVB.

#### Wire requestor's responsibilities:

1. Make sure the vendor provides all the required information as outlined below before requesting a wire payment
2. For a new vendor's 1st payment, please send an email to [Accounting@doublegdp.com](mailto:Accounting@doublegdp.com) with invoices, wire instructions, Form W-8Ben-E (if non-US vendors). For the 2nd payment and onward, send invoice only is okay unless the vendor requests any changes, such as bank changes
3. Make sure the vendor provides a formal invoice. (A quotation and/or a Pro Forma Invoice cannot be accepted to issue a payment) Exceptions should be discussed with the EA and Accounting.
4. For non-US vendors, make sure the invoices indicate the proper currency
Wire payment is used to pay any vendors who cannot get paid via Bill.com or to provides funds to **non-US team members** for business expenses. To request a wire payment, please send an email request to [accounting@doublegdp.com.](file:////doublegdp/handbook/-/blob/master/accounting@doublegdp.com) along with:

1. The invoice to be paid
2. Vendor's wire instructions - see below-requested information about wire instructions
3. If a foreign currency wire payment is required, please indicate which currency
4. If a wire payment needs to arrive by a certain date, please be very specific about it so Accounting can plan accordingly
5. A brief description for the Purpose of Payment (per bank requirement for international wires)
6. **Bank Fees** If any bank fees that DGDP should be covering to ensure the recipient can receive the full payment, please notify Accounting about how much bank fees to add to the wire total as the fees can vary vendor by vendor. For example, some Zambia vendors don't need DGDP to include any bank transfer charges, some of them do. Given each vendor's situation is different, Accounting requests vendors to provide how much bank fee to include and in what currency with the 1st payment request. For future recurring payments, Accounting can include the same amount of bank fees without asking again unless new changes are provided by the vendor and the amount of change is justified by Accounting.

    Typically, a wire instruction needs to include below:

1. Recipient's name
2. Recipient's address
3. Recipient's bank name
4. Recipient's bank address
5. Recipient's bank account
6. Recipient's bank ABA Routing Numbers (if domestic wire within the United States)
7. Recipient's IBAN, BIC, or SWIFT Code (if international bank outside of United States)
*   Sometimes the bank may require to provide additional information, Accounting will reach out to the wire requestor if such request arises. *
*   Wire payment is usually quicker, but for international wire payment, it still takes a few business days to clear. We suggest you submit the wire request at least 7 business days prior to the payment due date to allow accounting to get the wire initiated and approved.
*   If the wire payment is urgent, please flag "**URGENT Payment Request**" in the subject line when you send the wire request to [Accounting@doublegdp.com](mailto:Accounting@doublegdp.com). Please don't submit a payment request via a Gitlab ticket as it can easily be buried in the thread.
*   If the wire payment is recurring, please send the invoice that needs to be paid to [Accounting](mailto:accounting@doublegdp.com) and indicates this is a recurring payment with no wire instruction change. Please be specific if the wire needs to arrive by a certain date so Accounting can plan the wire accordingly.

Once a wire payment is initiated and approved, the controller will send a proof of payment to the wire requestor in the following business day or when such document  becomes  available in SVB. When the controller is not available, such as on vacation, the EA will provide such proof of payment.
