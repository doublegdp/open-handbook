## Business Expense Reports Paid with Personal Cash or Credit card

*   For expense reports, receipts should be submitted through **Expensify**
*   If you paid business expenses with your personal card, submit your receipts through Expensify. And submit your Expensify Report for approval within the system.
*   You can do this through the app, or by forwarding the receipt to [receipts@expensify.com](mailto:receipts@expensify.com) from your DGDP email address.
*   The reimbursement for **US-based team members** will be processed in Bill.com unless otherwise advised.
*   The reimbursement for **non-US team members** will be processed in Pilot unless otherwise advised.
*   Please submit all receipts by the 7th of the month as Accounting has the deadline to close the books monthly. For example, the expense report and the receipts for October 2020 need to be submitted by November 7th, 2020

## Expense Report Approval & Reimbursement

*   Expensify approval process is automated
*   Once you submit your expense report, the system will route your expense report to your direct manager for approval
*   After the manager approves in Expensify, Accounting will validate the receipts for accuracy and process payment within 7 days through one of the following methods:
    *   Bill.com for **US-based team members**
    *   Pilot for **non-US team members**
    *   Frequency of Pilot Reimbursement  
     Given the company has implemented the monthly stipend payment to cover the bank fees associated with the monthly Pilot payroll, effective September 1, 2021, Accounting will process monthly reimbursement along with the monthly Pilot payroll to effectively manage the bank fees the company needs to pay.
    ** Exception**: If the reimbursement amount is significant to the team member who needs to get the payment sooner than the monthly payment schedule, please send an e-mail request to Accounting@doublegdp.com for an off-cycle reimbursement payment.
    *   Your hiring agency, for non-US team members who are working with DGDP via an agency
    *   For non contracted DoubleGDP team members, [Wise](www.wise.com) will be used to process reimbursement unless otherwise agreed

## Company Owned Credit Card, Debit Card, and Wise Account Usage Guidance

**Company cards should be mostly used for these categories**:

*   Travel (airfare, lodging, dining, ground transportation, and other misc. travel expenses)
*   Business subscriptions, online ads, etc.
*   Business meals, food delivery, and venue, for employee events, business events
*   Office and business supplies
*   Other online payments, such as business tax payments, business licenses, etc.
*   **If you accidentally use a company credit card for a personal purchase, please send an email to [accounting@doublegdp.com](mailto:accounting@doublegdp.com) and your manager, including a copy of the receipt and the purchase amount in USD. The expense will be deducted from your paycheck in the following payroll cycle**

**Exceptions:**

**_DGDP’s Project and Program Expenses:_**

*   This exception is mostly applied to the CS team who manages the expenses for DGDP’s projects and programs. For project and programs related expenses, a business credit card or debit card can be used for expenses &lt;$500 per unit price. For example, small furniture with a unit price of less than $500 USD.
*   We encourage local teams to use vendors who can accept credit card payments. \
For example, for Zambia vendors, the payment approach should be followed as below:

**Expenses &lt;$500 per unit price**:

*   1st option: [Ramp Card](https://ramp.com/)
*   2nd option: [BOA Debit Card](https://www.bankofamerica.com/smallbusiness/)
*   3rd option: [Wise](https://wise.com/)
*   If neither Ramp nor BOA card works for a certain vendor, the purchaser should reach out to the CEO’s EA to process a [Wise](https://wise.com/) payment and copy Accounting. Once a Wise payment is made, the purchaser should send a receipt to the EA and Accounting.

**Expenses >$500 per unit price**:

*   Vendors should submit an invoice to Accounting and go through the company’s [Invoice process](https://handbook.doublegdp.com/finance/#invoices) outlined in the Handbook.
*   If a special situation arises that will require an urgent credit card payment for purchases over $500 per unit price, please contact CEO’s EA and Controller to get approval before the credit card payment.

### Business Expenses paid by DGDP's Ramp Visa Card

*   [Ramp Visa credit cards](file:////doublegdp/handbook/-/blob/master/www.ramp.com) are issued to **Customer Success Team**, only to pay purchases since Visa card is widely accepted in Zambia and Honduras.
*   **Always** use RAMP VISA Credit Card as your **first** option for payment. We strongly encourage finding vendors that accept Credit Cards payments.
*   ALL business expenses require a receipt submitted to [receipts@ramp.com](mailto:receipts@ramp.com)
*   All transactions require a memo
*   Make sure to submit an itemized receipt, not the credit card payment
*   For business meals or client meetings, please list the business purpose and the names of all the participant in the receipt
*   It is recommended that the most senior level colleague must pay for the expense

### Business Expenses paid by DGDP's Prepaid Card / BOA Card (_Applies to Zambia's CSM_)

*   If you paid business expenses with the company's pre-paid card, please submit your receipts through Expensify.
*   Very importantly, please name the report with the wording such as " Receipts Submission Only". For example, "October 2020 Expenses_ Receipts Submission Only". Including this specific wording will be very helpful for Accounting to differentiate the expense reports that do not require reimbursements versus the ones that they do.

### Business Expenses paid by DGDP's Brex Card

*   If you paid business expenses with the company's Brex card, please forward the receipts directly to [receipts@brex.com](mailto:receipts@brex.com) or upload your receipts directly to [Brex](https://www.brex.com/), also make sure you add a memo with a brief description of the expense.
*   ALL business expenses require a receipt submitted to Brex
*   All transactions require a memo
*   Make sure to submit an itemized receipt, not the credit card payment
*   For business meals or client meetings, please list the business purpose and the names of all the participant in the receipt
*   It is recommended that the most senior level colleague must pay for the expense

### Business Expenses paid by DGDP's Cash Advance

Depends on the business trip or the special situation of certain vendors, a cash advance payment can be arranged. Here is the process.

* Cash Advance requires an approval from the team member's manager
* Cash Advance requestor should forward the manager's approval to Accounting@doublegdp.com to coordinate a payment
* Accounting will either issue a cash advance payment in Pilot or the Executive Assistant will issue a cash advance in Wise
* The requestor should submit all receipts to Expensify right after the advanced cash is used for business purposes. Timely submitted receipts is very important and this will help accounting team to record these transactions properly
* Please name the expense report "Cash Advance expenses, receipt submission only" so the accounting team will know this is to offset the cash advance balance



### Business Meals and Entertainment Guidelines


1. It is important to have team-building events. For those of us who have others in our area getting together to visit or celebrate a milestone can form important social connections. DGDP supports the costs of these events periodically.
2. The most important thing when spending is that you act reasonably with the company's money. It's hard to give strict guidance on specific prices because what's reasonable will vary from country to country and city to city. You're responsible to interpret those prices relative to your location. That said, here are some guidelines for your consideration:
*   Choose moderately priced places for general team meetings or visits. For bigger occasions, pick a place that is appropriate for the cost.
*   For restaurants, this probably means $$ or $$$ on [TripAdvisor](https://www.tripadvisor.com/ShowTopic-g1-i12105-k12250210-Dollar_Sign-Tripadvisor_Support.html) but not $$$$.
*   It's okay to spend on drinks, but remember that you are in a professional context and act accordingly. There can be a huge range in price on wine and liquor; stay at or below the mid-range of cost.
