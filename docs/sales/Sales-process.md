---
title: Sales Process
---


## Sales Process Stages


| Stages |  Pipedrive | Inputs | Outcomes | Owner | Questions |
| ----------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| Identify community | Lead or Qualified Lead |  | Schedule first meeting | CS  | - What is the summary of the deal? <br/>- What are the next steps? <br/>- Are the next steps in the calendar? |
| Introduction and discovery meeting | Qualified Lead/Opportunity | Introduce DGDP and Product Overview | - Community pain points <br/>- Community managers/staff pain points - Confirm decision makers | CS - Escalation to Head of CS if answer is “No” | - Who is the decision maker? <br/>- What other decision makers are involved in the process? <br/>- Are all decision makers currently involved in the process? |
| Demo and validation of needs | Opportunity/Demo Scheduled | Demo, use case discussion and success stories | - Value proposition  | CS - Escalation to Head of CS if answer is “No” | - What are the top 3 things the prospect looks for when choosing a vendor/service? |
| Value Proposition | Proposal Made | Narrow down on product needs | - Contract inputs such as number of units | CS - Escalation to Head of CS if answer is “No” | What does the MSA/legal process look like? |
| Negotiation | Negotiation | Negotiation with decision makers | Decision maker agrees to contract terms | CS - Escalation to Head of CS if answer is “No” | - Do we have competition on the deal? <br/>- Why would we lose this deal? |
| Contract and close | Won or Lost | Draft Contract | Signed Contract | CS Head of CS marks opportunities as lost | Won: What are the success expectations / <br/>Lost: Reasons for losing the deal |
| Upsell | Opportunity | Demo, use case discussion and success stories | - Value proposition <br/>- Contract inputs such as number of users |   |  - What are the top 3 things the customer is looking for?   <br/>- Why would we lose this deal? |


## Pricing Guidelines
Sales reps will use the [DoubleGDP pricing guidelines](https://www.doublegdp.com/pricing/) to advise prospects and customers. In some instances the Head of Revenue may make exceptions for the quote as detailed [here](https://docs.google.com/document/d/1k4KAVqSXrZvKI6EldXIT2OgG6um0c6O6DinRZpZ2fPQ/edit#heading=h.6dtojkyezbr5).


## Objection Handling 
As part of the sales process, prospects will raise objections or concerns around price, product fit or competitors. The guide [here](https://docs.google.com/document/d/1k4KAVqSXrZvKI6EldXIT2OgG6um0c6O6DinRZpZ2fPQ/edit#heading=h.4c0mycpclrd1) can be used by the sales rep to respond in a way that alleviates these concerns and allows the deal to move to the next stage.


## Sales Forecasts and Pipeline Review Meetings
The sales forecast and pipeline review is conducted in the [CS Monday sync slot](https://handbook.doublegdp.com/customer-success/customer-success-meetings/#collaboration-with-doublegdp-teams). Focus will be placed on deals forecasted in the next quarter and leads of interest. These should be updated in [Pipedrive](https://doublegdp.pipedrive.com/). Prior to the meeting each member should have answers to:

1. What’s the community's alternative to using the DGDP platform?

1. What’s the community's compelling reason to buy?

1. Who’s the decision-maker?

1. Why would we lose this deal?

1. When is the next meeting? Has it been scheduled?



## Sales Resources/ Templates

1. Introduction Email [template](https://docs.google.com/document/d/1CLasXxF8s7-JRsDf1RjhY2L0yYeQm8UrZJZwng6PO8U/edit#heading=h.oiedotz6dfkd)
1. Internal Meeting Notes [Document](https://docs.google.com/document/d/1D5Ar4O-HTdGlA-fuOd_nHgQZyMqNykmDNHYtrVMJGH8/edit#heading=h.eerf8sz02g98) for sales conversations
1. [Sales Collateral](https://handbook.doublegdp.com/sales/#collateral)

1. [Discovery questions](https://docs.google.com/document/d/1Nk0NjJu5iTJT7dTxyJzORVJ0NFfrmO1-0RJOcNfmU5Q/edit): Discovery questions are designed to identify existing needs, problems, customer pain points, customer’s goals etc.

1. [Proposal Template](https://docs.google.com/document/d/14ctQ2A1SdhDdQ3xjibHKOlJfgsXJz7FIwJhfWUm_XxQ/edit)

1. [Contract Template](https://docs.google.com/document/d/1BdL2yvLAaAXECP0luEMTh_Kk2jYlL7aeGPBgAKiUA0U/edit): Edit the contract with community's contact details, approvers and subscription tier and pricing. 


## Tracking Product Suggestions from Sales Prospects
During sales conversations we will learn why communities are interested in our product and its features. In addition they may let us know which other software solutions would help their community grow or become more efficient. Our goal is to include 1 or 2 improvements that will be appealing to prospects in every sprint. This is the process our sales and product team use to collect and build those features.

- Complete the [Lessons from Sales Calls](https://docs.google.com/forms/d/16AF-WPe5v75YAMdl5lrJVA9HCCy89JQOCXYLkDgIdZU/edit) to share the information
- Review [Lessons from Sales Calls Responses](https://docs.google.com/spreadsheets/d/109_GRdskzwe9DDYP-TOKHzBLJ6_L9gXwtfPInRnGDRc/edit?resourcekey#gid=808913217) to view the statistics
- The product team and sales team review these suggestions together on their weekly sync call to determine what improvements will have the biggest impact for sales conversations.
- Selected use cases are documented as [epics](https://handbook.doublegdp.com/product/04-prod-communications/#creating-epics) and given a label of [Priority:1](https://handbook.doublegdp.com/product/04-prod-communications/#priority-labels) to be worked on in the next sprint
- Product team will discuss selected features as upcoming in their [sprint updates videos](https://handbook.doublegdp.com/product/04-prod-communications/#product-sprint-updates) and [product update blog posts](https://handbook.doublegdp.com/product/04-prod-communications/#product-update-blog-posts) to create additional visibility and enable the sales team to promote these features to sales prospects.


## Win-Loss Analysis

Losing deals is part of the sales process. Learning from prospects why they chose not to work with DoubleGDP will help us get better at the sales process and improve our product and service delivery. 

1. Lost Deal Interview: A 30-minute interview conducted with the prospect to determine why the deal was lost. This applies to deals that have reached the negotiation and contracting stages. Head of CS marks opportunities as lost.

Use this [interview template](https://docs.google.com/document/d/1dQo5NbSAzbrCy8ntsvXsac14ViUhndvQ46DJYhXY6tM/edit#heading=h.eg7flztx0m00). Read this [learning asset](https://blog.hubspot.com/sales/questions-to-ask-win-loss-review) for guidance. 

