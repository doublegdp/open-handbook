---
title: Partnerships Homepage
---

# Partnerships
Partnerships is responsible to bring in new partners who will:

- utilize our platform (as it is or as we can envision it) for their users.
- work closely with us to address customer pain points

We can be reached at [partnerships@doublegdp.com](mailto:partnerships@doublegdp.com)


## Collateral

We have an overview of [product features on our website](https://www.doublegdp.com/features/).

- The individual feature displays on the websites are provided as a series of ["Website Feature:" Slides](https://drive.google.com/drive/search?q=type:presentation%20%22website%20features%22) so that it's easy to iterate and update them as we learn.
- We also have those same slides linked from a [Compilation Deck](https://docs.google.com/presentation/d/1SvPNnoUuM4_kZd-DubG5AXSV-QgQ1oiXUXUmEsf29Fw/edit#slide=id.g1350ccf6f66_0_828) so that they can be used as a set or downloaded to share with prospects. 
- Additionally, we have developed one page ["Data Sheets"](https://drive.google.com/drive/search?q=title:data%20sheet) for key product features that can be shared with prospects as part of sales proposals.

We collect [feedback about existing sales collateral and requests for new collateral to be developed here.](https://docs.google.com/document/d/1083DsZoQV9nEtcYrGSakTB0WuEgOJFnm5IUN0wkth-c/edit#)


## Market

"Partners" include new cities / master-plan developments / property developers / community organizations / providers of complimentary services to what DoubleGDP does, and their users include staff, residents, businesses, and prospective members of their communities.

The ideal DoubleGDP partner is a planned development with current population or one having significant 18-month population potential led by a pragmatic entrepreneur with an established real estate track record.

 - Population potential: >10,000


## Market Insights

Here are the customer insights that we are learning about our market: [Customer Insights](https://docs.google.com/document/d/1NFHF1v3iHJyG8KHVvOEbjFGihQmYx0dnHmk0ceMCaCI/edit#heading=h.5mzg90e0f5op)


## Partnership Relationship Development "Funnel"

Our relationships with our partners also develop and mature over time.

We have recently switched from AirTable to [Pipedrive](https://doublegdp.pipedrive.com/pipeline/1/user/everyone). Pipedrive offers a better user interface and gives a better overview of all pipeline opportunities. On it we also show the maturity of how our partner relationships are developing. These relationships go through the following stages:

1. `No status / nurture` - We've not had contact, but are aware of their existence
1. `Prospect` Identified multi-use real estate project that already has residents or is likely to have residents within the next 18 months . No active conversation.
1. `Lead` - We've spoken, but not yet sure it's a qualified opportunity; e.g. either org may have some criteria that are unknown or that the other org does not meet.
1. `Qualified Lead` - Established interest in DoubleGDP platform, and identified several modules for use/co-development. (Close likelihood > 20%)
1. `Opportunity` -  Aligned on a vision for a partnership and established mutual interest in working together. Documented use cases; understood desired use-cases and revenue potential. (Close likelihood > 40%)
1. `Negotiating` - Verbal intent to partner. Drafting or negotiating a service agreement. (Close likelihood > 70%)
1. `Active Partner`- Agreement signed to start building software for new partner

![image](/img/sales/funnel2.png)

## New Planned Development Maturity "Funnel"

We've learned that partners have different needs depending on the maturity of the development at the planned communities they are building, supporting or investing in.

In [Pipedrive](https://doublegdp.pipedrive.com/pipeline/1/user/everyone), we show a landscape of planned community development projects that we are aware of.

These communities typically go through the following very simplified development lifecycle, which we use to categorize opportunities for partnership:

**Planning and Fundraising** -> **Sales and Infrastructure Development** -> **Pioneers Building** -> **Active Community - Pioneers** -> **Active Community - Early Adopters** -> **Active Community - Mainstream**


## Engaging new Planned Communities - Initial Email & Call Process
When you have conversation with an external party on behalf of DoubleGDP, please do the following:

1. Share that you're having a Partnerships conversation in slack in the #partners-and-marketing channel.
2. Refer to this [Initial email template](https://docs.google.com/document/d/1CLasXxF8s7-JRsDf1RjhY2L0yYeQm8UrZJZwng6PO8U/edit#heading=h.6c7d7iop0eii) and [call script](https://docs.google.com/document/d/1CLasXxF8s7-JRsDf1RjhY2L0yYeQm8UrZJZwng6PO8U/edit#heading=h.137yp6jy3z2h) to guide you with the first interactions with a new community. Here you will see how your first email should be structured and the kind of questions you should ask in the first call or meeting. See the Q&A below for additional guidelines.
3. If the prospect sounds interested in hearing more about DoubleGDP, please send them a follow-up email as per [this template](https://docs.google.com/document/d/1CLasXxF8s7-JRsDf1RjhY2L0yYeQm8UrZJZwng6PO8U/edit#heading=h.oiedotz6dfkd). You can use that email to introduce the prospect to the Head of Partnerships providing guidance on what the next steps will be.  
4. Ensure that you have the relevant context of any previous discussions by checking the notes document here: [Call Notes with Potential Partners](https://docs.google.com/document/d/1D5Ar4O-HTdGlA-fuOd_nHgQZyMqNykmDNHYtrVMJGH8/edit#heading=h.htey0d28qty8).
5. Follow the note-taking guidelines below.  


### Note-taking
 We create a new tab and record notes or link to a note document here: [Call Notes with Potential Partners](https://docs.google.com/document/d/1D5Ar4O-HTdGlA-fuOd_nHgQZyMqNykmDNHYtrVMJGH8/edit). This document serves as our single source of truth to record all partnerships conversations. It is shared with all@doublegdp.com.

Once opportunities become a `Qualified Lead`, create an epic in GitLab and dedicated notes document for that account, so that it becomes easier to have conversations around the work to be done and a notes document can be shared directly with the prospective partner. Note:

- The epic `Description` should provide a summary of the account that can help team members come up to speed quickly.
- Include a `References` section with links to working documents or other sources that team members may need. See [this example](https://gitlab.com/groups/doublegdp/-/epics/202) for reference.
- Preface the title with `[ACCOUNT]` -- this allows all active opportunities to be retrieved with [Query For Active Accounts](https://gitlab.com/search?search=%5BACCOUNT%5D&group_id=5507473&project_id=&scope=epics&search_code=false&snippets=false&repository_ref=&nav_source=navbar).


### Q&A on how to handle interview with a new Planned Community
Q: How best do we approach our introduction to the planned communities?

A: While we have the [email template](https://docs.google.com/document/d/1CLasXxF8s7-JRsDf1RjhY2L0yYeQm8UrZJZwng6PO8U/edit#heading=h.oq458jjvpt8q) to use for initial communication, we should each do our best to ensure that the tone and language comes across in a way that corresponds with the culture of the market that the prospect is based in. The prospects are likely to respond to communication that they feel is written for them.


Q: What happens if the planned communities already have a software solution they are using?

A: It will be best to do a little research to understand what use cases their current software covers. Thereafter, establish one or two areas that they may not have a solution for that DGDP covers and lead with that. This way we give our chance to get into the account and may eventually replace what they have.


Q: How soon after the initial introduction do we hand over the prospect to Partnerships?

A: Ideally this should happen quite early in the process but I would say it depends on your comfort level. As soon as the prospect asks questions that you might be uncomfortable answering, please make the introduction to Partnerships.
If they ask for a meeting, schedule a hybrid meeting where you can join them physically (where possible) and the Head of Startegic Partnerships can dial in through Zoom to address any questions they may have.


Q: Once we convert the prospects to customers, will they receive tax compliant invoices from DGDP to enable them make their payments?

A: Yes they will. The finance team will provide more guidance on that. However, DGDP will ensure that the invoicing and payment in various different markets remains compliant for both ourselves and our customers.

## Agreement Process

Signed copies of Software Services Agreements are stored in our [shared google drive](https://drive.google.com/drive/u/1/folders/0AKu6sw0UqxwlUk9PVA).

## Invoicing Process

Once the Software Services agreement is signed and the scope of work for each partner's environment agreed upon, development work will commence. Specific timelines will be discussed within which the partenr can begin to use the application. At the end of the month following the partner Go-live date, the billing process will begin and an invoice raised. Invoicing will be done either quarterly or annually in advance with payment due at the end of the quarter (in the case of quarterly billing) or at the end of the first quarter of the year in the case of annual billing.
