# Design System 
The purpose of the design system is to help create rapid and consistent front end UIs for the main app. This system uses a combination of standard and customized Material UI components. The design system repo is in Figma and will be viewable in Storybook using the Figma links.

## Demo City Setup
Engineers please use the below colors and logo in you local city setup. This will help us in QA to clearly identify elements that are primary or secondary colors and show a more consistant product when demoing to clients and in the sprint update videos. 

- Primary: #53A2BE
- Secondary: #1E4785
- [Link to top bar asset](https://drive.google.com/file/d/1w6CZzvpSeOG_YigNKVnPJjMFHQC0StzM/view?usp=sharing)


## Genrerl UX Rules of the thumb 

### Buttons

#### Button Color Strategy:

- If it clicks or taps, it’s the primary color.
- Primary color is only used for interactive elements like buttons, icon buttons or text links. 
- Secondary color is used for hover states and non-interactive elements like section dividers, color bars, chips, other decorative elements.  
- By using the colors this way, the user will know at-a-glance what is clickable and move the interface towards being intuitive.  

#### Button Type — Primary Button
Usually used to help the user know what they need to do to complete the primary purpose of the page, modal or form. 

Usage example:

- Call to Action (CAT) - Sign up now or Go to Task
- Next in a widget
- Save in edit mode
- Submit in a form
- Record Payment on payments page

Elements used for this button type:

- Contained (solid filled) 
- Floating Action Button (FAB) 

#### Button Type — Secondary Button
Usually used for the alternative to the primary button when the user is trying to complete the primary purpose of the page, modal or form. 

Usage examples:

- Edit 
- Cancel in a form or modal
- Back in a widget
- Export Data on payments page

Elements used for this button type:

- Outline (ghost)
- Icon buttons 

#### Button Type — Tertiary Buttons
Usually used for miscellaneous actions. Something that is useful for the user to be able to do but not necessary to complete the primary purpose of the page, modal or form. 

Usage examples:

- Additional actions on cards
- Additional actions for a page

Elements used for this button type:

- Kabab with menu
- Text link
- Speed Dial 

#### Button Combinations

- If 2 buttons are in line. Primary goes on the right and secondary on the left.
- If more than 2 buttons in line, then please submit the page, modal or section for UX review.
- If an input or selector has a button, then the element goes on the left and button goes on the right. 


## Grid
Figma link : [Grids](https://www.figma.com/file/oUdEdIbcdU4XXeroWiquw4/?node-id=9%3A269)

**XL (Extra Large) for screens 1536 and above**

- Columns: 12
- Gutter: 32px
- Side margins: 88px
(includes the 56px for the collasped left navigation bar icons with a 32px gutter)

**LG (Large)for screens 1535-1200**

- Columns: 12
- Gutter: 24px
- Side margins: 88px 
(includes the 56px for the collasped left navigation bar icons with a 32px gutter)

**MD (Medium) for screens 1199-900**

- Columns: 6
- Gutter: 24px
- Side margins: 72px 
(includes the 56px for the collasped left navigation bar icons with a 16px gutter)

**SM (Small) for screens 899-600**

- Columns: 4
- Gutter: 24px
- Side margins: 56px 
(left menu collapsed into hamburger with 24px icon and 16px margins on right and left)

**XS (Extra Small) for screens 599-0**

- Columns: 2
- Gutter: 24px
- Side margins: 40px 
(left menu collapsed into hamburger with 24px icon and 16px margin on left)

## Colors
Figma link : [Colors](https://www.figma.com/file/oUdEdIbcdU4XXeroWiquw4/?node-id=19%3A3092)

Colors usuage values for MUI components: 

- 100 = Branded color 
- 80 = visually 80% of branded color
- 60 = visually 60% of branded color
- 120 = alt dark 
- Button hover = color used for buttons on hover state 
- Button Outline Link = color used for secondary buttons (outline variant) resting state
- Button Outline hover = color used for secondary buttons (outline variant) active state

### Alert Colors

- Sucess: #67b388
- Warning: #f3d158
- Error: #d15249
- info: #598ec1

If a hover state is required for these colors:

- Sucess hover: #a84f22
- Warning hover: #a98b1f
- Error hover: #8a1b13
- info hover: #20507d

## Typography
Figma link : [Typography](https://www.figma.com/file/oUdEdIbcdU4XXeroWiquw4/?node-id=19%3A3093)

- Font: Roboto
- Styles, sizing and spacing are in the Figma link

## Buttons

### Primary Button
Figma link : [Primary Buttons](https://www.figma.com/file/oUdEdIbcdU4XXeroWiquw4/?node-id=2%3A4)

- Variant: Contained
- Color: Primary
- Size Props: Medium for most usage
- Usage: For the primary action like save on a form
- MUI Reference: [Buttons](https://mui.com/components/buttons/)

### Secondary Button
Figma link : [Secondary Button](https://www.figma.com/file/oUdEdIbcdU4XXeroWiquw4/?node-id=161%3A5000)

- Variant: Outlined
- Color: Primary
- Size Props: Medium for most usage
- Usage: For the secodnary action like cancel on a form
- MUI Reference: [Buttons](https://mui.com/components/buttons/)

### Icon Button
Figma link : [Icon Button](https://www.figma.com/file/oUdEdIbcdU4XXeroWiquw4/?node-id=158%3A9745)

- Font: Material icons
- Theme: Filled
- Padding: Medium
- Size: Small, Medium and Large usages
- MUI Reference: [Icons](https://mui.com/components/icons/)

### Split Button
Figma link : [Split Button](https://www.figma.com/file/oUdEdIbcdU4XXeroWiquw4/?node-id=18%3A5803)  

- Variant: Outlined  
- Color: Primary  
- Size Props: Medium for most usage  
- Usage: When a button has a list of options like on the Tasks page for Quick Links  
- MUI Reference: [Split Button](https://mui.com/components/button-group/#split-button)

## Navigation

### Page Titles
All pages should have a page title. 
 
Figma link : [Page Titles](https://www.figma.com/file/oUdEdIbcdU4XXeroWiquw4/?node-id=158%3A6227)

- H4 in text color.
- If a link, then it should be in the primary color like all links.
- Location is 16px below the breadcrumbs (see next section and figma link for more details).

### Breadcrumbs
REQUIRED for most pages. If page has a parent it must have a breadcrumb. 

Figma link : [Breadcrumbs](https://www.figma.com/file/oUdEdIbcdU4XXeroWiquw4/?node-id=158%3A6227)

- 16px space between top nav and breadcrumbs
- Margin left from left gutter for Grid / xl (above 1536px) = 32px 
- Margin left from left gutter for Grid / lg (1535-1200px) = 32px 
- Margin left from left gutter for Grid / md (1199-900) = 24px 
- Margin left from left gutter for Grid / sm (899-600) = 0px 
- Margin left from left gutter for Grid / sm (899-600) = 0px 
- MUI Reference: [Breadcrumbs](https://mui.com/components/breadcrumbs/)

### FAB (floating action button)
Figma link : [FAB](https://www.figma.com/file/oUdEdIbcdU4XXeroWiquw4/?node-id=187%3A5207)

- Variant: Round
- Color: Primary
- 32px space between top nav and FAB
- For spacing and placement see the Figma link
- FAB size for Grid / xl (above 1536px) = Large 
- FAB size for Grid / lg (1535-1200px) = Large 
- FAB size for Grid / md (1199-900) = Medium 
- FAB size for Grid / sm (899-600) = Small 
- FAB size for for Grid / sm (899-600) = Small 
- MUI Reference: [FAB](https://mui.com/components/floating-action-button/)

### Speed Dial
Figma link : [Speed Dial](https://www.figma.com/file/oUdEdIbcdU4XXeroWiquw4/?node-id=2%3A3)  

- Usage: For when a page has more than one action for the page
- Position: Upper right 
- Tooltip color: Text grey  
- FAB: color: Primary and Variant: Round (See FAB section for more details)
- 32px space between top nav and FAB
- FAB size for Grid / xl (above 1536px) = Large 
- FAB size for Grid / lg (1535-1200px) = Large 
- FAB size for Grid / md (1199-900) = Medium 
- FAB size for Grid / sm (899-600) = Small 
- FAB size for for Grid / sm (899-600) = Small   
- MUI Reference: [Speed Dial](https://mui.com/components/speed-dial/)  

### PageWrapper 
Link : [Link](https://gitlab.com/doublegdp/app/-/blob/master/app/javascript/src/shared/PageWrapper.jsx)

- Usage: All new page or screen added should be wrapped with the PageWrapper component
- pageTitle prop: Title for the page
- oneCol prop: For single column page
- showAvatar prop: if set to true, it will show the avatar
- breadCrumbObj: if passed will set breadcrumb for the page
- avatarObj: if passed will add user avatar, name and role to the page
- rightPanelObj: if passed will add the rightPanelObj components to the top right side of the page
- hideWrapper: if set to true it will render just the children component with a div around it


### Snackbar 
Link : [Link](https://gitlab.com/doublegdp/app/-/blob/master/app/javascript/src/shared/snackbar/Context/index.js#L15)

- Usage: Message alert within the app will be triggered through a centralised snackbar. The snackbar exposes a single api through which this is achieved. Currently it exposes two items namely:
  - **messageType**: _object_ => _{ error: 'error', success: 'success', warn: 'warning' }_
  - **showSnackbar**: _method_
     - args: {object}
       - type: _String_
       - message: _String_
       - style: _Object (optional)_
  
- Example: To use the snackbar invoke the showSnackbar method and pass down arguments as object. [Example Link](https://gitlab.com/doublegdp/app/-/blob/master/app/javascript/src/components/LandParcels/CreateLandParcel.jsx#L42).
- Jest Tests: Wrap test components within the `<MockedSnackbarProvider>` for related test files. [Example Test](https://gitlab.com/doublegdp/app/-/blob/master/app/javascript/src/__tests__/CreateLandParcel.test.js#L46)







