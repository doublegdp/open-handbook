# KPIs for Engineering

We are firm believers of measuring successes and failures.   And most times, what gets measured and communicated will improve over time.   

At DoubleGDP, we measure a few data points over time to make sure we can consistently improve  and spot trouble some trends before becoming unruly.  Some data points are measured constantly, and a overall data summary is compiled at the end of every week.

At the beginning of the sprint, two or more engineers assign each story a certain number of points (days of work).   Bugs automatically have 1 point (1 day's worth of work).  The total number of points in a sprint should be around 10 * total engineers.    10 * total number of engineers is the minimum point number engineers are expected to complete cumulatively.
