# Product Architecture and Security

Our product architecture overview and security guiding principles are [publicly shared.](https://docs.google.com/presentation/d/1LTFfLLNcau3f00CHMSClBLMnLAP8T64fPhsZsMhrCmE/edit?usp=sharing)


### Security Overview

#### Development

1. Production data is kept separate from development data and it doesn't leave production servers
2. DoubleGDP does not develop on real customer data and follows the client’s guidelines on data access
3. We encrypt communications (SSH, HTTPS) to development servers.


#### Production

1. We encrypt communications (SSH, HTTPS encryption) to our production servers.

   1. End-to-End encryption through CloudFlare.

2. We encrypt files at rest using S3 server side encryption.  SSE-KMS. 
   https://docs.aws.amazon.com/AmazonS3/latest/dev/serv-side-encryption.html

3. Heroku postgrees encryption: 
   https://devcenter.heroku.com/articles/heroku-postgres-production-tier-technical-characterization#data-encryption (Database is encrypted at the block-level storage encryption)

   
#### Sensitive File Handling

 1.  Engineers are sometimes given access to files containing sensitive information through google docs and following Google encryption procedures:
     https://services.google.com/fh/files/misc/google-workspace-encryption-wp.pdf

 2.  Files are access using HTTPS

#### Vulnerabilities.
 1. As part of our deployment process, we monitor for vulnerabilities and address them promptly.

#### Engineer Training on Privacy

   Starting January 2021, Engineers will be required to attend data privacy training.  


#### Application Permissions restructure.

1. As part of engineering efforts towards product security improvement, we have refined application permissions following the guidelines below:

2. We have application permissions under app/policies/.

3. Within app/policies we have submodules that reflect different modules we have like payments, tasks, communities

4. permissions are defined with module name and associated roles and permissions as an array e.g `community: {admin: %i[can_update_community]}`, `payments: {admin: %i[can_create_plan_payment]} `.

4. Within each specific policy module example CommunityPolicy we have a permission checker against a specific permission list in this case it checks for community permissions.
`  user_permissions = permission_list.dig(:community, user.user_type.to_sym, :permissions)
   return false if user_permissions.nil?

   user_permissions.include?(permission.to_s)`

5. The policies are invoked from either queries or mutations, passing in a current user user_type attributes and the permission to check against. Example:
   `return true if Communitypolicy.new.has_permission(context[:current_user].user_type, :can_update_community)`

6. To ensure backward compatibility, we have this check as first rule and older check as second `return true if context[:current_user]&.admin?`.

7. For better permissions management and extensibility, we define all permissions in `app/policies/permissions.yml` file and read and initialize them in ApplicationPolicy
 `PERMISSIONS = YAML.load_file("#{::Rails.root}/app/policies/permissions.yml")`
`@permission_list = PERMISSIONS.deep_transform_keys!(&:to_sym)`

8. Application permissions for diffent modules can be found in this file `https://gitlab.com/doublegdp/app/-/blob/master/app/policies/permissions.yml`

#### Sample Current Roles and Permissions for different modules.

Form

- [ ] can_see_menu_item <br>  
- [ ] can_create_category <br>  
- [ ] can_update_category <br/> 
- [ ] can_delete_category <br/> 
- [ ] can_create_form <br>  
- [ ] can_update_form <br>  
- [ ] can_create_form_properties <br/> 
- [ ] can_update_form_properties <br> 
- [ ] can_delete_form_properties <br>
- [ ] can_create_form_user <br>  
- [ ] can_update_form_user <br> 
- [ ] can_update_form_user_status <br> 
- [ ] can_create_user_form_properties <br>
- [ ] can_update_user_form_properties <br> 
- [ ] can_view_form_entries <br> 
- [ ] can_view_form_form_submissions <br> 
- [ ] can_view_form_user_properties <br> 
- [ ] can_fill_a_form <br>  
- [ ] can_edit_a_form <br>  
- [ ] can_view_own_forms <br>  
- [ ] can_access_forms <br> 
- [ ] can_view_form_user <br> 
- [ ] can_fetch_form
- [ ] can_fetch_form_properties
- [ ] can_fetch_form_property
- [ ] can_fetch_form_categories
- [ ] can_save_draft_form 

Payment Plan

- [ ] can_cancel_payment_plan <br>  
- [ ] can_create_payment_plan <br>  
- [ ] can_fetch_community_payment_plans  <br> 
- [ ] can_fetch_plan_statement  <br> 
- [ ] can_fetch_user_payment_plans  <br> 
- [ ] can_send_payment_reminder  <br> 
- [ ] can_transfer_payment_plan <br> 
- [ ] can_update_payment_day  <br> 
- [ ] can_update_payment_plan  <br> 
- [ ] can_allocate_general_funds
- [ ] can_view_menu_list

Plan Payments

- [ ] can_access_all_payments <br>  
- [ ] can_cancel_plan_payment <br>  
- [ ] can_create_plan_payment  <br> 
- [ ] can_fetch_payment_receipt  <br> 
- [ ] can_fetch_payment_stat_details  <br> 
- [ ] can_fetch_payments_list  <br> 
- [ ] can_view_menu_list <br> 
- [ ] can_see_menu_item
- [ ] can_transfer_plan_payment


Payments Records

- [ ] can_create_transaction
- [ ] can_fetch_user_transactions   
- [ ] can_fetch_transaction_summary 
- [ ] can_revert_transaction 
- [ ] can_fetch_accounting_stats  
- [ ] can_create_wallet_transaction  
- [ ] can_update_wallet_transaction 
- [ ] can_revert_wallet_transaction
- [ ] can_make_payment 
- [ ] can_see_menu_item 
- [ ] can_view_transaction_logs
- [ ] can_view_all_transaction_logs

Land parcel

 - [ ] can_see_menu_item <br> 
 - [ ] can_create_land_parcel <br> 
 - [ ] can_fetch_house <br> 
 - [ ] can_fetch_land_parcel <br> 
 - [ ] can_fetch_land_parcels <br> 
 - [ ] can_fetch_land_parcels_with_plans <br> 
 - [ ] can_merge_land_parcels <br> 
 - [ ] can_update_land_parcel <br> 
 - [ ] can_view_all_land_parcels <br>  
 - [ ] can_create_point_of_interest <br>  
 - [ ] can_create_point_of_interest_image <br>  
 - [ ] can_delete_point_of_interest

 Transaction

- [ ] can_create_transaction <br>  
- [ ] can_revert_transaction <br>  
- [ ] can_fetch_accounting_stats  <br> 
- [ ] can_fetch_transaction_summary  <br> 
- [ ] can_fetch_user_transactions 
- [ ] can_create_wallet_transaction
- [ ] can_update_wallet_transaction
- [ ] can_revert_wallet_transaction
- [ ] can_make_payment
- [ ] can_see_menu_item

Subscription Plan

- [ ] can_create_subscription_plan <br> 
- [ ] can_update_subscription_plan <br> 
- [ ] can_fetch_subscription_plans

User

- [ ] can_update_user_details <br>  
- [ ] can_merge_users <br>  
- [ ] can_import_users  <br> 
- [ ] can_send_one_time_login  <br> 
- [ ] can_access_all_users  <br> 
- [ ] can_view_own_profile  <br> 
- [ ] can_edit_own_profile <br> 
- [ ] can_get_substatus_count <br> 
- [ ] can_get_substatus_distribution <br> 
- [ ] can_view_admin_users <br> 
- [ ] can_get_user_count <br> 
- [ ] can_get_users 
- [ ] can_get_users_lite <br> 
- [ ] can_see_menu_item
- [ ] can_search_guests
- [ ] can_see_menu_item
- [ ] can_create_user
- [ ] can_view_guests
- [ ] can_view_hosts
- [ ] can_update_invitation
- [ ] can_search_user_ids
- [ ] can_create_users_via_csv

Profile

- [ ] can_see_menu_item

Logout

- [ ] can_see_menu_item

Communication

- [ ] can_see_menu_item

Label

- [ ] can_create_label
- [ ] can_delete_label
- [ ] can_merge_labels
- [ ] can_update_label
- [ ] can_create_user_label
- [ ] can_update_user_label
- [ ] can_fetch_all_labels
- [ ] can_fetch_label_users
- [ ] can_fetch_user_labels
- [ ] can_see_menu_item
- [ ] can_view_lead_labels

Community

- [ ] can_see_menu_item
- [ ] can_update_community_details
- [ ] can_view_community_details

Community Settings

- [ ] can_see_menu_item

Discussion

- [ ] can_create_discussion
- [ ] can_update_discussion
- [ ] can_access_all_discussions
- [ ] can_see_menu_item
- [ ] can_create_post
- [ ] can_view_posts
- [ ] can_delete_post_image
- [ ] can_update_post
- [ ] can_delete_post
- [ ] can_set_accessibility


Comment

- [ ] can_fetch_all_comments

Business

- [ ] can_see_menu_item
- [ ] can_access_business
- [ ] can_create_business
- [ ] can_delete_business
- [ ] can_update_business

Dashboard

- [ ] can_access_dashboard
- [ ] can_see_menu_item
- [ ] can_access_admin_processes_dashboard

Activity Log

- [ ] can_update_activity_log

Action Flow

- [ ] can_create_action_flow
- [ ] can_update_action_flow
- [ ] can_delete_action_flow
- [ ] can_get_action_flow_list
- [ ] can_get_action_flow_rule_fields
- [ ] can_get_action_flow_events
- [ ] can_get_action_flow_actions
- [ ] can_get_action_flow_action_fields

Event log

- [ ] can_fetch_logbook_events
- [ ] can_download_logbook_events

Lead Log

- [ ] can_create_lead_log
- [ ] can_update_lead_log
- [ ] can_fetch_lead_logs
- [ ] can_access_lead_scorecard

Lead

- [ ] can_view_lead_users
- [ ] can_edit_lead
- [ ] can_delete_lead
- [ ] can_view_lead_profile
- [ ] can_upload_lead_files
- [ ] can_see_menu_item

Messages

- [ ] can_see_menu_item
- [ ] can_access_messages
- [ ] can_access_user_messages

Amenity

- [ ] can_create_amenity
- [ ] can_access_amenities
- [ ] can_see_menu_item
- [ ] can_edit_amenities
- [ ] can_delete_amenities

My Forms

- [ ] can_see_menu_item
- [ ] can_access_own_forms
- [ ] can_fetch_form_task_comments
 
Entry Request

- [ ] can_see_menu_item <br> 
- [ ] can_access_logbook <br> 
- [ ] can_create_entry_request <br> 
- [ ] can_update_entry_request <br> 
- [ ] can_deny_entry <br> 
- [ ] can_grant_entry <br> 
- [ ] can_add_entry_request_note <br> 
- [ ] can_view_entry_requests <br> 
- [ ] can_view_entry_request <br> 
- [ ] can_update_invitation <br> 
- [ ] can_go_through_guest_verification <br> 
- [ ] can_invite_guest
- [ ] can_go_through_guest_verification
- [ ] can_update_invitation

Email Template 

- [ ] can_see_menu_item <br> 
- [ ] can_access_email_template <br> 
- [ ] can_create_email_template <br> 
- [ ] can_update_email_template <br> 
- [ ] can_view_email_template_variables <br> 
- [ ] can_view_email_templates <br> 
- [ ] can_view_email_template

Gate Access

 - [ ] can_see_menu_item 

Guestlist

- [ ] can_see_menu_item

Timesheet

- [ ] can_fetch_time_sheet_logs
- [ ] can_fetch_user_time_sheet_logs
- [ ] can_fetch_user_last_shift
- [ ] can_access_all_timesheets
- [ ] can_see_menu_item

Campaign

 - [ ] can_access_campaign <br> 
 - [ ] can_create_campaign <br> 
 - [ ] can_delete_campaign <br> 
 - [ ] can_create_campaign_through_users <br> 
 - [ ] can_remove_campaign_label <br> 
 - [ ] can_update_campaign
 
Community

- [ ] can_see_menu_item <br> 
- [ ] can_update_community_details <br> 
- [ ] can_view_community_details 

SOS  

- [ ] can_access_sos <br> 
- [ ] can_raise_sos <br> 
- [ ] can_cancel_sos 

Note 

- [ ] can_create_note <br> 
- [ ] can_see_menu_item <br> 
- [ ] can_update_note  <br> 
- [ ] can_set_note_reminder <br> 
- [ ] can_assign_note  <br> 
- [ ] can_bulk_assign_note <br> 
- [ ] can_create_note_comment <br> 
- [ ] can_update_note_comment <br> 
- [ ] can_delete_note_comment <br> 
- [ ] can_fetch_flagged_notes <br> 
- [ ] can_fetch_task_by_id <br> 
- [ ] can_fetch_task_comments <br> 
- [ ] can_fetch_task_histories <br> 
- [ ] can_get_task_count <br> 
- [ ] can_get_task_stats <br> 
- [ ] can_get_own_tasks <br> 
- [ ] can_fetch_all_notes <br> 
- [ ] can_fetch_user_notes <br> 
- [ ] can_view_create_task_button  <br> 
- [ ] can_view_create_sub_task_button <br> 
- [ ] can_access_tasks <br> 
- [ ] can_access_processes <br> 
- [ ] can_delete_note_document <br> 
- [ ] can_resolve_note_comments
- [ ] can_access_project_steps
- [ ] can_mark_task_as_complete
- [ ] can_view_task_lists
- [ ] can_create_task_lists
- [ ] can_get_comment_stats
- [ ] can_edit_task_list
- [ ] can_delete_task_list
- [ ] can_get_replies_requested_comments
- [ ] can_fetch_process_comments
- [ ] can_fetch_project_document_comments

Process

- [ ] can_view_process_templates
- [ ] can_create_process_template
- [ ] can_update_process_template
- [ ] can_delete_process_template
