# Tools and Services we integrate

> This section explains what each services does, when you should need it and how to use its basic use. All the services mentioned here are very important and should be monitored all the times to make sure everything is working smoothly

- [PagerDuty](https://doublegdp.pagerduty.com/)  
- [Rollbar](rollbar.com/)  
- [Hexnode](https://doublegdp.hexnodemdm.com/)    
- [Phrase](https://phrase.com/)    
- [Google Analytics](analytics.google.com/)   
- [Nexmo](https://identity.nexmo.com/)   
- [Hotjar**](analytics.google.com/)   

### PagerDuty

We use PagerDuty to manage incidents escalation and alerting the person on duty to acknowledge and possible fix that incident. [PagerDuty](https://doublegdp.pagerduty.com/) integrates with [Rollbar](rollbar.com/) so when an error occurs multiple times in a short period of time then PagerDuty will alert the person on duty, If this person does not acknowledge the incident, PagerDuty escalates this to the next person in the escalation level.   

**When do you need it**: Always    
**How to use it**: Stay alert when you are on duty and fix or delegate on reported issues   
**If you need Support**: Contact the Engineering Manager   

### Rollbar

We use [Rollbar](rollbar.com/) to track errors from all sides of the our platform, This helps us know where the error originated from, which part of the codebase, what browser the user was using and how many times it happened. We can then use this information to understand what caused this and come up with a possible fix sooner.
Rollbar also allows us to assign an issue to a specific engineer and you can also directly create a bug ticket from rollbar.

**When do you need it**: Always      
**How to use it**:  
- Stay alert when you are on duty and fix or delegate on reported issues   
- Make sure you have access to rollabr, if not contact the engineering manager    
- When an error is reported in most cases you will receive a slack alert, you can use the provided link to see more information about the error.  
- Pay attention to the follow tabs   
    - Traceback: This will help you figure out where in the codebase this originated from  
    - Occurrences: How often it has happened, The link where it happened, The browser/OS and the time for each occurrence   
    - Browser/IOS: This is important because some features may not work as expected on certain browsers, so it is good to pay attent to this.   

**If you need Support**: Contact Engineers in public channels   


### Hexnode

We use [Hexnode](https://doublegdp.hexnodemdm.com/) to manage company devices remotely, most of these are security guards' phones and custodians' as well. With Hexnode you can lock or unlock the phone, you can enable specific apps to run and many other things.

**When do you need it**: Only when requested        
**How to use it**:  There are few instances when we need to unlock the phone to change settings, to update policy, etc ... Here are few things to consider when in hexnode.  
- Make sure the device you want to modify is the right one by:  
    - Checking the battery percentage   
    - Checking the SIM's carrier     
    - Checking the phone's location   
    - Confirming last time it was active.    
    - Not relying on the device name or Model Name   
- Once you logged in, at the top look for "manage" in the menu and click on it    
- This will show you all the active devices, Identify the device you need by using the criteria mentioned above.   
- Once you identify the device, You can click on it   
- From the device details, there is an "Actions" menu, that you can choose depending on your need.    
- If you need to relax hexnode to allow the phone to be modified by the user(e.g: They want to change WIFI or add a new SIM) You can click on **Disable Kiosk Mode menu** and when you are done, You can **Enable Kiosk Mode** to lock it back   
- You can explore other options in Hexnode UI but be careful to not initiate any action without paying extra attention   

**If you need Support**:     
    - Contact Engineers in public channels   
    - Contact Engineering Manager   
    - Contact the CEO    

### Phrase

We use [Phrase](https://phrase.com/) for managing translations in the application.   
**When do you need it**: Only when requested or when there is an issue with synchronization of the translations    
**How to use it**: You can find a guide on how translations work with Phrase in the [Internationalization Section](/engineering/standards/internationalization#using-phrasecom-to-be-continued)   

**If you need Support**:  
    - Contact Engineers in public channels   
    - Contact one of our language translators   


### Google Analytics

As the name says it, [Google Analytics](analytics.google.com/) is used to track analytics for all our current communities, You can find a list of our communities and their respective [google analytics code here](https://docs.google.com/spreadsheets/d/1wdiOLSAS9Iik-gIUQXDMvyrYgFcV_Lk1K8CMmaCfqD8/edit#gid=0)   

**When do you need it**:    
    - When you need to see the analytics of the app  
    - When you need to add a new community     
**How to use it**:   
    - TBA   
**If you need Support**:      
    - Contact Engineers in public channels   

### Nexmo or Vonage

We currently use [Nexmo](https://identity.nexmo.com/) for all our outgoing SMSs, so whenever there is an issue with messages not being sent if that's not originating from out codebase then Nexmo is a good place to check.   

**When do you need it**:    
    - When you need to see the logs of all outgoing SMSs   
    - When you check if our credit has run out   
    - When messages are not being delivered yet certain users need login codes   
**How to use it**:  
    - For the last use case, You can navigate to SMS Logs, then find a message for a specific phone number  
    - Take note of the number and which country it was sent to, find the latest PIN code and share that with the user  

**If you need Support**:   
    - Contact Engineers in public channels   

### Hotjar

[Hotjar**](analytics.google.com/) is used to monitor how the user interacts with the application, what parts of the page they focus on, where do they click, how do they move their mouse and all other interesting actions. Hotjar records all these actions and provides us with a video recording that we can watch to understand deeper the real user experience from the actual users of our application, Hotjar also provides heatmaps that shows the focus of the users when they are using the page.
Here is an example of a [hotjar recording](https://insights.hotjar.com/r?site=2893337&recording=11726670623&opener=overview) on how one of our users used the logbook to invite a user.  

**When do you need it**:    
    - When you need to watch recordings from different communities  
    - When you need to setup a new community     
**How to use it**:  
    - For the last use case, Head over to the site list [https://insights.hotjar.com/site/list](https://insights.hotjar.com/site/list)   
    - Click on New Site button    
    - Add the requested information, e.g:    
            - For Website: use something like  https://demo.dgdp.com   
            - For Site Type: Choose Community   
            - For Site Owner: Choose DoubleGDP   
            - Then Agree to the terms    
            - Then Choose a plan - plus     
    - After this, you will be redirected to the site list for our communities   
    - Find the one who've just added, and note it says "Not installed"  
    - Click on Install tracking code     
    - Click on Add Code Manually    
    - Then at the bottom there will be a Site ID, copy this and add it to the community table    
    - Website name should match with community hostname      
    - Alternatively you can also copy the ID in the first column of each site list found [here](https://insights.hotjar.com/site/list)

**If you need Support**:     
    - Contact Engineers in public channels     
