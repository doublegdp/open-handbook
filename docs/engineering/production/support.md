# Support

## Customer support
At DoubleGDP, all Customers' inquiries end up as a ticket in Gitlab.   While there are many ways to create tickets in Gitlab, we recommend customers to either contact their Customer Success Manager or submit a **Service Desk Request** described in the section below in **Submitting a Service Desk Request**.

### Application Issues

A application or feature which produces an incorrect or unexpected result, or behaves in unintended ways is usually caused by a flaw in the computer code.  In such case, a production issue should be reported immediately by **Submitting a Service Desk Request** so that a software engineer can analyze the issue  can be directly opened.

Many times application issues are opened erroneously when the software behaves the way it was designed but does not fit the business need.  In such cases, a feature change needs to be created.  Please, **Contact your Customer Success Manager to gather the new requirements** into a set of stories and get them scheduled in the development pipeline.  If you already have gathered a detailed description for a 'New Feature' or 'Feature Change', you can always submit a Service Desk Request.

All **Service Desk Request** will be reviewed timely and triaged to determine urgency, completeness of the information and loop in additional team members such as CSMs. Product Managers, and Designers.

#### Anatomy of a bug
> Kindly note that these fields are required for a bug to be worked on

1. Summary: short description that best describes the bug
1. Steps to reproduce - well explained steps on how the bug happens
1. What is the expected correct behavior? - explain how the feature should normally work.   If the issue is an exception from Rollbar, add the Rollbar link reference.
1. Who should verify on Staging? - person responsible for this bug
1. Relevant logs and/or screenshots - links and any other information that might help to quickly fix the bug
1. Assignee: Assign the Product Manager if it's a high priority, so it's seen immediately. Otherwise, the Product Manager review the list of new bugs/issues once a week.

## Submitting a Service Desk Request
> The more details we receive, the quicker we can identify the issue and work on it to fix it.

#### New feature request: ####
1. Summary: short description that best describes the new feature
1. Current Feature behavior - Explanation on how the current feature works
1. New Feature behavior - Explanation on how you want the modified feature to work.

Please **email** the above information to our service desk at **[incoming+doublegdp-app-13905080-issue-@incoming.gitlab.com](mailto:incoming+doublegdp-app-13905080-issue-@incoming.gitlab.com)**

#### When requesting a feature change: ####
1. Summary: short description that best describes the new feature
1. Current Feature behavior - Explanation on how the current feature works
1. New Feature behavior - Explanation on how you want the modified feature to work.

Please **email** the above information to our service desk at **[incoming+doublegdp-app-13905080-issue-@incoming.gitlab.com](mailto:incoming+doublegdp-app-13905080-issue-@incoming.gitlab.com)**

#### When reporting an application issue (BUG): ####
1. Summary: short description that best describes the bug
1. Steps to reproduce - well explained steps on how the bug happens
1. What is the expected correct behavior? - explain how the feature should normally work.

Please **email** the above information to our service desk at **[incoming+doublegdp-app-13905080-issue-@incoming.gitlab.com](mailto:incoming+doublegdp-app-13905080-issue-@incoming.gitlab.com)**

### RCCAs (Root Cause and Corrective Action)

Root Cause and Corrective Actions are essential for documenting what went wrong in Production and proactively assuring business continuity.  They are logs containing production issue events, their resolutions, and potential follow up action items.

Post Mortems 2 15- minutes meetings:

- Initial 15 mins meeting to fill in the information according to the 'anatomy of a RCCA'.
  - Post Mortem resolutions should always result in a Merge Request.
    - Whether a code MR is created
    - And/or a Procedural change - a hand book MR.
- Follow up meeting to document the steps actually taken to prevent or mitigate the issue from happening in the future.

Root Cause and Corrective Actions are accessible by anyone at DoubleGDP and can be communicated outside the company.

#### Anatomy of an RCCA

- Description: Brief synapsis of what happened.

- Who was affected

- Fix(immediate): What was done to fix the issue.

- Long Term Fix:  if there is a need for the issue to be addressed. long term, then:
  - Create a List of stories.
  - Create a list of Procedure changes.

- Setup Follow up meeting date.

- Use the template named 'rcca'

- Follow up meeting:

  - GitLab Ticket will be created to track what steps were actually taken to mitigate the issue.
