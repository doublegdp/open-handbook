# Module Experts - Role Playing

## Engineering Subject Matter Experts

Team,
The following list is the list of experts for whenever you:

- need advice on the application
- need to looping an engineering expert in a discussion
- have a question about how something works.

Responsible experts are with (\*).
Here are the modules and experts :

- User management
    - Saurabh
    - **Nurudeen** (\*)
- Gate Access
    - **Olivier** (\*)
- Construction mgt & Time sheets
    - **Olivier** (\*)
- Community Engagement (Discussions, News, Social Media)
    - **Olivier** (\*)
- Messages/Notifications
    - **Saurabh** (\*)
    - tolu
- Campaigns
    - **Nurudeen** (\*)
- Forms and Workflows
    - Olivier
    - **Saurabh** (\*)
    - Victor (workflows)
- Payment/Invoices
    - Olivier
    - **Saurabh** (\*)
    - Tolu
- Land Management
    - **Victor** (\*)
- Devops
    - **Nurudeen** (\*)
    - Victor
- Product Design and User Interface Design
    - **Vanessa** (\*)

## Engineering Role Playing

Each engineer is required to perform a role of as a user in their selected user types at least 3 times in a week.  
Following is the list of roles and engineers responsible to perform that role.   
The role should be performed with:  
    - Different types of devices and browsers  
    - In both English and Spanish

- Admin
    - Everyone
- Client
    - Okeugo
    - Saurabh Shinde
- Contractor
    - Nurudeen
    - Bonny
- Custodian
    - Nurudeen
- Prospective Client
    - Tolulope Olaniyan
- Resident
    - Saurabh Shinde
    - Daniel
- Security Guard
    - Olivier
    - Daniel
- Visitor
    - Olivier
    - Bonny
- Site Worker
    - Daniel

### Role Playing Checklist

These are checks an engineer should make sure that they are passing when performing role play, these checks assume a community that supports all features that we currently have in the app.

**Client**

- Is able to Login with:
    - Facebook
    - Google
    - Phone Number
- Is able to see their past payments
- Is able to be scanned at the gate
- Is able to access their own profile
- Is able to access messages(Send and Receive)
- Is able to access their own plot(s)
- Is able to access forms
- Is able to submit a form and view their own submissions
- Is able to access guest list
- Is able to invite and update their own guest
- Is able to update their own basic profile info
- Is able to use the SOS feature

**Contractor**

- Is able to Login with:
    - Facebook
    - Google
    - Phone Number
- Is able to see their own timesheet
- Is able to be scanned for time recording and for gate access
- Is able to access their own profile
- Should not have access to search, payments, etc ...
- Is able to update their own basic profile info
- Is able to create a task
- Is able to view tasks assigned to them
- Is able to mark task as complete

**Custodian**

- Is able to Login with:
    - Facebook
    - Google
    - Phone Number
- Is able to see timesheet for all employees
- Is able to be scan or search for other users
- Is able to record shifts for Security guards and Contractors
- Is able to access their own profile
- Is able to access contractor and security guard's profiles
- Should not have payments, etc ...
- Is able to update their own basic profile info
- Is able to view tasks assigned to them
- Is able to mark task as complete
- Is able to create a task
- Is able to assign tasks to other users
- Is able to use the SOS feature


**Prospective Client**

- Is able to Login with:
    - Facebook
    - Google
    - Phone Number
- Is able to access their own profile
- Is able to access messages(Send and Receive)
- Is able to access active community discussions
- Is able to access community businesses
- Is able to update their own basic profile info

**Resident**

- Is able to Login with:
    - Facebook
    - Google
    - Phone Number
- Is able to see their past payments
- Is able to be scanned at the gate
- Is able to access their own profile
- Is able to access messages(Send and Receive)
- Is able to access their own plot(s)
- Is able to access forms
- Is able to submit a form and view their own submissions
- Is able to access guest list
- Is able to invite and update their own guest
- Is able to update their own basic profile info
- Is able to use the SOS feature

**Security Guard**

- Is able to Login with:
    - Facebook
    - Google
    - Phone Number
- Is able to see their own timesheet
- Is able to be scan or search for other users
- Is able to manually record entries at the gate.
- Is able to access their own profile
- Is able to access other users's profiles
  - Profile should only show restricted contact information for user verification
- Is able to update their own basic profile info
- Is able to view tasks assigned to them
- Is able to mark task as complete
- Is able to use the SOS feature

**Visitor**

- Is able to Login with:
    - Facebook
    - Google
    - del> Phone Number
- Is able to see their own profile
- Is able to access community news
- Is able to access community support
- Is able to update their own basic profile info
