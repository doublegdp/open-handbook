### Sprint Board

Below is an overview of our projects and how we use them to manage our product design and development cycles.
https://gitlab.com/doublegdp/app/-/boards/

#### Feature suggestions

Here’s how to add a new idea:

- Before adding a feature, perform a quick search in the Platform project to make sure it was not previously suggested to avoid duplicates. If it has been previously suggested, add your input and ideas as a comment to the task
- If the feature was not previously suggested, then create a new issue in the Platform project, select the feature dropdown in the field, fill out all the fields
- During our product planning discussions, we will categorize the suggestion and follow up with any questions. Where appropriate, we’ll group it with other features requests that have similar components
- The originator of the issue will be tagged as a stakeholder on the ticket and will be responsible for providing additional information and reviewing the ticket when on staging before being promoted to production
- If it is a high priority feature request, then leave a comment for the Product Manager in the task with @[name]


##### Prefer "Feature Requests" to "Requirements"

Whenever we refer to features that are discussed or requested by customers, we use the term "feature requests", "user story", or "use case" rather than "requirements." This is both to allow us to use the specific language that we hear in a request, even if we have not yet decided how to implement a feature. It also reinforces in our own process that software has many ways of being built, and that our goal is to provide functionality that solves a real-world use case, and that the specific way we choose to address that is not a "requirement" and may change in successive iterations.  

For new features requested by a user, we should make sure that we have gotten all the points that the user expects from the feature, we can make it easy by following doing this before the feature adoption by engineering team.    
- Walkthrough of the feature designs   
- Interview with at least 2 prospective users of that features  


### Issue creation
#### Anatomy of a user story
> Kindly note that these fields are required for a story to be worked on

1. Brief title*: short description (4 - 5 words) that best describes the story
1. Description*: As a < some type of user >, I want < to do something > so that < some business reason that provides value is achieved>.
1. Features: Enumerated list of key features to include
1. Acceptance Criteria*: Usually a list of steps to follow to complete the definition done.
1. Stakeholder*: The person who reported or suggested the issue


##### Engineering based issues

When improvements needs to be made to the code, As an Engineer you can should create a story for it with:  
1. Brief title: short description (4 - 5 words) that best describes the story  
1. Description: reason for the improvement  
1. What's wrong with what we have   
1. Solution: Explain the new suggested solution  
1. Will it have an impact on end user ? Yes or No  

When you are done, add a label called `engineering` to the story so that it is separated from product stories and discuss it with other engineers during Engineering Improvements meeting that happens bi-weekly on Mondays.

#### Product Roadmap

- Stories within these Epics are moved to Active Planning when the team begins scoping the story for future sprint
- Once a task is ready to be considered for an upcoming sprint, it is moved to one of the upcoming milestones of the sprint planning board. (Found here: https://gitlab.com/doublegdp/app/-/boards/1844746)
- Tags are used extensively in stories for reporting and tracking. Frequently used tags include "Sprint Priority" for all stories that are active in the current sprint, "Spike" for research stories, "Bug" to mark 'Please Fix me' issues.

#### UI/UX Design Sprint

UI/UX Design works at least 1 sprint ahead of the Dev Sprint.   By the time engineers estimates story during sprint planning, all the stories that should have designs and flows will have designs and flows.

##### UI/UX Design Sprint Board Flow

The UI/UX Sprint Board uses the 2 columns (UX:Design and UX:Review) on the Engineering Sprint Board.
Designers must start working on a story from the **UX:Design** Column.  If you do not have a story assigned to you from the **UX:Design** column, ask your supervisor for direction.

1. Review the story and make sure:  
   - ​	**You understand it.** (If not, contact the product manager responsible and ask for clarification)  
   - ​	**The story follows the DoubleDGP User story format.**  (If not, contact the product manager responsible because the story should be revised).  
   - In case the Story is missing designs, move the story to the **UI/UX DESIGN**.  
2. Move the Story to the **Doing** Column and assign it to yourself to indicate you are working on it.  
      - Set the Due Date  
      - if the ticket is taken in the morning add (weight -1 ) days to the current date  
      - if the ticket is taken in the afternoon, add (weight ) days to the current date  
3. When completed, move the ticket to the UX:Review and let the stakeholder by tagging them in a comment.  
4. Once reviewed and approved, the ticket may be promoted to an epic depending on the scope and will be assigned a label of eng::planning (when promoting to the epic the current ticket will get closed.   It should be reopened and added to the newly created epic and add the tag to the ticket eng::planning)  

#### Dev Sprint

Development and non-development stories that are active in the current sprint are captured in Dev Sprint.

- Dev Sprint Project is organized by Open, To do, Doing, Staging,Closed. Stories that have to be prioritized at the sprint start are moved into Open column.  
- Dev Sprint can only have 5 stories per engineer, unless:  
   - All the stories are completed and there s time left in the sprint.  
   - All the stories have a complexity of 1.  (never happens)  
   - A Priority 0 bug need to be worked on immediately.  
   - leftover tickets from the previous sprint already started.  
- When a Dev Sprint has started, the only way to add a ticket in the sprint is to take one out.  

##### Engineering Sprint Board Flow

Start working on a story you are assigned to from the **Todo** Column.  If you do not have a story assigned to you from the **Todo** column, pick the First story in the **Open** column.
Pick a ticket from the **Todo** Column, If you do not have a ticket assigned to you from the **Todo** column, pick the First story in the **TODO** column, Tickets with high priority are the top, so they should be worked on first before moving on to the ones at the bottom of the column or the **Open** Column 

1. Review the story and make sure:  
   - ​	**You understand it.** (If not, contact the product manager responsible and ask for clarification)  
   - ​	**The story follows the DoubleDGP User story format.**  (If not, contact the product manager responsible because the story should be revised).  
   - In case the Story is missing designs, move the story to the **UI/UX DESIGN**.  
2. Move the Story to the **Doing** Column and assign it to yourself to indicate you are working on it.  
      - Set the Due Date  
      - if the ticket is taken in the morning add (weight -1 ) days to the current date  
      - if the ticket is taken in the afternoon, add (weight ) days to the current date  
3. Create a branch from master using the brief description of the user story as the branch name.  
4. When completed, create a merge request to merge to master - Find 2 other engineers that can get review your code.  
5. Move your ticket from doing to **In review** column 
6. You will get a notification when your story is code review and is either accepted and merged, or is rejected and some issues must be corrected.  
7. If the merge is successful, the code reviewer moves the story to the **Staging** column for you to **validate the story functions correctly in the staging environment.**  
8. Once you verify the story in the **Staging** column functions according to the acceptance criteria, you must add the **staging:verified** label to the ticket 
9. Once the ticket has been pushed to production, Review it once more to make sure everything is working as expected then close once verified.   

##### What to do when the **TODO** Column is empty but the sprint is not over.

If you do not have a story assigned to you from the **Open** column, pick the last story in the **Open** column.

##### Emergency Response Sprint
On occasion, a situation dictates that engineering iterates faster than the regular sprint cycle.  At DoubleDGP, we call that an Emergency Response Sprint.   Epics labeled 'Emergency Escalation' have stories which are called 'Emergency Escalation' stories. If a ticket is in the TODO column and does not have an Epic labeled 'Emergency Escalation', DO NOT work on it until ALL the 'Emergency Escalation' stories are completed.  New 'Emergency Escalation' stories maybe added to the TODO column every day and may contains short videos describing bugs in the application.  Each videos may contain multiple bugs and all bugs must be fixed in the same ticket.  Engineers must use follow the 'priority' labels when deciding which 'Emergency Escalation' stories to work on next.

### Sprint Prioritizing

Goal: Everyday meeting between Product, Engineering, Design to prioritize the following 3 sprints based on ever changing information.

## Sprint Planning Process

Our sprints are designed to facilitate asynchronous remote work across multiple time zones. They establish a regular cadence of releasing features to customers and the internal synchronous communications required to plan and coordinate.

While sprints are our primary product development process, we also have regular company and other meeting cadences that help us stay coordinated.


### Sprint Retrospective and Planning meetings

Goal: Sprint retrospective and planning meetings take place on Thursday every 14 days. The first part of the meeting is reserved for a sprint retrospective and is used to provide the team key takeaways for improving sprints. The second part of the meeting is focused on sprint planning and marks the close of the previous sprint and the start of the next sprint. This time is also used to define the goals for the sprint, align on sprint stories, and move stories based on priorities and capacity.

#### Sprint Planning

Our Sprint Planning has 2 parts.  

##### Asynchronous Sprint planning

Because we are an all remote team, it is rather difficult to find a good time for all of us to meet.  In addition, meetings can be boring (especially sprint planning meetings).  Engineers must have all the following sprint stories graded before the SP meeting.  No email required. Thus, when the product manager signals the stories for the following sprint are ready for **Async Sprint Planning Review**, the engineers must:

- review the issues in the upcoming sprint and independently vote https://handbook.doublegdp.com/engineering/story_complexity/  
- review the completeness of the stories.  If the story is missing keys components such as acceptance criteria, the engineer should bring it to the product manager's attention by adding a 'thumb down' and an explanation of what's missing in a *comment*.   
- ensure they understand the story.   If they do not understand the story they should bring it to the product manager's attention by adding a 'thumb down' and an explanation of what's missing in a *comment*.    


##### Synchronous Sprint Planning

The meeting is really to answer questions that have been raised by engineering during **Asynchronous Sprint planning** .

Guidelines for us to hold each other accountable:  

- Sprint planning and retrospective meetings are designed to make decisions on moving stories to plan or to deprioritize. Any design or solutioning discussions should take place before sprint start during Product Planning meetings. A go/no-go decision will be made on the story based on the story’s complexity and size. If there are too many unknowns for a story it should be deprioritized for a later iteration.
- The meetings have a packed agenda. We should keep each other accountable on agenda and time and make quick decisions or provide concise feedback to keep the meeting moving. We should remind each other to move items to other meetings if they need further discussion
- Besides the moderator, there are rotating roles for the meeting to assist with timekeeping, task creation, and capacity tracking with the following responsibilities: Time Keeper remind team of how much time is left for each agenda item, Task Assigner creates tasks that come up during the meeting in Asana, Capacity Tracker keeps a count of Low, Med, High stories.



### Sprint Demo

We finish our sprint with a recording ([demo video guidelines](https://handbook.doublegdp.com/marketing/social-media/#sprint-update-video-guidelines)) that is posted on youtube.  The audience for these updates is the rest of the team at DGDP, the broader community, and our investor.  We also hold a meeting whose purpose is to communicate the features developed by the engineering team during the sprint, answer any related questions and concerns, and gather feedback from all the departments.  

Suggested format of information to cover in these updates is:

1. What did you aim to accomplish this sprint?
2. What progress did you make against those goals?
3. What were highlights and lowlights of the experience?
4. What are your goals for the next sprint?
5. Anything else of note -- upcoming events, thank yous, time off, something fun?

Engineers should only show stories that bring 'new value' in the demo and not what 'has been worked on'.  Therefore, bugs should not be demoed as it does not bring 'new value'.  Backend stories should not be demoed as they do not bring any value to the end user.   

#### Points to consider when doing a sprint demo  
- What’s most impactful (to end user)
- What’s most interesting
- What was the most difficult
- What are you proudest of


#### When
 - The Sprint end Tuesday (already does).
 - The demo Q&A will be on Thursday.
 - meeting length to 30 mins

#### Who is it for
 - Everyone who has questions about the engineering sprint Videos.
 - The meeting would be for answering questions and discussions.
 - meeting is optional.

#### Handling Exceptions
Here are some exceptions that may arise in the sprint demo process, and how to handle them:

1. If you are on PTO for an entire sprint, you may skip the demo. If you contibuted some to the sprint but will be out on the day that the demo is due, upload a video early.
2. You may demo in desktop mode if you explicitly mention that you are doing it and share the reason why. e.g. "this page doesn't yet work on mobile and we have a ticket in next sprint to fix that"
3. [View product management deviations](https://handbook.doublegdp.com/product/04-prod-communications/#product-sprint-updates) from the typical sprint demo process.

#### Prerequisite
 - Everyone should add 2 out of 3 of the following items at least 8 hours before the following to the [demo doc](https://docs.google.com/document/d/1_4MRX2H4l9ajG5d9NK1OrOOUsqcMlFqtRaZJ153pSQ0/edit) prior to the meeting:
   - Most useful feature.
   - one question
   - What's missing?
      - Out of all the features that have been developed, is there a feature that would improve the experience dramatically?

CEO will watch team videos, record a summary, create a playlist from them, and post to social media. CEO Updates are due by 9:30p PT.

### Engineering Retrospective

The meeting agenda has 3 parts:
- 'What we liked about the sprint' (10 mins)  
- 'What we did not liked about the sprint'  (10 mins)  
- 'What can we improve' (30 mins). 
  - Goal is to find 1 actionable item to improve the next sprint 

Engineers must fill in their responses at least 1 hour before the retrospective meeting [link here](https://docs.google.com/document/d/1hvIiCohHwWwnNJPqAq1VfqrQBvzF-ZT3A0f-FbGHvH0/edit#)


# Story Complexity

## Background

Estimating the complexity of a story is a constant struggle because each team member have a slightly different skillset so each member vote based on what they know.  Often, estimations end up varying widly.  In addition, the exercise is repeated everytime a story need to be evaluated, which is often time during backlog refinement and/or sprint planning.   



## Complexity Weight

We weigh our complexity with the number of days we think a story will take to finish:

| Gitlab Weight | Label  | Description                                              |
| ------------- | ------ | -------------------------------------------------------- |
| 1             | Low    | 1 day of work                                          |
| 2             | Medium | 2 days work                                             |
| 3             | High   | 3 days of work                                   |
| Too big       | X-High | > 18 hours of work -  Story is too big - Must be smaller |



## Complexity Map

The complexity map is a set of predefined attributes associating the story with its complexity.   This is a work in progress, but it is has helped engineers find consensus among themselves.   

| Low                                                          | Medium                                                       | High                                                         | X-High |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------ |
| Add a card on the screen                                     | feature that needs some work on both back end and front end  | Work that requires updating a feature in production that is high risk and/or visibility. | TODO   |
| Adding a link or routing to a button                         | Needs some changes done to the logic of the app (back end)   | Requires external api’s/Requires third-party integration/and external integration | TODO   |
| Cosmetic changes(ie: Update to fonts and icons)              | Working on a full small feature with a minimum of one day effort and half a day of testing. | Involves db migrations                                       | TODO   |
| Changes happen in 1-2 files in the code base.                | Affects more than 3 ruby files (test files not included)     | Requires change of mutations or table structure              | TODO   |
| An independent task that takes less than 4 hours to implement and test | Affects multiple screens                                     | Includes change of current architecture of a specific existing feature. | TODO   |
|                                                              | Affects more than 6 react files                              | Has multiple screens and Has front end and backend work      | TODO   |
|                                                              |                                                              | Has data security implications (third party APIs accessing DGDP data). | TODO   |
|                                                              |                                                              |                                                              | TODO   |
|                                                              |                                                              |                                                              |        |

### Gitlab Voting

In the story itself, developers do vote by adding a **numbered** reaction Emoji to the story:

![image-20200722182025702](/img/gitlab-reaction.png)



| To Vote:       | Use                                                   | retrieving emoji                              |
| -------------- | ----------------------------------------------------- | --------------------------------------------- |
| 1              | ![image-20200722185410847](/img/gitlab-emo1.png)      | Type:one, then scroll at the end of the list. |
| 2              | ![image-20200722185020079](/img/gitlab-emo2.png)      | Type: two                                     |
| 3              | ![image-20200722185459845](/img/gitlab-emo3.png)      | Type: three                                   |
| Xtra humongous | ![image-20200722185554022](/img/gitlab-whalesize.png) | type: whale                                   |



After voting, the average of the results is entered in the 'weight' field in gitlab.
