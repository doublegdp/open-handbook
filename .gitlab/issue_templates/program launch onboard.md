## High Level Goals

1. [blank]
2. [blank]
3. [blank]

Checklist

- [ ] Participants sign contracts. (Do we need them reviewed by Vish?)

- [ ] Do we need a new agreement with [blank], or is the existing one sufficient?

- [ ] Define roles and responsibilities

- [ ] Define KPIs and Goals

- [ ] Define a marketing plan

- [ ] [Vendor] sign contract

- [ ] Add [blank] Program to Playbook

- [ ] Order equipment for [blank] Program

- [ ] Add pics/videos feature to discussion board

For non-US vendors and the contracted team members, a completed W-8BEN Form is required.

Notify business partner that invoices are required to submit to accounting@doublegdp.com.

Invoice should include:

- [ ]  Recipient's name

- [ ] Recipient's address

- [ ] Recipient's bank name

- [ ] Recipient's bank address

- [ ] Recipient's bank ABA Routing Numbers (if domestic wire within United States)

- [ ] Recipient's IBAN, BIC or SWIFT Code (if international bank outside of United States)

- [ ] Payment terms


Reference Documents

Program Charter: https://docs.google.com/document/d/1ZNe0UibL89gHczQ8SfMMm1z60EJdyCDoE1aJBJ-YIqc/edit?ts=5f7c856e

Contracts folder: https://drive.google.com/drive/folders/15c8Plw0MqS8OGDSciW80kPCCm1CBNDrc?usp=sharing

Artist contracts: https://docs.google.com/document/d/16YAv1JixB1Pp7rKheUYtXLKOFmwnfT_bytHyxQHim1U/edit?usp=sharing

Budget: https://docs.google.com/spreadsheets/d/1Z1ss7UNE74IBMceobkYFlcq7-4ozhyrZaqr1GR5pkEM/edit#gid=665431103

Chena / DGDP contract:  https://drive.google.com/drive/u/1/folders/1IIE7HIeLsk4kZydoVm1Iz515sYrbhUZH

